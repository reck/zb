/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.7.17-log : Database - zb_server
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`zb_server` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `zb_server`;

/*Table structure for table `basic_config` */

DROP TABLE IF EXISTS `basic_config`;

CREATE TABLE `basic_config` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(50) NOT NULL COMMENT '标识项目是否处于管理员调试、修改状态，用户登录之后会有相应的弹窗提醒',
  `config_value` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='全局性配置';

/*Data for the table `basic_config` */

insert  into `basic_config`(`id`,`config_name`,`config_value`) values (1,'project_debugging_tip','0');

/*Table structure for table `basic_message_wall` */

DROP TABLE IF EXISTS `basic_message_wall`;

CREATE TABLE `basic_message_wall` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(100) NOT NULL COMMENT '留言内容',
  `user_id` bigint(11) DEFAULT NULL COMMENT '留言人id',
  `user_name` varchar(20) NOT NULL COMMENT '留言人名称',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态(0：可用，1：禁用)',
  `ip_address` varchar(20) NOT NULL COMMENT 'IP地址',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `basic_message_wall` */

/*Table structure for table `basic_source_download` */

DROP TABLE IF EXISTS `basic_source_download`;

CREATE TABLE `basic_source_download` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '资源名称',
  `down_url` varchar(150) NOT NULL COMMENT '下载地址',
  `icon_name` varchar(50) NOT NULL DEFAULT 'icon-download-alt' COMMENT '图标',
  `sort_num` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

/*Data for the table `basic_source_download` */

insert  into `basic_source_download`(`id`,`name`,`down_url`,`icon_name`,`sort_num`,`create_time`,`update_time`) values (1,'ActiveMQ消息监听处理入门Demo(Spring简单集成).zip','http://pan.baidu.com/s/1hrH9DnY','icon-download-alt',0,'2016-11-03 15:17:37','2016-11-03 15:17:39'),(2,'apache tomcat集群环境搭建图文教程.zip','http://pan.baidu.com/s/1o7DtCsq','icon-download-alt',0,'2016-11-03 15:18:34','2016-11-03 15:18:36'),(3,'chatpush-在线聊天室(可选指定用户聊天).zip','http://pan.baidu.com/s/1eR521IM','icon-download-alt',0,'2016-11-03 15:19:33','2016-11-03 15:19:35'),(4,'crm后台系统源码(bootstrap).zip','http://pan.baidu.com/s/1i4TcT29','icon-download-alt',0,'2016-11-03 15:19:56','2016-11-03 15:19:57'),(5,'DateTimePicker_JQuery日期和时间插件.zip','http://pan.baidu.com/s/1o8eyaF4','icon-download-alt',0,'2016-11-03 15:20:20','2016-11-03 15:20:22'),(6,'dwrcomet-推送消息给指定用户Demo.zip','http://pan.baidu.com/s/1kVAslEn','icon-download-alt',0,'2016-11-03 15:20:42','2016-11-03 15:20:44'),(7,'DwrPush-模拟消息推送至所有当前在线的用户.zip','http://pan.baidu.com/s/1i5BclKh','icon-download-alt',0,'2016-11-03 15:21:03','2016-11-03 15:21:05'),(8,'dwz富客户端 springmvc ibatis简单Demo.zip','http://pan.baidu.com/s/1boRTofX','icon-download-alt',0,'2016-11-03 15:21:24','2016-11-03 15:21:26'),(9,'easyui完整Demo-各种功能_布局.zip','http://pan.baidu.com/s/1nvJb8G5','icon-download-alt',0,'2016-11-03 15:21:52','2016-11-03 15:21:54'),(10,'erp项目源码--bootstrap 选项卡切换.zip','http://pan.baidu.com/s/1boLhDLl','icon-download-alt',0,'2016-11-03 15:22:13','2016-11-03 15:22:15'),(11,'Highcharts简单实例Demo.zip','http://pan.baidu.com/s/1dFt91nF','icon-download-alt',0,'2016-11-03 15:22:37','2016-11-03 15:22:38'),(12,'HTML5 CSS3鼠标悬停图片特效.zip','http://pan.baidu.com/s/1eSLXIee','icon-download-alt',0,'2016-11-03 15:23:01','2016-11-03 15:23:03'),(13,'Java web项目：mybatis、springmvc、seajs、freemarker、分布式session、自定义taglib等.zip','http://pan.baidu.com/s/1qYzsFxU','icon-download-alt',0,'2016-11-03 15:23:21','2016-11-03 15:23:23'),(14,'Java后台系统-不同框架优点集成-bootstrap布局-权限角色管理(含sql)----1.0.0版本.zip','http://pan.baidu.com/s/1i4GlFql','icon-download-alt',0,'2016-11-03 15:23:43','2016-11-03 15:23:44'),(15,'java线程异步：异步发送email、回调接口定义、其他异步线程demo.zip','http://pan.baidu.com/s/1jIbOunO','icon-download-alt',0,'2016-11-03 15:24:06','2016-11-03 15:24:08'),(16,'JAVA之23种设计模式的完整实例代码.zip','http://pan.baidu.com/s/1o8NJsL0','icon-download-alt',0,'2016-11-03 15:24:28','2016-11-03 15:24:29'),(17,'jd-gui反编译jar_复制源码为java文件(自己写的工具).rar','http://pan.baidu.com/s/1dE8mrap','icon-download-alt',0,'2016-11-03 15:24:47','2016-11-03 15:24:50'),(18,'jquery点击图片选中特效.rar','http://pan.baidu.com/s/1jIJNEq6','icon-download-alt',0,'2016-11-03 15:25:12','2016-11-03 15:25:13'),(19,'jQuery轻量级圆形进度指示器插件.zip','http://pan.baidu.com/s/1o8Lbz4m','icon-download-alt',0,'2016-11-03 15:27:51','2016-11-03 15:27:53'),(20,'lucene基于本地磁盘的文件索引Demo.zip','http://pan.baidu.com/s/1jI8suqy','icon-download-alt',0,'2016-11-03 15:28:11','2016-11-03 15:28:13'),(21,'lucene基于内存的检索Demo.zip','http://pan.baidu.com/s/1qXEB2DA','icon-download-alt',0,'2016-11-03 15:28:31','2016-11-03 15:28:32'),(22,'lucene索引——增删改查.zip','http://pan.baidu.com/s/1bo2kKCf','icon-download-alt',0,'2016-11-03 15:28:49','2016-11-03 15:28:50'),(23,'MongoDB环境搭建与实例代码测试.rar','http://pan.baidu.com/s/1o7OiXMM','icon-download-alt',0,'2016-11-03 15:29:09','2016-11-03 15:29:10'),(24,'orm框架 微信框架 自定义拦截器 自定义注解 数据源切换等.zip','http://pan.baidu.com/s/1jHMVoiU','icon-download-alt',0,'2016-11-03 15:29:29','2016-11-03 15:29:31'),(25,'pdf文件打印(freemarker作为pdf模板，通过参数替换、遍历显示效果)（java版 spring）.zip','http://pan.baidu.com/s/1kUWw8Vd','icon-download-alt',0,'2016-11-03 15:29:47','2016-11-03 15:29:49'),(26,'pluplaod文件分割上传Demo.zip','http://pan.baidu.com/s/1kV8Wpy3','icon-download-alt',0,'2016-11-03 15:30:05','2016-11-03 15:30:07'),(27,'SpringActiveMQ消息发送与接收(WEB版)-入门实例.zip','http://pan.baidu.com/s/1hsqL6vE','icon-download-alt',0,'2016-11-03 15:30:24','2016-11-03 15:30:26'),(28,'springbatch批处理(完整Demo).zip','http://pan.baidu.com/s/1hsgCgVq','icon-download-alt',0,'2016-11-03 15:30:47','2016-11-03 15:30:49'),(29,'springmvc mybatis集成—干净框架(直接使用).zip','http://pan.baidu.com/s/1i4XgFjR','icon-download-alt',0,'2016-11-03 15:31:09','2016-11-03 15:31:10'),(30,'springsecurity权限控制Demo.zip','http://pan.baidu.com/s/1i4MXsnr','icon-download-alt',0,'2016-11-03 15:31:30','2016-11-03 15:31:31'),(31,'spring_activemq消息发送接收(最终版-非WEB访问).zip','http://pan.baidu.com/s/1jIDcfII','icon-download-alt',0,'2016-11-03 15:31:47','2016-11-03 15:31:49'),(32,'spring_security3_详细配置(摘自网络).zip','http://pan.baidu.com/s/1kUAbWjl','icon-download-alt',0,'2016-11-03 15:32:09','2016-11-03 15:32:10'),(33,'SSO之CAS单点登录详细图文教程.zip','http://pan.baidu.com/s/1hrY4BBm','icon-download-alt',0,'2016-11-03 15:32:29','2016-11-03 15:32:30'),(34,'WebSocket消息聊天室Demo.zip','http://pan.baidu.com/s/1jI7CzAY','icon-download-alt',0,'2016-11-03 15:32:51','2016-11-03 15:32:52'),(35,'读取Excel工具类(包含流读取、文件读取).zip','http://pan.baidu.com/s/1skE92ET','icon-download-alt',0,'2016-11-03 15:33:09','2016-11-03 15:33:11'),(36,'断点 多线程下载实例.zip','http://pan.baidu.com/s/1miuHeIG','icon-download-alt',0,'2016-11-03 15:33:31','2016-11-03 15:33:32'),(37,'基于dwr消息推送Demo.zip','http://pan.baidu.com/s/1boRTogJ','icon-download-alt',0,'2016-11-03 15:33:47','2016-11-03 15:33:48'),(38,'手机_日期时间控件.zip','http://pan.baidu.com/s/1hs7G2HY','icon-download-alt',0,'2016-11-03 15:34:05','2016-11-03 15:34:07'),(39,'手机时间控件.zip','http://pan.baidu.com/s/1qXYou7M','icon-download-alt',0,'2016-11-03 15:34:25','2016-11-03 15:34:27'),(40,'数据采集—基于百度文库的抓取Demo.zip','http://pan.baidu.com/s/1cpBMeM','icon-download-alt',0,'2016-11-03 15:34:46','2016-11-03 15:34:47'),(41,'网页加载进度条-实例Demo.zip','http://pan.baidu.com/s/1pLViL9x','icon-download-alt',0,'2016-11-03 15:35:07','2016-11-03 15:35:08'),(42,'微信JSAPI支付完整Demo.zip','http://pan.baidu.com/s/1nvdehnj','icon-download-alt',0,'2016-11-03 15:35:29','2016-11-03 15:35:30'),(43,'微信JSAPI支付完整Demo第二版(新增关闭订单、查询订单、查询退款、下载对账单、申请退款).zip','http://pan.baidu.com/s/1i4VKKNN','icon-download-alt',0,'2016-11-03 15:35:47','2016-11-03 15:35:48'),(44,'异步发送email之demo.zip','http://pan.baidu.com/s/1o7IJv1S','icon-download-alt',0,'2016-11-03 15:36:05','2016-11-03 15:36:07'),(45,'银联PC网关支付demo(退款、对账文件、查询、撤销).zip','http://pan.baidu.com/s/1pKXAz0Z','icon-download-alt',0,'2016-11-03 15:36:25','2016-11-03 15:36:27'),(46,'支付宝PC及时到账支付Demo-JAVA.zip','http://pan.baidu.com/s/1pLyEKsz','icon-download-alt',0,'2016-11-03 15:36:44','2016-11-03 15:36:46'),(47,'支付宝PC支付完整Demo.zip','http://pan.baidu.com/s/1kUAbWjH','icon-download-alt',0,'2016-11-03 15:37:03','2016-11-03 15:37:05');

/*Table structure for table `sys_app` */

DROP TABLE IF EXISTS `sys_app`;

CREATE TABLE `sys_app` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL COMMENT '名称',
  `sort` int(11) NOT NULL COMMENT '排序',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `status` tinyint(1) NOT NULL COMMENT '是否启用',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `icon` varchar(30) NOT NULL COMMENT '图标',
  `online` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否上线，0：未上线，1：已上线',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COMMENT='应用表';

/*Data for the table `sys_app` */

insert  into `sys_app`(`id`,`name`,`sort`,`update_time`,`create_time`,`status`,`code`,`icon`,`online`) values (4,'BOSS系统管理中心',1,'2017-12-07 00:07:42','2017-12-07 00:07:42',0,'zb-center-web-boss','',1),(105,'应用监控系统',2,'2017-12-07 00:40:52','2017-12-07 00:40:52',0,'zb-center-web-monitor','',0),(106,'定时任务管理系统',3,'2017-12-22 01:39:53','2017-12-07 00:41:24',0,'zb-center-web-task','',0),(107,'订单管理系统',4,'2018-05-17 22:27:07','2017-12-07 00:42:03',1,'zb-center-web-order','',0);

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `sort` varchar(100) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `permission_code` varchar(50) NOT NULL COMMENT '所需权限',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `app_id` bigint(20) NOT NULL COMMENT '菜单所属应用id',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`id`,`name`,`url`,`parent_id`,`sort`,`icon`,`type`,`permission_code`,`status`,`app_id`,`create_time`,`update_time`) values (1,'系统管理','#',0,'1','fa fa-cogs','0','sys:mgt',0,4,'2017-12-02 03:00:00','2017-12-02 03:00:00'),(2,'用户管理','/user/toUserListView',1,'2','','1','user:mgt',0,4,'2017-12-02 03:00:00','2017-12-02 03:00:00'),(4,'角色管理','/role/toRoleListView',1,'4',NULL,'1','role:mgt',0,4,'2017-12-02 03:00:00','2017-12-02 03:00:00'),(5,'权限管理','/permission/toListView',1,'3',NULL,'1','permission:mgt',0,4,'2017-12-02 03:00:00','2017-12-02 03:00:00'),(24,'应用管理','/app/toAppListView',1,'1',NULL,'1','app:mgt',0,4,'2017-12-02 03:00:00','2017-12-02 03:00:00'),(25,'菜单管理','/menu/toMenuListView',1,'5','','1','menu:mgt',0,4,'2017-12-02 03:00:00','2017-12-02 03:00:00'),(45,'在线聊天','#',0,'6','fa fa-comments','0','online:chat:mgt',0,4,'2018-01-08 19:10:07','2018-01-08 19:10:07'),(46,'netty实现在线聊天','/chat/netty/view',45,'1','','1','online:chat:netty:view',0,4,'2018-01-08 19:11:46','2018-01-08 19:11:46');

/*Table structure for table `sys_permission` */

DROP TABLE IF EXISTS `sys_permission`;

CREATE TABLE `sys_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '权限名称',
  `code` varchar(50) NOT NULL COMMENT '权限代码',
  `description` varchar(50) DEFAULT NULL COMMENT '描述',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父id',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态，0：可用，1：不可用',
  `app_id` bigint(20) NOT NULL COMMENT '所属应用id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COMMENT='权限表';

/*Data for the table `sys_permission` */

insert  into `sys_permission`(`id`,`name`,`code`,`description`,`parent_id`,`status`,`app_id`) values (1,'系统管理','sys:mgt','对系统模块的管理',0,0,4),(2,'人员管理','user:mgt','对人员进行管理',1,0,4),(3,'角色管理','role:mgt','对角色进行管理',1,0,4),(4,'权限管理','permission:mgt','对权限进行管理',1,0,4),(9,'查看用户列表','user:list','可以查看用户列表',2,0,4),(10,'编辑用户信息','user:edit','是否能进入编辑用户信息页面',9,0,4),(11,'删除用户','user:delete','可以删除用户',9,0,4),(12,'查看角色列表','role:list','可以查看角色列表',3,0,4),(13,'查看权限列表','permission:list',NULL,4,0,4),(14,'新增角色','role:add','新增角色信息',12,0,4),(15,'更新角色','role:update','是否可以提交更新',12,0,4),(16,'删除角色','role:delete','可以删除角色',12,0,4),(17,'新增权限','permission:add','可以添加新权限',13,0,4),(18,'编辑权限','permission:edit','是否能进入权限编辑页面',13,0,4),(19,'删除权限','permission:delete','可以删除权限数据',13,0,4),(20,'添加用户','user:add','可以添加新的用户',9,0,4),(43,'应用管理','app:mgt','应用管理',1,0,4),(44,'查看应用列表','app:list','查看应用列表',43,0,4),(45,'应用编辑','app:edit','是否能进入应用编辑页面',44,0,4),(46,'删除应用','app:delete','删除应用',44,0,4),(47,'添加应用','app:add','添加应用',44,0,4),(48,'为用户分配应用','user:assign:app','为用户分配应用',9,0,4),(49,'为用户分配角色','user:assign:role','为用户分配角色',9,0,4),(50,'菜单管理','menu:mgt','菜单管理',1,0,4),(70,'查看菜单列表','menu:list','查看菜单列表',50,0,4),(75,'新增菜单','menu:add','可以添加新菜单',50,0,4),(76,'删除菜单','menu:delete','可以删除菜单',50,0,4),(77,'编辑菜单','menu:edit','是否能进入菜单编辑页面',50,0,4),(81,'编辑角色','role:edit','是否能进入编辑页面',12,0,4),(82,'更新菜单','menu:update','提交更新菜单信息',50,0,4),(83,'更新用户信息','user:update','更新用户信息',9,0,4),(84,'更新权限','permission:update','是否可以提交更新',13,0,4),(85,'更新应用信息','app:update','是否可以提交更新',44,0,4),(95,'在线聊天','online:chat:mgt','在线聊天',0,0,4),(96,'netty实现在线聊天','online:chat:netty:view','进入在线聊天的页面',95,0,4);

/*Table structure for table `sys_re_user_app` */

DROP TABLE IF EXISTS `sys_re_user_app`;

CREATE TABLE `sys_re_user_app` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL COMMENT '用户id',
  `app_id` bigint(11) NOT NULL COMMENT '应用id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;

/*Data for the table `sys_re_user_app` */

insert  into `sys_re_user_app`(`id`,`user_id`,`app_id`) values (3,41,1),(100,47,4),(119,1,4),(127,39,86),(133,34,89),(136,36,4),(137,38,4),(138,40,4),(139,43,4),(140,48,4),(144,49,4),(145,50,4),(146,51,4),(147,52,4),(148,53,4),(149,54,4),(150,55,4),(151,56,4),(152,57,4),(153,58,4),(154,59,4),(155,60,4),(156,61,4),(157,62,4),(158,63,4),(159,64,4),(160,65,4),(161,66,4),(162,67,4),(163,68,4),(164,69,4);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(50) DEFAULT NULL COMMENT '描述',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `update_time` datetime NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='角色表';

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`app_id`,`name`,`description`,`status`,`update_time`,`create_time`) values (1,4,'超级管理员','最高级别权限管理员',0,'2016-08-01 15:17:52','2016-08-01 15:17:53'),(2,4,'员工','基层员工',0,'2016-08-01 15:18:21','2016-08-01 15:18:23'),(3,4,'财务','财务部角色',0,'2016-08-03 13:21:56','2016-08-03 13:21:58'),(4,4,'人事','人事',0,'2017-03-21 13:19:34','2017-03-21 13:19:34'),(6,4,'出纳员','出纳员',0,'2017-03-21 13:22:06','2017-03-21 13:22:06'),(7,4,'采购经理','采购经理',0,'2017-03-21 13:22:23','2017-03-21 13:22:23'),(8,4,'总经理','总经理',0,'2017-03-21 13:23:03','2017-03-21 13:23:03'),(15,4,'OA审批员','OA审批员',0,'2017-12-03 16:23:11','2017-12-03 16:23:11'),(16,4,'公众号运营员','公众号运营员',0,'2017-12-03 16:25:33','2017-12-03 16:25:33');

/*Table structure for table `sys_role_permission` */

DROP TABLE IF EXISTS `sys_role_permission`;

CREATE TABLE `sys_role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  `app_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3724 DEFAULT CHARSET=utf8 COMMENT='角色权限表';

/*Data for the table `sys_role_permission` */

insert  into `sys_role_permission`(`id`,`role_id`,`permission_id`,`app_id`) values (2667,6,1,4),(2668,6,2,4),(2669,6,9,4),(2670,6,21,4),(2671,6,22,4),(2672,6,23,4),(2673,6,24,4),(2674,6,25,4),(2675,6,39,4),(2676,6,40,4),(2677,6,26,4),(2678,6,27,4),(3238,7,1,4),(3239,7,2,4),(3240,7,9,4),(3241,7,10,4),(3242,7,43,4),(3243,7,44,4),(3244,7,45,4),(3274,15,1,4),(3275,15,2,4),(3276,15,9,4),(3281,2,1,4),(3282,2,2,4),(3283,2,9,4),(3284,2,10,4),(3285,2,21,4),(3286,2,22,4),(3287,2,23,4),(3288,2,24,4),(3289,2,25,4),(3290,2,39,4),(3291,2,40,4),(3292,2,26,4),(3293,2,27,4),(3294,2,28,4),(3295,2,29,4),(3296,2,30,4),(3297,2,78,4),(3298,2,79,4),(3299,2,80,4),(3300,2,31,4),(3301,2,32,4),(3302,2,33,4),(3303,2,34,4),(3304,2,35,4),(3329,16,1,4),(3330,16,2,4),(3331,16,9,4),(3478,8,1,4),(3479,8,2,4),(3480,8,9,4),(3481,8,10,4),(3482,8,21,4),(3483,8,22,4),(3484,8,23,4),(3485,8,24,4),(3486,8,25,4),(3487,8,39,4),(3488,8,40,4),(3489,8,26,4),(3490,8,27,4),(3491,8,28,4),(3492,8,29,4),(3493,8,30,4),(3494,8,31,4),(3495,8,32,4),(3496,8,33,4),(3497,8,34,4),(3498,8,35,4),(3674,1,1,4),(3675,1,2,4),(3676,1,9,4),(3677,1,10,4),(3678,1,20,4),(3679,1,48,4),(3680,1,49,4),(3681,1,3,4),(3682,1,12,4),(3683,1,14,4),(3684,1,15,4),(3685,1,81,4),(3686,1,4,4),(3687,1,13,4),(3688,1,17,4),(3689,1,18,4),(3690,1,84,4),(3691,1,43,4),(3692,1,44,4),(3693,1,45,4),(3694,1,47,4),(3695,1,85,4),(3696,1,50,4),(3697,1,70,4),(3698,1,75,4),(3699,1,77,4),(3700,1,82,4),(3701,1,86,4),(3702,1,87,4),(3703,1,88,4),(3704,1,91,4),(3705,1,92,4),(3706,1,89,4),(3707,1,90,4),(3708,1,93,4),(3709,1,94,4),(3710,1,95,4),(3711,1,96,4),(3712,3,1,4),(3713,3,2,4),(3714,3,9,4),(3715,3,10,4),(3716,4,1,4),(3717,4,2,4),(3718,4,9,4),(3719,4,10,4),(3720,4,83,4),(3721,4,28,4),(3722,4,30,4),(3723,4,35,4);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL COMMENT '登录用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `real_name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '账号状态',
  `open_id` varchar(50) DEFAULT NULL COMMENT '微信openid',
  `from_system` tinyint(4) NOT NULL DEFAULT '0' COMMENT '来源哪个系统。(后期会进行与第三方系统做用户数据对接，默认为当前系统用户中心)',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`user_name`,`password`,`real_name`,`status`,`open_id`,`from_system`,`create_time`,`update_time`) values (1,'admin','e10adc3949ba59abbe56e057f20f883e','超级管理员',0,NULL,0,'2016-08-03 13:22:46','2017-11-30 01:58:35'),(34,'chunayuan','e10adc3949ba59abbe56e057f20f883e','李斯',0,NULL,0,'2017-07-11 16:41:36','2017-11-30 02:15:43'),(38,'libai','e10adc3949ba59abbe56e057f20f883e','李白',0,NULL,0,'2017-07-11 16:42:56','2017-12-07 00:51:04'),(39,'zongjingli','e10adc3949ba59abbe56e057f20f883e','王化',1,NULL,0,'2017-07-11 16:43:18','2017-10-25 16:11:34'),(40,'sunbin','e10adc3949ba59abbe56e057f20f883e','孙膑',0,NULL,0,'2017-08-17 10:49:04','2017-12-07 00:50:55'),(43,'sunshangxiang','e10adc3949ba59abbe56e057f20f883e','孙尚香',0,NULL,0,'2017-08-22 00:10:42','2017-09-12 00:40:34'),(47,'wuzetian','e10adc3949ba59abbe56e057f20f883e','武则天',0,NULL,0,'2017-09-12 00:25:05','2017-12-07 00:50:44'),(57,'zhangliang','e10adc3949ba59abbe56e057f20f883e','张良',0,NULL,0,'2017-12-07 00:51:19','2017-12-07 00:51:19'),(58,'sunwukong','e10adc3949ba59abbe56e057f20f883e','孙悟空',0,NULL,0,'2017-12-07 00:51:40','2017-12-07 00:51:40'),(59,'zhouyu','e10adc3949ba59abbe56e057f20f883e','周瑜',2,NULL,0,'2017-12-07 00:52:17','2017-12-07 00:52:17'),(60,'diaochan','e10adc3949ba59abbe56e057f20f883e','貂蝉',1,NULL,0,'2017-12-07 00:52:32','2017-12-07 00:52:32'),(61,'bailishouyue','e10adc3949ba59abbe56e057f20f883e','百里守约',0,NULL,0,'2017-12-07 00:52:52','2017-12-07 00:52:52'),(62,'laofuzi','e10adc3949ba59abbe56e057f20f883e','老夫子',2,NULL,0,'2017-12-07 00:53:40','2017-12-07 00:53:40'),(63,'zhugeliang','e10adc3949ba59abbe56e057f20f883e','诸葛亮',0,NULL,0,'2017-12-21 16:51:51','2017-12-21 16:51:51'),(64,'daji2017','e10adc3949ba59abbe56e057f20f883e','妲己',0,NULL,0,'2017-12-21 16:52:32','2017-12-21 16:52:32'),(65,'zhongkui','e10adc3949ba59abbe56e057f20f883e','钟馗',0,NULL,0,'2017-12-21 16:53:38','2017-12-21 16:53:38'),(66,'zhenji','e10adc3949ba59abbe56e057f20f883e','甄姬',0,NULL,0,'2017-12-21 16:53:51','2017-12-21 16:53:51'),(67,'taiyizhenren','e10adc3949ba59abbe56e057f20f883e','太乙真人',0,NULL,0,'2017-12-21 16:54:13','2017-12-21 16:54:13'),(68,'mingshiyin','e10adc3949ba59abbe56e057f20f883e','明世隐',0,NULL,0,'2017-12-21 16:54:24','2017-12-21 16:54:24'),(69,'nvwa2017','e10adc3949ba59abbe56e057f20f883e','女娲',1,NULL,0,'2017-12-21 16:54:48','2017-12-21 16:54:48');

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `app_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`id`,`user_id`,`role_id`,`app_id`) values (121,38,7,4),(220,1,1,4),(221,34,15,89),(223,39,16,86),(237,43,2,4),(240,40,2,4),(241,47,4,4),(242,59,2,4),(244,60,2,4),(245,60,6,4);

/*Table structure for table `visit_record` */

DROP TABLE IF EXISTS `visit_record`;

CREATE TABLE `visit_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cli_ip` varchar(20) DEFAULT NULL COMMENT '客户端ip',
  `cli_sys_version` longtext COMMENT '客户端系统版本',
  `cli_sys_arch` varchar(20) DEFAULT NULL COMMENT '客户端操作系统位数',
  `cli_sys_name` varchar(20) DEFAULT NULL COMMENT '客户端系统名称',
  `cli_sys_agent` longtext COMMENT '浏览器基本信息',
  `cli_req_url` varchar(200) DEFAULT NULL COMMENT '客户端发出请求时的完整URL',
  `cli_req_uri` varchar(50) DEFAULT NULL COMMENT '请求行中的资源名部分',
  `cli_req_method` varchar(10) DEFAULT NULL COMMENT '客户机请求方式',
  `local_addr` varchar(50) DEFAULT NULL COMMENT 'WEB服务器的IP地址',
  `local_name` varchar(50) DEFAULT NULL COMMENT 'WEB服务器的主机名',
  `req_session_id` varchar(50) NOT NULL COMMENT '会话的sessionId',
  `login_user_name` varchar(20) NOT NULL COMMENT '登录用户名',
  `create_time` datetime NOT NULL COMMENT '来访时间',
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3351 DEFAULT CHARSET=utf8;

/*Data for the table `visit_record` */

insert  into `visit_record`(`id`,`cli_ip`,`cli_sys_version`,`cli_sys_arch`,`cli_sys_name`,`cli_sys_agent`,`cli_req_url`,`cli_req_uri`,`cli_req_method`,`local_addr`,`local_name`,`req_session_id`,`login_user_name`,`create_time`,`update_time`) values (3343,'127.0.0.1','6.1','amd64','Windows 7','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','http://server.zhoubang85.com/main','/main','GET','127.0.0.1','server.zhoubang85.com','8D38C5B09A85EC9883CECAAE04066B9C','admin','2018-05-17 21:03:19','2018-05-17 21:03:19'),(3344,'127.0.0.1','6.1','amd64','Windows 7','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','http://server.zhoubang85.com/main','/main','GET','127.0.0.1','server.zhoubang85.com','D66B5521CEA7CD8A0EBEFF554D1881FB','admin','2018-05-17 21:05:58','2018-05-17 21:05:58'),(3345,'127.0.0.1','6.1','amd64','Windows 7','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','http://localhost/main','/main','GET','0:0:0:0:0:0:0:1','0:0:0:0:0:0:0:1','FC90D6E0B5F5D5C2AB93A662A7DF8BA7','admin','2018-05-17 22:26:48','2018-05-17 22:26:48'),(3346,'127.0.0.1','6.1','amd64','Windows 7','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','http://server.zhoubang85.com/main','/main','GET','127.0.0.1','server.zhoubang85.com','34B9AF5B76611164B6A9C7D8E5AA0C93','admin','2018-05-17 22:26:58','2018-05-17 22:26:58'),(3347,'127.0.0.1','6.1','amd64','Windows 7','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','http://server.zhoubang85.com/main','/main','GET','127.0.0.1','server.zhoubang85.com','5DBE89093F4386E4A0B7283A22F92CA7','admin','2018-05-17 22:29:24','2018-05-17 22:29:24'),(3348,'127.0.0.1','6.1','amd64','Windows 7','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','http://server.zhoubang85.com/main','/main','GET','127.0.0.1','server.zhoubang85.com','55378B570ED77ED2FFF63BD8C5CD0C20','admin','2018-05-17 22:31:27','2018-05-17 22:31:27'),(3349,'127.0.0.1','6.1','amd64','Windows 7','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','http://server.zhoubang85.com/main','/main','GET','127.0.0.1','server.zhoubang85.com','F9B15FF8084AB557A2280F638428AF2A','admin','2018-05-17 22:34:16','2018-05-17 22:34:16'),(3350,'127.0.0.1','6.1','amd64','Windows 7','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','http://server.zhoubang85.com/main','/main','GET','127.0.0.1','server.zhoubang85.com','A0F77E4729C036623FA287CAADAB5740','admin','2018-05-17 22:48:24','2018-05-17 22:48:24');

