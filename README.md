
#### 预览网址（请直接看网页最下面的效果图）：[https://gitee.com/zhoubang85/sea_springboot](https://gitee.com/zhoubang85/sea_springboot)

#### 详细的项目说明以及部署步骤等，已经更新在了wiki中 ： [https://gitee.com/zhoubang85/zb/wikis](https://gitee.com/zhoubang85/zb/wikis)
<br/>


##### 如有遇到什么问题，可以在码云上反馈（建议在Issues栏目下反馈）
<br/>

##### 项目中所使用到的第三方jar，已经上传到附件板块里面了，可自行下载使用；

##### 对于刚刚接触我这个项目的朋友而言，或许因为系统环境等因素，导致前期部署成本有点高，如果觉得麻烦的话，同时对springboot也感兴趣的话，建议可以直接去看当前项目springboot的重构版本 https://gitee.com/zhoubang85/sea_springboot



