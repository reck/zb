package pers.zb.center.common.core.enums;

public enum SeparatorEnum {

    PERMISSION_UPDATE("#");//权限更新，消息拼接符

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private SeparatorEnum(String type) {
        this.type = type;
    }
}