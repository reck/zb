package pers.zb.center.common.core.enums.entity;

public interface BaseEntityEnum<E extends Enum<E>> {

    int getIntValue();
}
