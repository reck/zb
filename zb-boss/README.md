##### 如有遇到什么问题，可以在码云上反馈，也可以通过邮件 842324724@qq.com 反馈，我会第一时间处理
<br/>

#### zb-boss项目部署、启动步骤(main方式启动服务)：
###### 首先，你本地需要先启动zookeeper服务、redis服务。
##### 1、打包编译项目
##### 2、执行zb-center-service-user模块下的main方法，实现服务注册
##### 3、部署zb-center-web-boss到tomcat，启动访问即可
###### 【项目启动的两种方式】：
###### 1、上面是最简洁快速的启动项目的方式，以main方法启动，方便本地服务测试。
###### 2、可以通过在对应模块的target目录下，通过CMD命令窗口执行命令 java -jar 模块名称.jar 的方式启动服务。


##### 预览网址（请直接看网页最下面的效果图）：[https://gitee.com/zhoubang85/sea_springboot](https://gitee.com/zhoubang85/sea_springboot)

##### 技术论坛：[http://www.2b2b92b.cn](http://www.2b2b92b.cn)
