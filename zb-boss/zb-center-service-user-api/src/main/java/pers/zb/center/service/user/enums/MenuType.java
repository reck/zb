package pers.zb.center.service.user.enums;


import pers.zb.center.common.core.enums.entity.BaseEntityEnum;

/**
 * 菜单类型
 * 
 * 
 * 创建日期：2016年8月3日 下午1:32:52 操作用户：zhoubang
 *
 */
public enum MenuType implements BaseEntityEnum<MenuType> {

    DIRECTORY(0, "目录"), MENU(1, "菜单");

    private MenuType(int code, String description) {
        this.code = new Integer(code);
        this.description = description;
    }

    private int code;

    private String description;

    public int getCode() {

        return code;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int getIntValue() {
        return this.code;
    }
}
