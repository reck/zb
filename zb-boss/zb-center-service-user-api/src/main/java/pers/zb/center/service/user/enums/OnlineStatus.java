package pers.zb.center.service.user.enums;


import pers.zb.center.common.core.enums.entity.BaseEntityEnum;

/**
 * 应用是否上线
 */
public enum OnlineStatus implements BaseEntityEnum<OnlineStatus> {

    NO(0, "未上线"), YES(1, "已上线");

    private OnlineStatus(int code, String description) {
        this.code = new Integer(code);
        this.description = description;
    }

    private int code;

    private String description;

    public int getCode() {

        return code;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int getIntValue() {
        return this.code;
    }
}
