package pers.zb.center.service.user.api.basic;


import pers.zb.center.common.core.service.BaseService;
import pers.zb.center.service.user.entity.basic.BasicMessageWall;

import java.util.List;

public interface BasicMessageWallService extends BaseService<BasicMessageWall> {

    /**
     * 获取对应数量的留言列表
     * @param num
     * @return
     */
    List<BasicMessageWall> getMessageList(int num);
}
