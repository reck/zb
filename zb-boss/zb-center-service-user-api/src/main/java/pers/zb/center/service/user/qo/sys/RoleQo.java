package pers.zb.center.service.user.qo.sys;

import java.io.Serializable;

public class RoleQo implements Serializable {

    private static final long serialVersionUID = 9069414783397826105L;

    private Long roleId;// 角色id
    private Long appId;// 应用id

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

}
