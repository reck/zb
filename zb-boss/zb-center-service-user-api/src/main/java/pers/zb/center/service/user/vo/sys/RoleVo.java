package pers.zb.center.service.user.vo.sys;


import pers.zb.center.common.core.enums.Status;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * 
 * 创建日期：2016年8月14日 下午1:50:55
 * 操作用户：zhoubang
 * 
 */
public class RoleVo implements Serializable{
    
    private static final long serialVersionUID = 763228346704155487L;

    private String id;

    private String name;// 名称
    private String description;// 描述
    private Status status;// 状态
    private Date updateTime;// 最后更新时间
    private Date createTime;// 用户创建时间

    private Long appId;//应用id
    private String appName;//所属应用
    private String appIcon;//应用图标
    private String appCode;//应用编码
    private String appStatus;//应用状态

    public String getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(String appIcon) {
        this.appIcon = appIcon;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(String appStatus) {
        this.appStatus = appStatus;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    @SuppressWarnings("unused")
    private String statusName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusName() {
        return status.getDescription();
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @Override
    public int hashCode() {
        return new Long(this.id).intValue();
    }
}



