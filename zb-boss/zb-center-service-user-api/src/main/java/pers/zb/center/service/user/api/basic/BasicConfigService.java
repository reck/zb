package pers.zb.center.service.user.api.basic;


import pers.zb.center.common.core.service.BaseService;
import pers.zb.center.service.user.entity.basic.BasicConfig;

public interface BasicConfigService extends BaseService<BasicConfig> {
    
    /**
     * 根据配置名称获取值
     * @description 
     * 
     * @author zhoubang 
     * @date 2017年3月20日 下午3:00:20 
     * 
     * @param configName
     * @return
     */
    public BasicConfig getConfigByName(String configName);
}
