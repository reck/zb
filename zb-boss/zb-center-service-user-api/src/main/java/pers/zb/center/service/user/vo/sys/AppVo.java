package pers.zb.center.service.user.vo.sys;


import pers.zb.center.common.core.enums.Status;
import pers.zb.center.service.user.enums.OnlineStatus;

import java.io.Serializable;
import java.util.Date;

public class AppVo implements Serializable {

    private static final long serialVersionUID = 851019162980632315L;

    private Long id;
    private String name;// 应用名
    private Status status;// 应用状态
    private int sort;// 应用排序
    private String code;// 应用编码
    private String icon;// 应用图标
    private OnlineStatus online;// 应用是否上线
    @SuppressWarnings("unused")
    private String statusName;// 应用状态描述
    private String onlineStatusName;// 应用上线状态描述

    private Date createTime;
    private Date updateTime;

    public String getOnlineStatusName() {
        return online.getDescription();
    }

    public void setOnlineStatusName(String onlineStatusName) {
        this.onlineStatusName = onlineStatusName;
    }

    public OnlineStatus getOnline() {
        return online;
    }

    public void setOnline(OnlineStatus online) {
        this.online = online;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStatusName() {
        return status.getDescription();
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
