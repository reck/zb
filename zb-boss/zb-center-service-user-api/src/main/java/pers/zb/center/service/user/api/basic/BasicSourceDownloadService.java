package pers.zb.center.service.user.api.basic;


import pers.zb.center.common.core.service.BaseService;
import pers.zb.center.service.user.entity.basic.BasicSourceDownload;

public interface BasicSourceDownloadService extends BaseService<BasicSourceDownload> {
}
