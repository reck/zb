package pers.zb.center.service.user.qo.sys;


import pers.zb.center.common.core.enums.Status;

import java.io.Serializable;

public class AppQo implements Serializable {

    private static final long serialVersionUID = -4815652578119718069L;

    private Long appId;// 应用id
    private String name;// 应用名称
    private Status status;// 应用状态
    private Long[] appIdArr;// 多个应用id的数组
    private int sortNum;// 排序
    private String code;// 编码
    private String icon;// 编码

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long[] getAppIdArr() {
        return appIdArr;
    }

    public void setAppIdArr(Long[] appIdArr) {
        this.appIdArr = appIdArr;
    }

    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
