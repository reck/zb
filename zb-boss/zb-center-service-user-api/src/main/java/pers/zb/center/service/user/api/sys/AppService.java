package pers.zb.center.service.user.api.sys;


import pers.zb.center.common.core.annotation.DataSource;
import pers.zb.center.common.core.enums.DataSourceEnum;
import pers.zb.center.common.core.service.BaseService;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.common.core.vo.ZtreeVo;
import pers.zb.center.service.user.entity.sys.SysApp;
import pers.zb.center.service.user.entity.sys.SysRole;
import pers.zb.center.service.user.qo.sys.AppQo;
import pers.zb.center.service.user.vo.sys.AppVo;

import java.util.List;

public interface AppService extends BaseService<SysApp> {

    /**
     * 获取所有应用列表，并根据字段排序
     * 
     * 创建日期：2017年9月9日  下午8:40:42
     * 操作用户：zhoubang
     * 
     * @param columnName
     * @param orderType
     * @return
     */
    List<SysApp> getAllAppForOrderBy(String columnName, String orderType);

    /**
     * 根据编码获取应用
     *
     * 创建日期：2017年9月9日  下午3:45:54
     * 操作用户：zhoubang
     *
     * @param code
     * @return
     */
    SysApp getAppByCode(String code);

    /**
     * 根据名称获取应用
     *
     * 创建日期：2017年9月9日  下午3:46:02
     * 操作用户：zhoubang
     *
     * @param appName
     * @return
     */
    SysApp getAppByName(String appName);

    /**
     * 获取应用列表————分页查询
     *      这里是使用APO注入的数据源。默认是MYSQL数据源。
     *      如果想切换数据源的访问，你可以在这service的接口方法上，使用@DataSource(DataSourceEnum.MYSQL)注解实现数据源动态切换。
     *      DataSourceEnum 是数据源枚举。目前只测试mysql与sqlserver
     *
     * 创建日期：2017年9月8日  下午10:53:10
     * 操作用户：zhoubang
     *
     * @param pager
     * @param appQo
     * @return
     */
    @DataSource(DataSourceEnum.MYSQL)
    Pager<AppVo> getList(Pager<AppVo> pager, AppQo appQo);


    /**
     * 删除应用
     *
     * 创建日期：2017年9月8日  下午10:54:44
     * 操作用户：zhoubang
     *
     * @param appId
     * @return
     * @throws Exception
     */
    AjaxResult<String> deleteApp(Long appId) throws Exception;

    /**
     * 更新应用信息
     *
     * 创建日期：2017年9月8日  下午10:55:20
     * 操作用户：zhoubang
     *
     * @param appQo
     * @throws Exception
     */
    AjaxResult<String> updateApp(AppQo appQo) throws Exception;

    /**
     * 保存应用
     *
     * 创建日期：2017年9月8日  下午10:55:34
     * 操作用户：zhoubang
     *
     * @param appQo
     * @throws Exception
     */
    AjaxResult<String> saveApp(AppQo appQo) throws Exception;

    /**
     * 批量删除应用
     *
     * 创建日期：2017年9月8日  下午10:55:48
     * 操作用户：zhoubang
     *
     * @param appIdArr
     * @return
     * @throws Exception
     */
    AjaxResult<String> deleteApps(Long[] appIdArr) throws Exception;

    /**
     * 获取角色列表
     * 1、若appId不为空，user为空，则查询该appId下面对应的角色列表
     * 2、若appId为空，user不为空，则查询该userId下面对应的角色列表
     * 3、若appId不为空，user不为空，则查询同时满足appId和userId条件的角色列表
     *
     * 创建日期：2017年9月9日  下午9:21:41
     * 操作用户：zhoubang
     *
     * @param appId
     * @return
     * @throws Exception
     */
    List<SysRole> getRoleListByAppIdUserId(Long appId, Long userId) throws Exception;

    /**
     * 应用tree
     * @param isShowTopParent
     * @return
     */
    List<ZtreeVo> queryAllFormatWithZtree(boolean isShowTopParent,List<AppVo> vos);
}
