package pers.zb.center.service.user.api.sys;


import pers.zb.center.common.core.service.BaseService;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.service.user.entity.sys.SysReUserApp;
import pers.zb.center.service.user.qo.sys.UserQo;

import java.util.List;

public interface ReUserAppService extends BaseService<SysReUserApp> {

    /**
     * 分配应用
     * 
     * 创建日期：2017年9月9日  下午5:24:34
     * 操作用户：zhoubang
     * 
     * @param userQo
     * @return
     * @throws Exception
     */
    AjaxResult<String> assignApp(UserQo userQo) throws Exception;

    /**
     * 获取已经分配的应用
     * 
     * 创建日期：2017年9月9日  下午6:31:05
     * 操作用户：zhoubang
     * 
     * @param userId
     * @return
     * @throws Exception
     */
    List<SysReUserApp> getAppsByUserId(Long userId) throws Exception;

    /**
     * 为用户分配某个应用下的角色
     * 
     * 创建日期：2017年9月9日  下午11:17:41
     * 操作用户：zhoubang
     * 
     * @param userQo
     * @return
     * @throws Exception
     */
    AjaxResult<String> assignRole(UserQo userQo) throws Exception;

}
