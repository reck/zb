package pers.zb.center.service.user.vo.record;


import java.io.Serializable;

/**
 * 系统访问统计VO
 */
public class AccessStatisticsVo implements Comparable<AccessStatisticsVo>,Serializable {
    private String dateVal;
    private int hourVal;
    private int reqNum;
    private int clientNum;

    public String getDateVal() {
        return dateVal;
    }

    public void setDateVal(String dateVal) {
        this.dateVal = dateVal;
    }

    public int getHourVal() {
        return hourVal;
    }

    public void setHourVal(int hourVal) {
        this.hourVal = hourVal;
    }

    public int getReqNum() {
        return reqNum;
    }

    public void setReqNum(int reqNum) {
        this.reqNum = reqNum;
    }

    public int getClientNum() {
        return clientNum;
    }

    public void setClientNum(int clientNum) {
        this.clientNum = clientNum;
    }

    public AccessStatisticsVo() {
    }

    public AccessStatisticsVo(String dateVal, int hourVal, int reqNum, int clientNum) {
        this.dateVal = dateVal;
        this.hourVal = hourVal;
        this.reqNum = reqNum;
        this.clientNum = clientNum;
    }

    @Override
    public String toString() {
        return "AccessStatisticsVo{" +
                "dateVal='" + dateVal + '\'' +
                ", hourVal=" + hourVal +
                ", reqNum=" + reqNum +
                ", clientNum=" + clientNum +
                '}';
    }

    public int compare(AccessStatisticsVo o1, AccessStatisticsVo o2) {
        if(o1.getHourVal() < o2.getHourVal()){
            return -1;
        }else if(o1.getHourVal() > o2.getHourVal()){
            return 1;
        }else{
            return 0;
        }
    }

    public int compareTo(AccessStatisticsVo o) {
        if(o.getHourVal() < getHourVal()){
            return 1;
        }else if(o.getHourVal() > getHourVal()){
            return -1;
        }else{
            return 0;
        }
    }
}
