package pers.zb.center.service.user.api.sys;


import pers.zb.center.common.core.service.BaseService;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.common.core.vo.ZtreeVo;
import pers.zb.center.service.user.entity.sys.SysMenu;

import java.util.List;

public interface MenuService extends BaseService<SysMenu> {

	/**
	 * 获取所有父菜单
	 * @return
	 */
	List<SysMenu> getAllParentList();

	/**
	 * 获取父菜单下面的子菜单
	 * @param id
	 * @return
	 */
	List<SysMenu> getSubMenuByParentId(Long id);

	/**
	 * 获取菜单列表
	 * @param pager
	 * @return
	 */
	Pager<SysMenu> getList(Pager<SysMenu> pager, SysMenu menu);

	/**
	 * 获取所有菜单TREE
	 * @return
	 */
    List<ZtreeVo> queryAllFormatWithZtree(Boolean showTopParent);

	/**
	 * 更新菜单
	 * @param menu
	 * @return
	 */
	AjaxResult<String> updateMenu(SysMenu menu);

	/**
	 * 删除菜单
	 * @param menuId
	 * @return
	 */
    AjaxResult<String> deleteMenu(Long menuId);

	/**
	 * 添加菜单
	 * @param menu
	 * @return
	 */
	AjaxResult<String> saveMenu(SysMenu menu);
}
