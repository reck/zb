package pers.zb.center.service.user.api.record;


import pers.zb.center.common.core.service.BaseService;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.service.user.entity.record.VisitRecord;
import pers.zb.center.service.user.vo.record.AccessStatisticsVo;

import java.util.TreeMap;
import java.util.TreeSet;

public interface VisitRecordService extends BaseService<VisitRecord> {

    /**
     * 根据sessionId获取用户登录信息
     * @param sessionId
     * @return
     * @throws Exception
     */
    VisitRecord selectBySessionId(String sessionId) throws Exception;

    /**
     * 获取上次登录信息
     * @param
     * @return
     * @throws Exception
     */
    VisitRecord getLastLogin(String userName) throws Exception;

    /**
     * 获取用户某日登录次数
     * @param userName
     * @return
     * @throws Exception
     */
    Integer getCurUserDayLoginNum(String userName, String day) throws Exception;

    /**
     * 系统首页访问统计
     * @param dateStr
     * @param dayNum
     * @return
     */
    TreeMap<String,TreeSet<AccessStatisticsVo>> accessStatistics(String dateStr, int dayNum) throws Exception;

    /**
     * 获取访问记录
     * @param pager
     * @param visitRecord
     * @return
     * @throws Exception
     */
    Pager<VisitRecord> getList(Pager<VisitRecord> pager, VisitRecord visitRecord) throws Exception;
}
