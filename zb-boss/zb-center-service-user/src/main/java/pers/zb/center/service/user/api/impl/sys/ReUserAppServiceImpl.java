package pers.zb.center.service.user.api.impl.sys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pers.zb.center.common.core.service.BaseServiceImpl;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.service.user.api.sys.ReUserAppService;
import pers.zb.center.service.user.dao.sys.AppMapper;
import pers.zb.center.service.user.dao.sys.ReUserAppMapper;
import pers.zb.center.service.user.dao.sys.UserMapper;
import pers.zb.center.service.user.dao.sys.UserRoleMapper;
import pers.zb.center.service.user.entity.sys.SysApp;
import pers.zb.center.service.user.entity.sys.SysReUserApp;
import pers.zb.center.service.user.entity.sys.SysUser;
import pers.zb.center.service.user.entity.sys.SysUserRole;
import pers.zb.center.service.user.qo.sys.UserQo;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service("reUserAppServiceImpl")
public class ReUserAppServiceImpl extends BaseServiceImpl<SysReUserApp> implements ReUserAppService {

    @Autowired
    private ReUserAppMapper reUserAppMapper;
    
    @Autowired
    private UserMapper userMapper;
    
    @Autowired
    private AppMapper appMapper;
    
    @Autowired
    private UserRoleMapper userRoleMapper;
    
    @Override
    @Transactional
    public AjaxResult<String> assignApp(UserQo userQo) throws Exception {
        AjaxResult<String> result = new AjaxResult<String>();
        if(userQo.getUserId() == null || "".equals(userQo.getUserId())){
            result.setCode(10002);
            result.setMsg("请指定具体的用户");
            return result;
        }
        
        
        //先删除之前绑定的应用数据：无论是一个也没有指定还是指定了，都需要先删除之前绑定过的，进行清空。
        Example example = new Example(SysReUserApp.class);
        example.createCriteria().andEqualTo("userId", userQo.getUserId());
        reUserAppMapper.deleteByExample(example);
        
        //没有指定应用，需要将该用户与所有的应用进行解绑操作
        if(userQo.getAppIdArr() != null && userQo.getAppIdArr().length > 0){
            for (Long appId : userQo.getAppIdArr()) {
                SysReUserApp sysReUserApp = new SysReUserApp();
                sysReUserApp.setAppId(appId);
                sysReUserApp.setUserId(userQo.getUserId());
                reUserAppMapper.insert(sysReUserApp);
            }
        }
        return result;
    }

    @Override
    public List<SysReUserApp> getAppsByUserId(Long userId) throws Exception {
        Example example = new Example(SysReUserApp.class);
        example.createCriteria().andEqualTo("userId", userId);
        return reUserAppMapper.selectByExample(example);
    }

    @Override
    @Transactional
    public AjaxResult<String> assignRole(UserQo userQo) throws Exception {
        AjaxResult<String> result = new AjaxResult<String>();
        if(userQo.getUserId() == null || "".equals(userQo.getUserId())){
            result.setCode(10002);
            result.setMsg("请指定具体的用户");
            return result;
        }
        if(userQo.getAppId() == null || "".equals(userQo.getAppId())){
            result.setCode(10003);
            result.setMsg("请选择一个应用");
            return result;
        }
        
        //校验用户是否还存在系统中
        SysUser user = userMapper.selectByPrimaryKey(userQo.getUserId());
        if(user == null){
            result.setCode(10004);
            result.setMsg("用户不存在");
            return result;
        }
        
        //校验应用是否存在
        SysApp app = appMapper.selectByPrimaryKey(userQo.getAppId());
        if(app == null){
            result.setCode(10005);
            result.setMsg("系统应用不存在");
            return result;
        }
        
        if(userQo.getRoleIds() == null || userQo.getRoleIds().length <= 0){
            result.setCode(10006);
            result.setMsg("请为用户指定角色");
            return result;
        }
        
        //删除用户在当前应用中拥有的角色
        Example example = new Example(SysUserRole.class);
        example.createCriteria().andEqualTo("userId", userQo.getUserId()).andEqualTo("appId", userQo.getAppId());
        userRoleMapper.deleteByExample(example);
        
        //循环遍历，保存用户角色应用的关系数据
        Long[] roleIds = userQo.getRoleIds();
        for (int i = 0; i < roleIds.length; i++) {
            SysUserRole userRole = new SysUserRole();
            userRole.setAppId(userQo.getAppId());
            userRole.setRoleId(roleIds[i]);
            userRole.setUserId(userQo.getUserId());
            userRoleMapper.insert(userRole);
        }
        
        return result;
    }

}
