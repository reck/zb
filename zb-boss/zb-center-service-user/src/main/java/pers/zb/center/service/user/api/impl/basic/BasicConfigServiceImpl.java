package pers.zb.center.service.user.api.impl.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.zb.center.common.core.service.BaseServiceImpl;
import pers.zb.center.service.user.api.basic.BasicConfigService;
import pers.zb.center.service.user.dao.basic.BasicConfigMapper;
import pers.zb.center.service.user.entity.basic.BasicConfig;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service("basicConfigServiceImpl")
public class BasicConfigServiceImpl extends BaseServiceImpl<BasicConfig> implements BasicConfigService {

    @Autowired
    private BasicConfigMapper basicConfigMapper;

    @Override
    public BasicConfig getConfigByName(String configName) {
        Example example = new Example(BasicConfig.class);
        example.createCriteria().andEqualTo("configName", configName);
        List<BasicConfig>  list = basicConfigMapper.selectByExample(example);
        if(list != null && list.size() > 0){
            return list.get(0);
        }
        return null;
    }
}
