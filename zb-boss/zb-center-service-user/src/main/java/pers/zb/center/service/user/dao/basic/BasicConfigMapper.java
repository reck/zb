package pers.zb.center.service.user.dao.basic;

import pers.zb.center.service.user.entity.basic.BasicConfig;
import tk.mybatis.mapper.common.Mapper;

public interface BasicConfigMapper extends Mapper<BasicConfig> {

}
