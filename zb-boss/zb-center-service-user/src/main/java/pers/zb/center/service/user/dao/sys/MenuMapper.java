package pers.zb.center.service.user.dao.sys;


import org.apache.ibatis.annotations.Param;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.service.user.entity.sys.SysMenu;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface MenuMapper extends Mapper<SysMenu> {

    /**
     * 获取菜单列表
     * @param sysMenu
     * @param pager
     * @return
     */
    List<SysMenu> getList(@Param("menu") SysMenu sysMenu, @Param("pager") Pager<SysMenu> pager);
}
