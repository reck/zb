package pers.zb.center.service.user.dao.sys;

import pers.zb.center.service.user.entity.sys.SysReUserApp;
import tk.mybatis.mapper.common.Mapper;

public interface ReUserAppMapper extends Mapper<SysReUserApp> {

}
