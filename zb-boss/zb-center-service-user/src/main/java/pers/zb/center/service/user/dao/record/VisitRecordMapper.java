package pers.zb.center.service.user.dao.record;

import org.apache.ibatis.annotations.Param;

import pers.zb.center.service.user.entity.record.VisitRecord;
import pers.zb.center.service.user.vo.record.AccessStatisticsVo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface VisitRecordMapper extends Mapper<VisitRecord> {

    /**
     * 获取上次登录的信息
     * @param userName
     * @return
     */
    VisitRecord getLastLogin(@Param("userName") String userName);

    /**
     * 获取用户某日登录次数
     * @param userName
     * @return
     */
    Integer getCurUserDayLoginNum(@Param("userName") String userName,@Param("day") String day);

    /**
     * 获取系统访问统计
     * @param dateList 需要查询的日期列表
     * @return
     */
    List<AccessStatisticsVo> accessStatistics(@Param("dateList") List<String> dateList);
}
