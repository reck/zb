
package pers.zb.center.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pers.zb.center.service.util.SpringContextUtil;


/**
 * 用户服务
 */
public class UserService_Main {

	private static final Log LOG = LogFactory.getLog(UserService_Main.class);

	public static void main(String[] args) {
		try {
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "application/spring-context.xml" });
			// 初始化SpringContextUtil
			SpringContextUtil ctxUtil = new SpringContextUtil();
			ctxUtil.setApplicationContext(context);
			
			context.start();
			LOG.info("UserService Dubbo Service == context start");
			
		} catch (Exception e) {
			LOG.error("[zb-center-service-user] == application start error:", e);
			return;
		}
		
		synchronized (UserService_Main.class) {
			while (true) {
				try {
					UserService_Main.class.wait();
				} catch (InterruptedException e) {
					LOG.error("== synchronized error:", e);
				}
			}
		}
	}
}
