package pers.zb.center.service.user.dao.sys;


import pers.zb.center.service.user.entity.sys.SysRolePermission;
import tk.mybatis.mapper.common.Mapper;

public interface RolePermissionMapper extends Mapper<SysRolePermission> {

}
