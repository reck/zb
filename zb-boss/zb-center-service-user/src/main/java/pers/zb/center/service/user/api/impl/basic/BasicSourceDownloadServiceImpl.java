package pers.zb.center.service.user.api.impl.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.zb.center.common.core.service.BaseServiceImpl;
import pers.zb.center.service.user.api.basic.BasicSourceDownloadService;
import pers.zb.center.service.user.dao.basic.BasicSourceDownloadMapper;
import pers.zb.center.service.user.entity.basic.BasicSourceDownload;

@Service("basicSourceDownloadServiceImpl")
public class BasicSourceDownloadServiceImpl extends BaseServiceImpl<BasicSourceDownload> implements BasicSourceDownloadService {

    @Autowired
    private BasicSourceDownloadMapper basicSourceDownloadMapper;
}
