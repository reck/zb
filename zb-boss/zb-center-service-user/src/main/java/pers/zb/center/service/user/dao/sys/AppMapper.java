package pers.zb.center.service.user.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.service.user.entity.sys.SysApp;
import pers.zb.center.service.user.qo.sys.AppQo;
import pers.zb.center.service.user.vo.sys.AppVo;
import tk.mybatis.mapper.common.Mapper;

public interface AppMapper extends Mapper<SysApp> {

    /**
     * 根据应用名获取应用
     * 
     * 创建日期：2017年9月8日 下午11:03:36 操作用户：zhoubang
     * 
     * @param appName
     * @return
     */
    List<SysApp> getAppByName(@Param("name") String appName);

    /**
     * 根据编码获取应用
     * 
     * 创建日期：2017年9月9日  下午3:44:51
     * 操作用户：zhoubang
     * 
     * @param code
     * @return
     */
    List<SysApp> getAppByCode(@Param("code") String code);
    
    /**
     * 分页获取应用
     * 
     * 创建日期：2017年9月8日 下午11:03:24 操作用户：zhoubang
     * 
     * @param appQo
     * @param pager
     * @return
     */
    List<AppVo> getAppList(@Param("appQo") AppQo appQo, @Param("pager") Pager<AppVo> pager);

}
