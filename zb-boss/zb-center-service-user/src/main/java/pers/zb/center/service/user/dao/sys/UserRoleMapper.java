package pers.zb.center.service.user.dao.sys;

import pers.zb.center.service.user.entity.sys.SysUserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<SysUserRole> {


}
