package pers.zb.center.service.user.api.impl.sys;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pers.zb.center.common.core.enums.Status;
import pers.zb.center.common.core.service.BaseServiceImpl;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.common.core.vo.ZtreeVo;
import pers.zb.center.service.user.api.sys.MenuService;
import pers.zb.center.service.user.dao.sys.MenuMapper;
import pers.zb.center.service.user.entity.sys.SysMenu;
import pers.zb.center.service.user.enums.MenuType;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("menuServiceImpl")
public class MenuServiceImpl extends BaseServiceImpl<SysMenu> implements MenuService {

	@Autowired
	private MenuMapper menuMapper;

	@Override
	public List<SysMenu> getAllParentList() {
		Example example = new Example(SysMenu.class);
		example.createCriteria().andEqualTo("parentId", 0).andEqualTo("status",Status.ENABLE);
		example.setOrderByClause("sort ASC");
		return menuMapper.selectByExample(example);
	}

	@Override
	public List<SysMenu> getSubMenuByParentId(Long id) {
		Example example = new Example(SysMenu.class);
		example.createCriteria().andEqualTo("parentId", id);
		example.setOrderByClause("sort ASC");//排序值升序排序
		return menuMapper.selectByExample(example);
	}

	@Override
	public Pager<SysMenu> getList(Pager<SysMenu> pager, SysMenu menu) {
		if(pager.getUsePager()){
			PageHelper.offsetPage(pager.getOffset(), pager.getLimit());
		}
		List<SysMenu> vos = menuMapper.getList(menu,pager);
		pager.setRows(vos);
		PageInfo<SysMenu> pageInfo=new PageInfo<SysMenu>(vos);
		pager.setTotal(pageInfo.getTotal());
		return pager;
	}

	@Override
	public List<ZtreeVo> queryAllFormatWithZtree(Boolean showTopParent) {
		List<ZtreeVo> results = new ArrayList<ZtreeVo>();

		Example example = new Example(SysMenu.class);
		example.createCriteria().andEqualTo("status",Status.ENABLE);//只获取可用状态的菜单
		List<SysMenu> menus = menuMapper.selectByExample(example);

		if(showTopParent){
			ZtreeVo result = new ZtreeVo();
			result.setId("-1");
			result.setpId("0");
			result.setName("无上级权限");
			results.add(result);
		}

		if (CollectionUtils.isNotEmpty(menus)) {
			for (SysMenu menu : menus) {
				ZtreeVo foo = new ZtreeVo();
				foo.setId(String.valueOf(menu.getId()));
				foo.setpId(String.valueOf(menu.getParentId()));
				foo.setName(menu.getName());
				results.add(foo);
			}
		}
		return results;
	}

	@Override
	@Transactional(propagation= Propagation.REQUIRED,rollbackFor=Exception.class)
	public AjaxResult<String> updateMenu(SysMenu menu) {
		AjaxResult<String> result = new AjaxResult<String>();
		if(menu.getType() == MenuType.MENU){//子菜单
			menu.setIcon("");
		}else {
			menu.setUrl("#");
			menu.setParentId(0L);
			if(StringUtils.isBlank(menu.getIcon())){
				menu.setIcon("fa fa-bars");//默认的菜单图标
			}
		}
		int num = menuMapper.updateByPrimaryKeySelective(menu);
		if(num <= 0){
			result.setCode(10001);
			result.setMsg("菜单更新失败");
		}
		return result;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public AjaxResult<String> deleteMenu(Long menuId) {
		AjaxResult<String> result = new AjaxResult<String>();
		if(menuId == null || StringUtils.isBlank(String.valueOf(menuId))){
			result.setCode(10001);
			result.setMsg("菜单删除失败[menuId为空]");
			return result;
		}
		SysMenu menu = get(menuId);
		menu.setStatus(Status.DISABLE);//为了不影响实际项目功能的使用，菜单不做物理删除。如果手误删除错误，可以手动去数据库中更改即可
		menuMapper.updateByPrimaryKeySelective(menu);

		//判断是否存在子菜单，一并更新为禁用状态
		List<SysMenu> subList = getSubMenuByParentId(menuId);
		if(subList != null && subList.size() > 0){
			for (SysMenu subMenu : subList) {
				subMenu.setStatus(Status.DISABLE);
				menuMapper.updateByPrimaryKeySelective(subMenu);
			}
		}
		return result;
	}

    @Override
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
    public AjaxResult<String> saveMenu(SysMenu menu) {
		AjaxResult<String> result = new AjaxResult<String>();
		if(menu.getType() == MenuType.MENU){//子菜单
			menu.setIcon("");
		}else {
			menu.setUrl("#");
			menu.setParentId(0L);
			if(StringUtils.isBlank(menu.getIcon())){
				menu.setIcon("fa fa-bars");//默认的菜单图标
			}
		}
		menu.setStatus(Status.ENABLE);
		menu.setCreateTime(new Date());
		menu.setUpdateTime(new Date());
		int num = menuMapper.insert(menu);
		if(num <= 0){
			result.setCode(10001);
			result.setMsg("菜单添加失败");
		}
		return result;
    }
}
