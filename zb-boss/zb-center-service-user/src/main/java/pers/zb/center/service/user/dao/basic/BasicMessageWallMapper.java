package pers.zb.center.service.user.dao.basic;

import org.apache.ibatis.annotations.Param;
import pers.zb.center.service.user.entity.basic.BasicMessageWall;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface BasicMessageWallMapper extends Mapper<BasicMessageWall> {

    /**
     * 获取指定数量的留言列表
     * @param num
     * @return
     */
    List<BasicMessageWall> getMessageList(@Param("num") int num);
}
