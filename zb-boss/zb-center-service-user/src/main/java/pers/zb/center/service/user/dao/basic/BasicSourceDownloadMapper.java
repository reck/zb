package pers.zb.center.service.user.dao.basic;

import pers.zb.center.service.user.entity.basic.BasicSourceDownload;
import tk.mybatis.mapper.common.Mapper;

public interface BasicSourceDownloadMapper extends Mapper<BasicSourceDownload> {

}
