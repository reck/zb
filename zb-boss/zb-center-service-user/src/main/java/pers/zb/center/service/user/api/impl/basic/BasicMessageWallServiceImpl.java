package pers.zb.center.service.user.api.impl.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.zb.center.common.core.service.BaseServiceImpl;
import pers.zb.center.service.user.api.basic.BasicMessageWallService;
import pers.zb.center.service.user.dao.basic.BasicMessageWallMapper;
import pers.zb.center.service.user.entity.basic.BasicMessageWall;

import java.util.List;

@Service("basicMessageWallServiceImpl")
public class BasicMessageWallServiceImpl extends BaseServiceImpl<BasicMessageWall> implements BasicMessageWallService {

    @Autowired
    private BasicMessageWallMapper basicMessageWallMapper;

    @Override
    public List<BasicMessageWall> getMessageList(int num) {
        List<BasicMessageWall>  list = basicMessageWallMapper.getMessageList(num);
        return list;
    }
}
