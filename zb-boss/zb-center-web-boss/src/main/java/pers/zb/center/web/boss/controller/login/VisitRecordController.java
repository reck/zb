package pers.zb.center.web.boss.controller.login;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pers.zb.center.common.core.util.JsonUtil;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.common.core.vo.JQDatatableResult;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.service.user.api.record.VisitRecordService;
import pers.zb.center.service.user.entity.record.VisitRecord;
import pers.zb.center.service.user.vo.record.AccessStatisticsVo;

import java.util.TreeMap;
import java.util.TreeSet;

@Controller
@RequestMapping(value="/access/record")
public class VisitRecordController {

    @Autowired
    private VisitRecordService visitRecordService;

    /**
     * 获取系统访问统计数据
     * @param
     * @return
     */
    @RequestMapping(value = "/accessStatistics/{dateStr}/{dayNum}",method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult<String> accessStatistics(@PathVariable String dateStr, @PathVariable int dayNum) {
        AjaxResult<String> result = new AjaxResult<String>();

        TreeMap<String, TreeSet<AccessStatisticsVo>> treeMapDate = null;
        try {
            treeMapDate = visitRecordService.accessStatistics(dateStr,dayNum);
            result.setResult(JsonUtil.toJson(treeMapDate));
        } catch (Exception e) {
            result.setCode(500);
            result.setMsg("获取系统访问统计数据出现错误：" + e.getMessage());
        }
        return result;
    }


    /**
     * 获取访问记录
     * @param pager
     * @param visitRecord
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public JQDatatableResult<VisitRecord> getAccessRecordList(@ModelAttribute Pager<VisitRecord> pager, @ModelAttribute VisitRecord visitRecord) throws Exception {
        pager = visitRecordService.getList(pager, visitRecord);

        JQDatatableResult<VisitRecord> result = new JQDatatableResult<VisitRecord>();
        result.setData(pager.getRows());
        result.setRecordsTotal(pager.getTotal().intValue());
        result.setDraw(pager.getDraw());
        result.setRecordsFiltered(pager.getTotal().intValue());
        return result;
    }
}
