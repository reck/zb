package pers.zb.center.web.boss.controller.sys;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pers.zb.center.common.core.enums.Status;
import pers.zb.center.common.core.util.JsonUtil;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.common.core.vo.ZtreeVo;
import pers.zb.center.service.user.api.sys.AppService;
import pers.zb.center.service.user.api.sys.MenuService;
import pers.zb.center.service.user.entity.sys.SysMenu;
import pers.zb.center.service.user.enums.MenuType;
import pers.zb.center.service.user.qo.sys.AppQo;
import pers.zb.center.service.user.vo.sys.AppVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * 应用管理
 * 
 * @author zhoubang
 *
 */
@Controller
@RequestMapping("/menu")
public class MenuController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MenuService menuService;

    @Autowired
    private AppService appService;

    /**
     * 进入菜单列表页面
     * 
     * 创建日期：2017年9月8日 下午11:23:52 操作用户：zhoubang
     * 
     * @param request
     * @param response
     * @param map
     * @return
     */
    @RequestMapping("/toMenuListView")
    @RequiresPermissions("menu:list")
    public String toMenuListView(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
        //应用系统列表
        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);
        map.put("apps", vos.getRows());
        return "/sys/menu/list";
    }

    /**
     * 获取菜单列表
     * 
     * 创建日期：2017年9月8日 下午11:25:58 操作用户：zhoubang
     * 
     * @return
     */
    @RequestMapping("/list")
    @RequiresPermissions("menu:list")
    @ResponseBody
    public List<SysMenu> list(SysMenu sysMenu) {
        Pager<SysMenu> pager = new Pager<SysMenu>();
        pager.setLimit(Integer.MAX_VALUE);

        Pager<SysMenu> vos = menuService.getList(pager,sysMenu);
        if(vos != null){
            return vos.getRows();
        }
        return null;
    }

    /**
     * 进入菜单信息编辑页面
     * 
     * 创建日期：2017年9月8日 下午11:29:50 操作用户：zhoubang
     * 
     * @param map
     * @return
     */
    @RequestMapping("/toEditView")
    @RequiresPermissions("menu:edit")
    public String toEditView(ModelMap map, Long menuId) {
        map.put("menuId",menuId);
        map.put("menuType", MenuType.values());
        map.put("status", Status.values());
        if(null != menuId){
            //获取菜单对象
            SysMenu menu = menuService.get(menuId);
            map.put("menu",menu);

            //获取父菜单
            SysMenu parentMenu = menuService.get(menu.getParentId());
            map.put("parentMenu",parentMenu);
        }
        return "/sys/menu/edit";
    }

    /**
     * 进入添加菜单页面
     * @param map
     * @return
     */
    @RequestMapping("/toAddView")
    @RequiresPermissions("menu:add")
    public String toAddView(ModelMap map) {
        //应用系统列表
        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);
        map.put("apps", vos.getRows());

        map.put("menuType", MenuType.values());
        map.put("status", Status.values());
        return "/sys/menu/add";
    }


    /**
     * 进入选择父菜单的页面
     * @param map
     * @param menuId
     * @return
     */
    @RequestMapping("/toSelecrParentMenuView")
    @RequiresPermissions("menu:list")
    public String toSelecrParentMenuView(ModelMap map, Long menuId) {
        map.put("menuId",menuId);
        SysMenu menu = menuService.get(menuId);
        map.put("menu",menu);
        return "/sys/menu/selectParentMenu";
    }

    /**
     * 获取所有菜单列表Tree
     * @param open 是否展开节点
     * @param menuId 菜单id
     * @return
     */
    @RequestMapping("/getMenuTree")
    @ResponseBody
    public String getPermissionTree(Boolean open,Long menuId,Boolean showTopParent) {
        //获取对应应用下的权限列表
        List<ZtreeVo> allPermission = menuService.queryAllFormatWithZtree(showTopParent);
        //json字符串包含权限的复选框选中属性设置以及展开属性设置.
        return JsonUtil.toJson(setPZtreeCheck(allPermission,open,menuId));
    }

    /**
     * 对菜单tree进行操作
     *
     * 作者: zhoubang
     * 日期：2015年4月29日 上午9:52:16
     * @return
     */
    public static List<ZtreeVo> setPZtreeCheck(List<ZtreeVo> menuList,Boolean open,Long menuId){
        for (ZtreeVo ztreeVo : menuList) {
            if (ztreeVo.getId().equals(String.valueOf(menuId))){
                ztreeVo.setChecked(true);
            }else {
                ztreeVo.setChecked(false);
            }
            ztreeVo.setOpen(open);
        }
        return menuList;
    }


    /**
     * 更新菜单
     * @param menu
     * @return
     */
    @RequestMapping("/updateMenu")
    @RequiresPermissions("menu:update")
    @ResponseBody
    public AjaxResult<String> updateMenu(@ModelAttribute SysMenu menu) {
        logger.debug("更新菜单信息，menu:" + JsonUtil.toJson(menu));

        AjaxResult<String> result = new AjaxResult<String>();
        try {
            // 更新菜单信息
            result = menuService.updateMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();

            result.setCode(10005);
            result.setMsg("更新失败");
            return result;
        }
        return result;
    }

    /**
     * 删除菜单
     * @param menuId
     * @return
     */
    @RequestMapping("/deleteMenu")
    @RequiresPermissions("menu:delete")
    @ResponseBody
    public AjaxResult<String> deleteMenu(Long menuId) {
        logger.debug("删除菜单，menuId:" + menuId);

        AjaxResult<String> result = new AjaxResult<String>();
        try {
            result = menuService.deleteMenu(menuId);
        } catch (Exception e) {
            e.printStackTrace();

            result.setCode(10003);
            result.setMsg("删除失败");
            return result;
        }
        return result;
    }


    /**
     * 添加菜单
     * @param menu
     * @return
     */
    @RequestMapping("/saveMenu")
    @RequiresPermissions("menu:add")
    @ResponseBody
    public AjaxResult<String> saveMenu(SysMenu menu) {
        logger.debug("添加菜单，menu:" + JsonUtil.toJson(menu));

        AjaxResult<String> result = new AjaxResult<String>();
        try {
            result = menuService.saveMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();

            result.setCode(10003);
            result.setMsg("添加失败");
            return result;
        }
        return result;
    }
}
