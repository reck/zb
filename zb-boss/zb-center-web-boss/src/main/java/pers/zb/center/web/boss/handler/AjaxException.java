package pers.zb.center.web.boss.handler;

import pers.zb.center.common.core.vo.AjaxResult;


public class AjaxException extends Exception{
    public AjaxException() {
    }

    public AjaxException(String message) {
        super(message);
    }


    public AjaxException(Throwable cause) {
        super(cause);
    }

    public AjaxException(String message, Throwable cause) {
        super(message, cause);
    }

}
