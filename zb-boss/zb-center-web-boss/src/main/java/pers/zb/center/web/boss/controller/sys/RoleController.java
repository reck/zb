package pers.zb.center.web.boss.controller.sys;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pers.zb.center.common.core.enums.Status;
import pers.zb.center.common.core.util.JsonUtil;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.common.core.vo.JQDatatableResult;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.common.core.vo.ZtreeVo;
import pers.zb.center.service.user.api.sys.AppService;
import pers.zb.center.service.user.api.sys.PermissionService;
import pers.zb.center.service.user.api.sys.RolePermissionService;
import pers.zb.center.service.user.api.sys.RoleService;
import pers.zb.center.service.user.entity.sys.SysRole;
import pers.zb.center.service.user.entity.sys.SysRolePermission;
import pers.zb.center.service.user.qo.sys.AppQo;
import pers.zb.center.service.user.qo.sys.RoleQo;
import pers.zb.center.service.user.vo.sys.AppVo;
import pers.zb.center.service.user.vo.sys.RoleVo;


/**
 * 
 * 角色管理
 * 
 * 创建日期：2016年8月13日 下午4:07:01
 * 操作用户：zhoubang
 *
 */
@Controller
@RequestMapping("/role")
public class RoleController {
    
    private Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private RoleService roleService;
    
    @Autowired
    private PermissionService permissionService;
    
    @Autowired
    private RolePermissionService rolePermissionService;
    
    @Autowired
    private AppService appService;
    
    /**
     * 进入角色列表页面
     * 
     * 日期：2016年8月13日 下午4:07:06
     * 用户：zhoubang
     * 
     * @param request
     * @param response
     */
    @RequestMapping("/toRoleListView")
    @RequiresPermissions("role:list")
    public String toRoleListView(HttpServletRequest request, HttpServletResponse response,ModelMap map) {
        //角色状态
        map.put("status", Status.values());
        
        //获取所有系统列表
        map.put("apps", appService.getAllAppForOrderBy("sort", "asc"));

        //应用系统列表
        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);
        map.put("apps", vos.getRows());

        List<ZtreeVo> allApps = appService.queryAllFormatWithZtree(false,vos.getRows());
        // json字符串包含角色的复选框选中属性设置以及展开属性设置.
        map.put("allApps", JsonUtil.toJson(setPZtreeCheck(allApps, null)));

        return "/sys/role/list";
    }
    
    /**
     * 分页获取角色列表
     * 
     * 日期：2016年8月14日 下午1:18:43
     * 用户：zhoubang
     * 
     * @param pager
     * @return
     */
    @RequestMapping("/list")
    @RequiresPermissions("role:list")
    @ResponseBody
    public JQDatatableResult<RoleVo> list(Pager<RoleVo> pager, SysRole role) {
        pager = roleService.getList(pager, role);

        JQDatatableResult<RoleVo> result = new JQDatatableResult<RoleVo>();
        result.setData(pager.getRows());
        result.setRecordsTotal(pager.getTotal().intValue());
        result.setDraw(pager.getDraw());
        result.setRecordsFiltered(pager.getTotal().intValue());
        return result;
    }
    
    /**
     * 进入添加角色页面
     * 
     * 日期：2016年8月14日 下午2:11:53
     * 用户：zhoubang
     * 
     * @param modelMap
     * @return
     */
    @RequestMapping("/toAddView")
    @RequiresPermissions("role:add")
    public String toAddView(ModelMap modelMap) {
        //获取所有权限列表
        //List<ZtreeVo> allPermission = permissionService.queryAllFormatWithZtree(true,appId);
        
        //json字符串包含权限的复选框选中属性设置以及展开属性设置.
        //modelMap.put("permissions", JsonUtil.toJson(setPZtreeCheck(allPermission, null)));

        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);
        modelMap.put("apps", vos.getRows());

        modelMap.put("status", Status.values());

        //modelMap.put("apps", appService.getAllAppForOrderBy("sort", "asc"));
        //modelMap.put("appObj", appService.get(appId));
        return "/sys/role/add";
    }
    
    /**
     * 对角色的权限tree进行操作
     *      对角色拥有的权限进行复选框选中的属性设置,并且展开对应的权限tree。
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午9:52:16
     * @param list
     * @return
     */
    public static List<ZtreeVo> setPZtreeCheck(List<ZtreeVo> list ,List<SysRolePermission> rolePermissionList){
        for (ZtreeVo ztreeVo : list) {
            if(rolePermissionList != null){
                for (SysRolePermission rolePermission : rolePermissionList) {
                    if(StringUtils.equals(String.valueOf(rolePermission.getPermissionId()), ztreeVo.getId())){
                        ztreeVo.setChecked(true);
                    }
                }
            }else{
                ztreeVo.setChecked(false);
            }
            
            ztreeVo.setOpen(true);
        }
        return list;
    }
    
    /**
     * 保存角色
     * 
     * 创建日期：2016年8月4日 下午1:50:10
     * 操作用户：zhoubang
     * 
     * @param role
     * @param permissionIds
     * @return
     */
    @RequestMapping("/addRole")
    @RequiresPermissions("role:add")
    @ResponseBody
    public AjaxResult<String> add(SysRole role, Long[] permissionIds) {
        AjaxResult<String> result = new AjaxResult<String>();
        
        if (StringUtils.isBlank(role.getName())) {
            result.setCode(10001);
            result.setMsg("请填写角色名称");
            return result;
        }
        if (StringUtils.isBlank(role.getDescription())) {
            result.setCode(10002);
            result.setMsg("请填写角色描述");
            return result;
        }
        if (permissionIds == null || permissionIds.length == 0) {
            result.setCode(10003);
            result.setMsg("请至少选择一个权限");
            return result;
        }
        if (role.getAppId() == null) {
            result.setCode(10005);
            result.setMsg("请指定一个系统应用");
            return result;
        }
        try {
            roleService.addRole(role,permissionIds);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(10004);
            result.setMsg("添加失败");
            return result;
        }
        return result;
    }
    
    /**
     * 进入角色编辑页面
     * 
     * 日期：2016年8月20日 下午5:01:36
     * 用户：zhoubang
     * 
     * @param qo
     * @return
     */
    @RequestMapping("/toEditView")
    @RequiresPermissions("role:edit")
    public String toEditView(ModelMap map,RoleQo qo) {
        logger.debug("角色编辑：qo：" + JsonUtil.toJson(qo));
        
        //获取应用下的所有权限列表
        //List<ZtreeVo> allPermission = permissionService.queryAllFormatWithZtree(true,qo.getAppId());
        //角色拥有的权限
        //List<SysRolePermission> rolePermissions = rolePermissionService.getPermissionByRoleId(qo.getRoleId());
        //json字符串包含权限的复选框选中属性设置以及展开属性设置.
        //map.put("permissions", JsonUtil.toJson(setPZtreeCheck(allPermission, rolePermissions)));
        
        //角色对象
        map.put("role", roleService.get(qo.getRoleId()));
        map.put("status", Status.values());

        //应用列表
        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);
        map.put("apps", vos.getRows());

        return "/sys/role/edit";
    }

    /**
     * 获取角色的权限列表
     * @param appId
     * @return
     */
    @RequestMapping("/getPermissionTree")
    @ResponseBody
    public String getPermissionTree(Long roleId,Long appId,Boolean showTopParent,Boolean open) {
        //获取对应应用下的权限列表
        List<ZtreeVo> allPermission = permissionService.queryAllFormatWithZtree(showTopParent, appId);
        //角色拥有的权限
        List<SysRolePermission> rolePermissions = rolePermissionService.getPermissionByRoleId(roleId);
        //json字符串包含权限的复选框选中属性设置以及展开属性设置.
        return JsonUtil.toJson(setPZtreeCheck(allPermission, rolePermissions));
    }


    /**
     * 更新角色信息
     * 
     * 日期：2016年8月20日 下午5:21:04
     * 用户：zhoubang
     * 
     * @param role
     * @param permissionIds
     * @return
     */
    @RequestMapping("/updateRole")
    @RequiresPermissions("role:update")
    @ResponseBody
    public AjaxResult<String> updateRole(SysRole role, Long[] permissionIds) {
        AjaxResult<String> result = new AjaxResult<String>();
        
        if (StringUtils.isBlank(role.getName())) {
            result.setCode(10001);
            result.setMsg("请填写角色名称");
            return result;
        }
        
        if (StringUtils.isBlank(role.getDescription())) {
            result.setCode(10002);
            result.setMsg("请填写角色描述");
            return result;
        }
        
        if(permissionIds == null || permissionIds.length <= 0){
            result.setCode(10003);
            result.setMsg("请为角色指定权限");
            return result;
        }
        
        if(role.getId() == null){
            result.setCode(10004);
            result.setMsg("角色ID不存在");
            return result;
        }
        
        SysRole roleObj = roleService.get(role.getId());
        if(roleObj == null){
            result.setCode(10005);
            result.setMsg("该角色不存在");
            return result;
        }
        
        //更新角色
        try {
            roleService.updateRole(role,permissionIds);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(10006);
            result.setMsg("更新失败");
            return result;
        }
        
        return result;
    }
    
    /**
     * 删除角色
     * 
     * 日期：2016年8月20日 下午5:53:16
     * 用户：zhoubang
     * 
     * @param roleId
     * @return
     */
    @RequestMapping("/deleteRole")
    @RequiresPermissions("role:delete")
    @ResponseBody
    public AjaxResult<String> deleteRole(Long roleId) {
        AjaxResult<String> result = new AjaxResult<String>();
        //删除角色
        try {
            result = roleService.deleteRole(roleId);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(10003);
            result.setMsg("角色删除失败");
            return result;
        }
        return result;
    }
    
}
