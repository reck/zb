package pers.zb.center.web.boss.controller.sys;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pers.zb.center.common.core.enums.Status;
import pers.zb.center.common.core.util.JsonUtil;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.common.core.vo.JQDatatableResult;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.common.core.vo.ZtreeVo;
import pers.zb.center.service.user.api.sys.AppService;
import pers.zb.center.service.user.entity.sys.SysApp;
import pers.zb.center.service.user.entity.sys.SysRole;
import pers.zb.center.service.user.qo.sys.AppQo;
import pers.zb.center.service.user.vo.sys.AppVo;


/**
 * 应用管理
 * 
 * @author zhoubang
 *
 */
@Controller
@RequestMapping("/app")
public class AppController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AppService appService;

    /**
     * 进入应用列表页面
     * 
     * 创建日期：2017年9月8日 下午11:23:52 操作用户：zhoubang
     * 
     * @param request
     * @param response
     * @param map
     * @return
     */
    @RequestMapping("/toAppListView")
    @RequiresPermissions("app:list")
    public String toAppListView(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
        // 应用状态
        map.put("status", Status.values());

        return "/sys/app/list";
    }

    /**
     * 分页获取应用列表
     * 
     * 创建日期：2017年9月8日 下午11:25:58 操作用户：zhoubang
     * 
     * @param pager
     * @param appQo
     * @return
     */
    @RequestMapping("/list")
    @RequiresPermissions("app:list")
    @ResponseBody
    public JQDatatableResult<AppVo> list(@ModelAttribute Pager<AppVo> pager, @ModelAttribute AppQo appQo) {
        pager = appService.getList(pager, appQo);

        JQDatatableResult<AppVo> result = new JQDatatableResult<AppVo>();
        result.setData(pager.getRows());
        result.setRecordsTotal(pager.getTotal().intValue());
        result.setDraw(pager.getDraw());
        result.setRecordsFiltered(pager.getTotal().intValue());
        return result;
    }

    /**
     * 删除应用
     * 
     * 创建日期：2017年9月8日 下午11:26:48 操作用户：zhoubang
     * 
     * @return
     */
    @RequestMapping("/deleteApp")
    @RequiresPermissions("app:delete")
    @ResponseBody
    public AjaxResult<String> deleteApp(@ModelAttribute AppQo appQo) {
        logger.debug("删除应用，appQo:" + JsonUtil.toJson(appQo));

        AjaxResult<String> result = new AjaxResult<String>();
        try {
            // 删除应用
            result = appService.deleteApp(appQo.getAppId());
        } catch (Exception e) {
            e.printStackTrace();

            result.setCode(10003);
            result.setMsg("删除失败");
            return result;
        }
        return result;
    }

    /**
     * 批量删除应用
     * 
     * 创建日期：2017年9月8日 下午11:28:43 操作用户：zhoubang
     * 
     * @param appQo
     * @return
     */
    @RequestMapping("/deleteApps")
    @RequiresPermissions("app:delete")
    @ResponseBody
    public AjaxResult<String> deleteApps(@ModelAttribute AppQo appQo) {
        logger.debug("批量删除应用，appQo:" + JsonUtil.toJson(appQo));

        AjaxResult<String> result = new AjaxResult<String>();
        try {
            // 批量删除应用
            result = appService.deleteApps(appQo.getAppIdArr());
        } catch (Exception e) {
            e.printStackTrace();

            result.setCode(10003);
            result.setMsg("删除失败");
            return result;
        }
        return result;
    }

    /**
     * 进入应用信息编辑页面
     * 
     * 创建日期：2017年9月8日 下午11:29:50 操作用户：zhoubang
     * 
     * @param map
     * @param appQo
     * @return
     */
    @RequestMapping("/toEditView")
    @RequiresPermissions("app:edit")
    public String toEditView(ModelMap map, AppQo appQo) {
        if (appQo.getAppId() != null) {
            SysApp sysApp = appService.get(appQo.getAppId());
            map.put("sysApp", sysApp);// 应用信息
        }
        map.put("status", Status.values());// 应用状态
        return "/sys/app/edit";
    }

    /**
     * 更新应用信息
     * 
     * 创建日期：2017年9月8日 下午11:31:01 操作用户：zhoubang
     * 
     * @param appQo
     * @return
     */
    @RequestMapping("/updateApp")
    @RequiresPermissions("app:edit")
    @ResponseBody
    public AjaxResult<String> updateApp(@ModelAttribute AppQo appQo) {
        logger.debug("更新应用信息，appQo:" + JsonUtil.toJson(appQo));

        AjaxResult<String> result = new AjaxResult<String>();
        try {
            // 更新应用信息
            result = appService.updateApp(appQo);
        } catch (Exception e) {
            e.printStackTrace();

            result.setCode(10005);
            result.setMsg("更新失败");
            return result;
        }
        return result;
    }

    /**
     * 进入添加应用页面
     * 
     * 创建日期：2017年9月8日 下午11:33:18 操作用户：zhoubang
     * 
     * @param map
     * @return
     */
    @RequestMapping("/toAddView")
    @RequiresPermissions("app:add")
    public String toAddView(ModelMap map) {
        map.put("status", Status.values());
        return "/sys/app/add";
    }

    /**
     * 新增应用
     * 
     * 日期：2016年8月20日 下午4:39:13 用户：zhoubang
     * 
     * @return
     */
    @RequestMapping("/addApp")
    @RequiresPermissions("app:add")
    @ResponseBody
    public AjaxResult<String> addApp(@ModelAttribute AppQo appQo) {
        logger.debug("新增应用，appQo:" + JsonUtil.toJson(appQo));

        AjaxResult<String> result = new AjaxResult<String>();

        // 保存应用
        try {
            result = appService.saveApp(appQo);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(10005);
            result.setMsg("添加失败");
            return result;
        }
        return result;
    }
    
    /**
     * 获取应用所包含的角色列表
     * 
     * 创建日期：2017年9月9日  下午9:20:12
     * 操作用户：zhoubang
     * 
     * @param appId
     * @return
     */
    @RequestMapping("/role/list")
    @ResponseBody
    public List<SysRole> appRoleList(Long appId, Long userId) {
        logger.debug("获取应用所包含的角色列表，appId:" + appId + ",userId:" + userId);

        try {
            //获取应用所包含的角色列表
            List<SysRole> roleList = appService.getRoleListByAppIdUserId(appId,userId);
            return roleList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取应用详情
     * @param appId
     * @return
     */
    @RequestMapping("/detail/{appId}")
    @RequiresPermissions("app:list")
    @ResponseBody
    public SysApp getAppDetail(@PathVariable Long appId) {
        logger.debug("获取应用详情信息，appId:" + appId + ",appId:" + appId);

        try {
            SysApp sysApp = appService.get(appId);
            return sysApp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取应用tree
     * @param showTopParent 是否显示顶层节点：《全部》
     * @param open 是否展开
     * @param enable 是否只获取可用状态的应用
     * @return
     */
    @RequestMapping("/apps/tree")
    @ResponseBody
    public String getPermissionTree(Boolean showTopParent,Boolean open,Boolean enable) {
        //应用系统列表
        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);

        List<ZtreeVo> allApps = appService.queryAllFormatWithZtree(showTopParent,vos.getRows());
        //json字符串包含权限的复选框选中属性设置以及展开属性设置.
        return JsonUtil.toJson(setPZtreeCheck(allApps,open));
    }

    /**
     * 对权限tree进行操作
     * 对权限进行复选框选中的属性设置,并且展开对应的权限tree。
     * <p>
     * 作者: zhoubang
     * 日期：2015年4月29日 上午9:52:16
     *
     * @param list
     * @return
     */
    public static List<ZtreeVo> setPZtreeCheck(List<ZtreeVo> list,Boolean open) {
        for (ZtreeVo ztreeVo : list) {
            ztreeVo.setChecked(false);
            ztreeVo.setOpen(open);//是否展开
        }
        return list;
    }

}
