package pers.zb.center.web.boss.controller.ws.webservice.core;

public class CommonResponse extends AbstractResponse implements
        IResponse<CommonResponse> {

    private int resultCode;
    private String resultDesc;

    @Override
    public int getResultCode() {
        return resultCode;
    }

    @Override
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    @Override
    public String getResultDesc() {
        return resultDesc;
    }

    @Override
    public void setResultDesc(String resultDesc) {
        this.resultDesc = resultDesc;
    }

}
