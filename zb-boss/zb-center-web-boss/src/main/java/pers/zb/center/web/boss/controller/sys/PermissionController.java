package pers.zb.center.web.boss.controller.sys;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pers.zb.center.common.core.enums.Status;
import pers.zb.center.common.core.util.JsonUtil;
import pers.zb.center.common.core.vo.AjaxResult;
import pers.zb.center.common.core.vo.Pager;
import pers.zb.center.common.core.vo.ZtreeVo;
import pers.zb.center.service.user.api.sys.AppService;
import pers.zb.center.service.user.api.sys.PermissionService;
import pers.zb.center.service.user.entity.sys.SysPermission;
import pers.zb.center.service.user.entity.sys.SysRole;
import pers.zb.center.service.user.qo.sys.AppQo;
import pers.zb.center.service.user.vo.sys.AppVo;
import pers.zb.center.service.user.vo.sys.PermissionVo;

import javax.annotation.Resource;
import java.util.List;


/**
 * 角色管理
 * <p>
 * <p>
 * 创建日期：2016年8月4日 上午9:39:35 操作用户：zhoubang
 */
@Controller
@RequestMapping("/permission")
public class PermissionController {

    @Resource
    private PermissionService permissionService;

    @Autowired
    private AppService appService;

    /**
     * 进入权限管理页面
     * <p>
     * 创建日期：2016年8月4日 上午9:39:59
     * 操作用户：zhoubang
     *
     * @param map
     * @return
     */
    @RequestMapping("/toListView")
    @RequiresPermissions("permission:list")
    public String toListView(ModelMap map) {
        map.put("status", Status.values());

        //获取所有系统列表
        //map.put("apps", appService.getAllAppForOrderBy("sort", "asc"));

        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);
        map.put("apps", vos.getRows());
        List<ZtreeVo> allApps = appService.queryAllFormatWithZtree(false,vos.getRows());
        // json字符串包含角色的复选框选中属性设置以及展开属性设置.
        map.put("allApps", JsonUtil.toJson(setPZtreeCheck(allApps, null, null)));

        return "/sys/permission/list";
    }

    /**
     * 对角色tree进行操作 对用户拥有的角色进行复选框选中的属性设置,并且展开对应的权限tree。
     *
     * 作者: zhoubang 日期：2015年4月29日 上午9:52:16
     *
     * @param list
     *            所有角色列表
     * @param roleList
     *            用户拥有的角色列表
     * @param chkDisabledRoleIds
     * @return
     */
    public static List<ZtreeVo> setPZtreeCheck(List<ZtreeVo> list, List<SysRole> roleList, List<Long> chkDisabledRoleIds) {
        for (ZtreeVo ztreeVo : list) {
            if (roleList != null) {
                for (SysRole role : roleList) {
                    if (StringUtils.equals(String.valueOf(role.getId()), ztreeVo.getId())) {
                        ztreeVo.setChecked(true);
                    }
                    // 设置需要禁用check复选框选中的节点
                    if (null != chkDisabledRoleIds) {
                        for (Long roleId : chkDisabledRoleIds) {
                            if (role.getId().longValue() == roleId.longValue()) {
                                ztreeVo.setChkDisabled(true);
                            }
                        }
                    }
                }
            } else {
                ztreeVo.setChecked(false);
            }
            ztreeVo.setOpen(true);
        }
        return list;
    }

    /**
     * 分页获取所有权限列表
     * <p>
     * 创建日期：2016年8月4日 上午10:27:50
     * 操作用户：zhoubang
     *
     * @return
     */
    /*@RequestMapping("/list")
    @ResponseBody
    public JQDatatableResult<PermissionVo> list(Pager<PermissionVo> pager, SysPermission permission) {
        pager = permissionService.getList(pager, permission);

        JQDatatableResult<PermissionVo> result = new JQDatatableResult<PermissionVo>();
        result.setData(pager.getRows());
        result.setRecordsTotal(pager.getTotal().intValue());
        result.setDraw(pager.getDraw());
        result.setRecordsFiltered(pager.getTotal().intValue());
        return result;
    }*/
    @RequestMapping("/list")
    @RequiresPermissions("permission:list")
    @ResponseBody
    public List<PermissionVo> list(SysPermission qo) {
        Pager<PermissionVo> pager = new Pager<PermissionVo>();
        pager.setLimit(Integer.MAX_VALUE);

        Pager<PermissionVo> vos = permissionService.getList(pager,qo);
        if(vos != null){
            return vos.getRows();
        }
        return null;
    }

    /**
     * 进入权限添加页面
     * @param map
     * @param appId 权限添加页面里面的查询条件
     * @param appIdByListView 来自权限列表页面的appId查询参数
     * @param selectPermissionId 权限列表页面所选择的需要在哪一个权限下面添加权限
     * @return
     */
    @RequestMapping("/toAddView")
    @RequiresPermissions("permission:add")
    public String toAddView(ModelMap map, Long appId,Long appIdByListView,String selectPermissionId) {

        //获取所有权限列表
        //List<ZtreeVo> allPermission = permissionService.queryAllFormatWithZtree(false, appId);
        //json字符串包含权限的复选框选中属性设置以及展开属性设置.
        //map.put("permissions", JsonUtil.toJson(setPZtreeCheck(allPermission,false)));

        map.put("appObj", appService.get(appId));
        map.put("status", Status.values());
        map.put("selectPermissionId", selectPermissionId);
        map.put("appIdByListView", appIdByListView);

        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);
        map.put("apps", vos.getRows());

        return "/sys/permission/add";
    }

    /**
     * 获取对应应用下的权限列表
     * @param appId
     * @return
     */
    @RequestMapping("/getPermissionTree")
    @RequiresPermissions("permission:list")
    @ResponseBody
    public String getPermissionTree(Long appId,Boolean showTopParent,Boolean open) {
        //获取对应应用下的权限列表
        List<ZtreeVo> allPermission = permissionService.queryAllFormatWithZtree(showTopParent, appId);
        //json字符串包含权限的复选框选中属性设置以及展开属性设置.
        return JsonUtil.toJson(setPZtreeCheck(allPermission,open));
    }

    /**
     * 对权限tree进行操作
     * 对权限进行复选框选中的属性设置,并且展开对应的权限tree。
     * <p>
     * 作者: zhoubang
     * 日期：2015年4月29日 上午9:52:16
     *
     * @param list
     * @return
     */
    public static List<ZtreeVo> setPZtreeCheck(List<ZtreeVo> list,Boolean open) {
        for (ZtreeVo ztreeVo : list) {
            ztreeVo.setChecked(false);
            ztreeVo.setOpen(open);//是否展开
        }
        return list;
    }


    /**
     * 添加权限
     * <p>
     * 创建日期：2016年8月4日 下午3:29:56
     * 操作用户：zhoubang
     *
     * @param permission
     * @return
     */
    @RequestMapping("/addPermission")
    @RequiresPermissions("permission:add")
    @ResponseBody
    public AjaxResult<String> addPermission(SysPermission permission) {
        AjaxResult<String> result = new AjaxResult<String>();

        if (StringUtils.isBlank(permission.getName())) {
            result.setCode(10001);
            result.setMsg("请填写权限名称");
            return result;
        }
        if (StringUtils.isBlank(permission.getDescription())) {
            result.setCode(10002);
            result.setMsg("请填写权限描述");
            return result;
        }

        if (StringUtils.isBlank(permission.getCode())) {
            result.setCode(10003);
            result.setMsg("请填写权限代码");
            return result;
        }
        
        /*if (permission.getParentId() == null) {
            result.setCode(10004);
            result.setMsg("请设置权限所属层级");
            return result;
        }*/

        try {
            SysPermission sysPermission = permissionService.selectByCode(permission.getCode());
            if (sysPermission != null) {
                result.setCode(10006);
                result.setMsg("权限code已经存在");
                return result;
            }

            //如果没有选择所在的父节点，则默认为最上层菜单
            if (permission.getParentId() == null) {
                permission.setParentId(0L);
            }
            permissionService.save(permission);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(10005);
            result.setMsg("添加失败");
            return result;
        }
        return result;
    }

    /**
     * 进入权限编辑页面
     * <p>
     * 创建日期：2016年8月4日 下午3:45:41
     * 操作用户：zhoubang
     *
     * @param modelMap
     * @param permissionId
     * @return
     */
    @RequestMapping("/editPermissionView")
    @RequiresPermissions("permission:edit")
    public String editPermissionView(ModelMap modelMap, Long permissionId) {
        //权限信息
        SysPermission permission = permissionService.get(permissionId);
        modelMap.put("permission", permission);

        //获取所有权限列表
        //List<ZtreeVo> allPermission = permissionService.queryAllFormatWithZtree(true, permission.getAppId());
        //角色对应的权限.json字符串包含权限的复选框选中属性设置以及展开属性设置.
        //modelMap.put("permissions", JsonUtil.toJson(setPZtreeCheck(allPermission,true)));

        modelMap.put("status", Status.values());

        Pager<AppVo> pager = new Pager<AppVo>();
        pager.setLimit(Integer.MAX_VALUE);
        AppQo appQo = new AppQo();
        appQo.setStatus(Status.ENABLE);
        Pager<AppVo> vos = appService.getList(pager,appQo);
        modelMap.put("apps", vos.getRows());

        return "/sys/permission/edit";
    }


    /**
     * 权限删除
     * <p>
     * 创建日期：2016年8月4日 下午5:24:23
     * 操作用户：zhoubang
     *
     * @param permissionId
     * @return
     */
    @RequestMapping("/deletePermission")
    @RequiresPermissions("permission:delete")
    @ResponseBody
    public AjaxResult<String> deletePermission(Long permissionId) {
        AjaxResult<String> result = new AjaxResult<String>();
        try {
            //删除权限
            result = permissionService.deletePermission(permissionId);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(10003);
            result.setMsg("删除权限失败");
            return result;
        }
        return result;
    }

    /**
     * 更新权限信息
     * <p>
     * 日期：2016年8月20日 下午6:13:06
     * 用户：zhoubang
     *
     * @param permission
     * @return
     */
    @RequestMapping("/updatePermission")
    @RequiresPermissions("permission:update")
    @ResponseBody
    public AjaxResult<String> updatePermission(SysPermission permission) {
        AjaxResult<String> result = new AjaxResult<String>();

        if (permission.getId() == null) {
            result.setCode(10001);
            result.setMsg("权限ID为空");
            return result;
        }

        if (StringUtils.isBlank(permission.getName())) {
            result.setCode(10002);
            result.setMsg("请输入权限名称");
            return result;
        }

        if (permission.getStatus() == null) {
            result.setCode(10003);
            result.setMsg("请选择权限状态");
            return result;
        }

        SysPermission p = permissionService.get(permission.getId());
        if (p == null) {
            result.setCode(10005);
            result.setMsg("该权限不存在");
            return result;
        }

        //更新权限
        try {
            permissionService.updatePermission(permission);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(10007);
            result.setMsg("权限更新失败");
            return result;
        }

        return result;
    }

}
