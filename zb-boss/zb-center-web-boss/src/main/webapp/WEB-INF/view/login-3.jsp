<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>shiro权限认证-[BOSS系统]</title>
<link href="${_cdnStaticResourcesPath}/resources/static/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<style type="text/css">
	@keyframes cc{
		0%{opacity: 0.4;transform: scale(0.1) ;top: -350px;}
		50%{opacity: 0.4;transform: scale(0.1) rotateX(200deg);top: -50px;}
		100%{opacity: 1;transform: scale(1) rotateX(0deg);top: 0;}
	}
	/*@media only screen and (max-width: 500px) {
		.login{
			width: 95%!important;
		}
		.sr{
			padding:30px 70px!important;
		}
	}*/
	html,body{
		height:100%;
		width:100%;
		background: url(${_cdnStaticResourcesPath}/resources/static/images/loginbg.jpg) no-repeat center center;
		background-size:1920px 1000px;
		perspective:500px;
	}
	*{
		font-family:"浠垮畫";
	}
	.login{
		width:450px;
		position: relative;
		top: 0;
		margin: 15% auto;
		background:rgba(255,255,255,1);
		border-radius: 10px;
		color: #27d;
		animation: cc 4s ease-in-out ;
	}
	.login header{

		padding: 10px;
		border-bottom: 1px solid #27d;
	}
	.login header h1{
		margin:0;
		padding:0;
		text-align: center;
		font-size: 25px;
		line-height: 40px;
	}
	.sr{
		padding:30px 90px;
	}
	.name{
		position:relative;
		margin-bottom:20px;
		clear:both;
		border:1px solid #27d;
		width: 100%;
		height: 40px;
	}
	.name label{
		position: absolute;
		color: #27d;
		top: 10px;
		left: 10px;
	}
	.name_inp{
		padding-left: 34px;
		height: 38px;
		line-height: 38px;
		line-height: 36px\9;
		border: none;
		background-color: #fff;
		width:100%;
		outline:none;
	}
	.dl{
		width:150px;
		height:40px;
		background:#27d;
		line-height:40px;
		display:block;
		margin:0 auto;
		color:#fff;
		border:0;
		font-size:18px;
	}
</style>

<script src="<c:url value="/resources/static/js/jquery-1.9.1.min.js"/>" type="text/javascript"></script>
<link href="${_cdnStaticResourcesPath}/resources/static/js/plugins/message/message.css" type="text/css" rel="stylesheet" />
<script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/message/message.js"></script>
<script type="text/javascript">
	//是否开启验证码登录
	var openVerifyCode = false;

	//是否开启ajax登录
	var openAjaxLogin = true;

	$(document).keyup(function(event) {
        if (event.keyCode == 13) {
            $("#loginLink").trigger("click");
        }
    });
	
	function reset(){
		$("#userName").val("");
		$("#password").val("");
		$("#userName").focus();
	}
	
	//客户端校验
    function check() {
        if ($("#userName").val() == "") {
            $.message({
                message:"请输入账户名",
                type:'warning'
            });
            $("#userName").focus();
            return false;
        } else {
            $("#userName").val(jQuery.trim($('#userName').val()));
        }

        if ($("#password").val() == "") {
            $.message({
                message:"请输入密码",
                type:'warning'
            });
            $("#password").focus();
            return false;
        }
        return true;
    }
	
    //服务器校验
    function severCheck(){
        if(check()){
            //使用ajax登录
            if(openAjaxLogin){
                var userName = $("#userName").val();
                var password = $("#password").val();
                
                $.ajax({
                    type: "post",
                    url: "<c:url value='/login/login'/>",
                    data: {"userName" : userName, "password" : password , "openAjaxLogin" : openAjaxLogin},
                    dataType:'json',
                    cache: false,
                    success: function(data){
                        if(200 == data.code){
                            window.location.href="<c:url value='/'/>";
                        }else if("usererror" == data.msg){
                            $.message({
                                message:"用户名或密码错误",
                                type:'error'
                            });
                            $("#userName").focus();
                        }else{
                            $.message({
                                message:data.msg,
                                type:'error'
                            });
                            $("#userName").focus();
                        }
                    }
                });
            }
        }
    }
</script>
</head>
<body>
<div style="height:1px;"></div>
	<div class="login">
		<header>
			<h1>登录走一波</h1>
		</header>
		<div class="sr">
			<div class="name">
				<label>
					<i class="sublist-icon glyphicon glyphicon-user"></i>
				</label>
				<input type="text"  placeholder="请输入登录账户" name="userName" id="userName" class="name_inp">
			</div>
			<div class="name">
				<label>
					<i class="sublist-icon glyphicon glyphicon-pencil"></i>
				</label>
				<input type="text"  placeholder="请输入登录密码" name="password" id="password" class="name_inp">
			</div>
			<button class="dl" id="loginLink" onclick="severCheck();">走起</button>
		</div>
	</div>
</body>
</body>
</html>