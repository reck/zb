﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>权限列表</title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <link href="http://v3.bootcss.com/dist/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="${_cdnStaticResourcesPath}/resources/static/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/jquery.treegrid.css">

    <style>
        .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td{
            color: #000000;
            padding: 5px;
        }
        /**表头的高度*/
        .table thead > tr > th,.table thead > tr > td{line-height: 1.2!important;}
        /** 避免其他css的全局样式对当前table的td的对齐方式产生影响 */
        .table.table-bordered tbody tr td:nth-child(3){
            text-align: left!important;
        }

        /**表格操作按钮样式*/
        .btn{
            padding: 3px 10px;
        }
        /**顶部操作按钮样式*/
        .border .btn{
            padding: 5px 10px;
            font-size: 12px!important;
            line-height: 18px!important;
            margin-left: 10px;
        }
        /**行选中的背景色，覆盖bootstrap默认的颜色*/
        .treegrid-selected{
            background-color: #C1E3C1!important;
        }
        .table-striped > tbody > tr:nth-child(2n+1) > td{
            background-color: inherit;
        }
        label{
            font-weight: inherit!important;
        }
    </style>

    <script src="${_cdnStaticResourcesPath}/resources/static/js/jquery-fixed.js" type="text/javascript" ></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/jquery.treegrid.min.js"></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/jquery.treegrid.bootstrap3.js"></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/jquery.treegrid.extension.js"></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/tree.table.js"></script>

</head>

<body id="permissionlist_frame_box" style="height: 100%;">
<div class="page-content clearfix">
    <div id="permission_list_style">
        <!--条件查询-->
        <div class="search_style" style="background-color: #fff;">
            <div class="title_names">搜索查询</div>
            <ul class="search_content clearfix" style="margin-bottom: 0px!important;">
                <li><label class="l_f">权限名称</label><input id="name" name="name" type="text"  class="text_add ml-10" placeholder="请输入权限名称"  style="width:200px;height: 30px;line-height: 30px;"/></li>
                <li class="ml-10">
                    <label class="l_f">权限状态</label>
                    <div class="col-xs-6">
						<span class="select-box">
							<select class="select" id="status" name="status" size="1" style="height: 30px;line-height: 30px;">
								<option value="">全部</option>
								<c:forEach items="${status}" var="s">
                                    <option value="${s}">${s.description}</option>
                                </c:forEach>
							</select>
						</span>
                    </div>
                </li>
                <li class="ml-10">
                    <label class="l_f">所属应用</label>
                    <div class="col-xs-4">
						<span class="select-box">
							<select class="select" id="appId" name="appId" style="height: 30px;line-height: 30px;">
								<option value="">全部</option>
								<c:forEach items="${apps}" var="app">
                                    <option value="${app.id}">${app.name}</option>
                                </c:forEach>
							</select>
						</span>
                    </div>
                </li>
                <li style="width:90px;margin-left: 20px;"><button type="button" class="btn_search" style="height: 30px;line-height: 30px;"><i class="icon-search"></i>查询</button></li>
            </ul>
        </div>

        <!--功能按钮-->
        <div class="border clearfix" style="margin-top: 20px;">
			<span class="l_f">
                <a style="background-color: #fff!important;border-color: #ccc!important;color:#333!important;text-shadow: none!important;" href="javascript:void(0);" onfocus="this.blur();" title="刷新权限列表" onclick="getGrid(true,false);" class="btn btn-default"><i class="icon-refresh"></i>刷新列表</a>
                <a href="javascript:addPermission()" onfocus="this.blur();" title="新增权限" onclick="" class="btn btn-success"><i class="icon-plus"></i>新增权限</a>
                <a style="background-color: #199!important;border-color: #199!important;" href="javascript:foldingAll()" onfocus="this.blur();" title="折叠所有权限" onclick="" class="btn btn-info"><i class="icon-minus-sign"></i>折叠所有权限</a>
                <a style="background-color: #199!important;border-color: #199!important;" href="javascript:openAll()" onfocus="this.blur();" title="展开所有权限" onclick="" class="btn btn-info"><i class="icon-plus-sign"></i>展开所有权限</a>
			</span>
        </div>

        <div class="h_products_list clearfix" id="table_list">
            <div id="scrollsidebar" class="left_Treeview">
                <div class="show_btn" id="rightArrow"><span></span></div>
                <div class="widget-box side_content" >
                    <div class="side_title"><a title="隐藏" class="close_btn"><span></span></a></div>
                    <div class="side_list"><div class="widget-header header-color-green2"><span style="font-size: 13px;">应用列表(点击查询对应的权限)</span></div>
                        <div class="widget-body">
                            <div class="widget-main padding-8"><div id="appTree" class="ztree"></div></div>
                        </div>
                    </div>
                </div>
            </div>


            <!--表格数据-->
            <div class="table_menu_list" id="permissionListIframe">
                <table id="permissionDataGrid"></table>
            </div>
        </div>
    </div>
</div>

<input type="hidden" des="获取权限列表url" id="getPermissionListUrl" value="${ctx}/permission/list"/>
<input type="hidden" des="编辑权限页面url" id="toEditPermissionUrl" value="${ctx}/permission/editPermissionView"/>
<input type="hidden" des="删除权限url" id="deletePermissionUrl" value="${ctx}/permission/deletePermission"/>
<input type="hidden" des="进入添加权限页面url" id="toAddPermissionViewUrl" value="${ctx}/permission/toAddView"/>
<input type="hidden" name="allApps" value='${allApps}' des="应用列表json字符串，这里value必须是单引号，避免与allApps值中的双引号冲突，出现问题"/>
</body>

<script>
    $(function () {
        //当前页面左侧菜单收缩功能
        $("#permission_list_style").fix({
            float : 'left',
            //minStatue : true,
            skin : 'green',
            durationTime :false,
            spacingw:30,//设置隐藏时的距离
            spacingh:260,//设置显示时间距
        });

        /**
         * 浏览器出口缩放的时候，table表格自适应窗口。以免出现数据显示不全
         */
        $(window).resize(function () {
            $("#permissionDataGrid").css("height",$(window).height() - 250);
            $("#permissionDataGrid tbody").css("height",$(window).height() - 250);
        });
        getGrid(false,false);
    });

    $(document).ready(function(){
        var setting = {
            check : {
                enable : true //显示check复选框
            },
            view: {
                fontCss : {background:"#FFFFFF"}
            },
            callback : {
                onClick : function(event, treeId, treeNode){
                    treeNodeCallback(event, treeId, treeNode,true);
                },
                onCheck: function(event, treeId, treeNode){
                    treeNodeCallback(event, treeId, treeNode,treeNode.checked);
                }
            }
        };
        var apps = $("input[name='allApps']").val();
        $.fn.zTree.init($("#appTree"), setting, eval("(" + apps + ")"));
    });

    /**
     * 左侧应用菜单点击事件
     */
    function treeNodeCallback(event, treeId, treeNode, isSelected){
        var zTree = $.fn.zTree.getZTreeObj("appTree");
        zTree.checkAllNodes(false);//取消所有选中的node
        if(isSelected){
            treeNode.checked = true;//设置当前点击的node节点为选中状态
            zTree.updateNode(treeNode);//更新节点信息，否则不会生效
            $("#appId").val(treeNode.id);//赋予appId的参数值
        }else{
            $("#appId").val("");
        }
        getGrid(false,false);//重新加载table数据
    }

    /**
     * 折叠所有权限
     */
    function foldingAll(){
        getGrid(false,true);
    }
    /**
     * 展开所有权限
     */
    function openAll(){
        getGrid(false,false);
    }

    //条件查询
    $(".btn_search").click(function (){
        //如果选择了所属应用，则动态更新选中左侧的tree的对应的应用节点
        var appId = $("#appId").val();
        var treeObj = $.fn.zTree.getZTreeObj("appTree");
        treeObj.checkAllNodes(false);//取消所有选中的node
        if(appId != ""){
            var selIndex = $("option:selected", "#appId").index() - 1;
            var treeNode = treeObj.getNodes()[selIndex];//设置节点选中
            treeNode.checked = true;
            treeObj.updateNode(treeNode);//更新节点信息，否则不会生效
        }

        //渲染表格数据
        getGrid(false,false);//重新加载table数据
    });


    function getGrid(clearReload,folding) {
        var colunms = TreeGrid.initColumn();
        var table= new TreeTable(TreeGrid.id, $("#getPermissionListUrl").val(), colunms);

        //先清空之前已经初始化好的table，不然的话每次初始化的时候，表格的border会出现递增加1px
        //$("#permissionListIframe").html("");
        $("#permissionListIframe").html("<table id=\"permissionDataGrid\"></table>");//加入表格容器

        //clearReload表示是否是清空查询条件，重新加载表格数据
        if(clearReload != undefined && clearReload == true){
            var treeObj = $.fn.zTree.getZTreeObj("appTree");
            treeObj.checkAllNodes(false);//取消左侧菜单中所有选中的node

            $("#status").val("");
            $("#appId").val("");

            table.clear();
        }else{
            var appId = $("#appId").val();
            var status = $("#status").val();
            var name = $("#name").val();
            var data = {"appId" : appId , "status" : status , "name" : name};
            table.setData(data);
        }

        table.setExpandColumn(2);
        table.setIdField("id");
        table.setCodeField("id");
        table.setParentCodeField("parentId");
        if(folding != undefined && folding == true){//是否折叠
            table.setExpandAll(false);//全部折叠
        }else{
            table.setExpandAll(true);//全部展开
        }
        table.setHeight($(window).height()-250);
        table.setMethod("post");
        table.init();
        TreeGrid.table = table;
    }


    var TreeGrid = {
        id: "permissionDataGrid",
        table: null,
        layerIndex: -1
    };

    /**
     * 初始化表格的列
     */
    TreeGrid.initColumn = function () {
        var columns = [
            {field: 'selectItem', radio: true,width: '5%'},
            {title: 'ID', field: 'id', visible: false, valign: 'middle', width: '5%'},
            {title: '名称', field: 'name', valign: 'middle', width: '15%'},
            {title: '描述', field: 'description', valign: 'middle', width: '15%',formatter: function(item, index){
                return "<span title='" + item.description + "'>" + item.description + "</span>";
            }},
            {title: '代码', field: 'code', valign: 'middle', width: '15%'},
            {title: '父级权限', field: 'parentName', valign: 'middle', width: '15%',formatter: function(item, index){
                if(item.parentName == "" || item.parentName == null){
                    return "<span style='color: #FF7E03;'>(顶级菜单)</span>";
                }else{
                    return item.parentName;
                }
            }},
            {title: '状态', field: 'status', valign: 'middle', width: '8%', formatter: function(item, index){
                if(item.status == 'ENABLE'){
                    return '<span style="font-size: 90%;background-color: #52AD52!important;" class="label label-success radius">启用</span>';
                }
                if(item.status == 'DISABLE'){
                    return '<span style="font-size: 90%" class="label label-danger radius">禁用</span>';
                }
            }},
            {title: '所属应用', field: 'appName', valign: 'middle', width: '12%'},
            {title: '操作', field: 'id', valign: 'middle', width: '10%', formatter: function(item, index){
                return "<a style=\"background-color: #428BCA!important;border-color: #428BCA!important;\" class=\"btn btn-xs btn-info\" onClick=\"permissionEdit('编辑权限信息','" + $("#toEditPermissionUrl").val() + "?permissionId=" + item.id + "')\" href=\"javascript:;\" title=\"编辑权限信息\"><i class=\"icon-edit bigger-120\"></i></a>"
                    + "<a class=\"btn btn-xs btn-danger\" onClick=\"permissionDelete('" + item.id + "')\" href=\"javascript:;\" title=\"删除\"><i class=\"icon-trash bigger-120\"></i></a>";
            }
            }]
        return columns;
    };



    /*权限-编辑*/
    function permissionEdit(title,url){
        //layer_show(title,url,w,h);
        var loadUniqe;
        zeroModal.show({
            title: title,
            iframe: true,
            url: url,
            opacity: 0.8,
            width: '50%',
            top : "60px",
            height: ($(window).height() - 200) + "px",
            esc : true,
            buttons: [{
                className: 'zeromodal-btn zeromodal-btn-primary',
                name: '保存',
                fn: function(opt) {
                    loadUniqe = zeroModal.loading();//loading加载
                    $("#zeromodalIframe").contents().find("#btn_submit").click();//权限编辑页面中的form提交按钮
                    zeroModal.close(loadUniqe); // 关闭loading加载
                    return false;//需要加上return false，防止弹出框自动关闭，不然提交成功等提示信息将不会显示
                }
            }, {
                className: 'zeromodal-btn zeromodal-btn-default',
                name: '取消',
                fn: function(opt) {
                    zeroModal.close(loadUniqe); // 关闭等待框
                }
            }]
        });
    }

    /*权限-删除*/
    function permissionDelete(permissionId){
        $("body").dialog({
            type: "danger",
            title: "系统提示",
            discription: '确定要删除该权限吗？',
            animateIn: "fadeInRight-hastrans",//rotateInUpLeft-hastrans
            animateOut: "rotateOutUpLeft-hastrans",
            showBoxShadow:true,
            buttons: [{name: '确定',className: 'defalut'}, {name: '取消', className: 'reverse'}]
        },function(ret) {
            if(ret.index===0){
                $.ajax({
                    type : "post",
                    url : $("#deletePermissionUrl").val(),
                    dataType : "json",
                    data : {
                        "permissionId" : permissionId
                    },
                    success : function(result) {
                        if (result.code == 200) {
                            $.message("删除成功");
                            getGrid(false,false);//重新加载table数据
                        } else {
                            $.message({
                                message:result.msg,
                                type:'error'
                            });
                        }
                    }
                });
            }else{
                return false;
            }
        });
    }


    //批量删除权限
    function batchDeletePermission(){
        var checks = $("#permissionListTable tbody input[type='checkbox']:checked");//获取当前页所有复选框
        var permissionIdStr = "";
        for(var i = 0; i < checks.length; i++){
            var uid = $(checks[i]).val();
            permissionIdStr += uid;
            if(i < checks.length - 1){
                permissionIdStr += ",";
            }
        }
        if(permissionIdStr == ""){
            $.message({
                message:"请先选择权限",
                type:'info'
            });
            return;
        }

        $("body").dialog({
            type: "danger",
            title: "系统提示",
            discription: "确定要删除选中的权限吗？",
            animateIn: "fadeInRight-hastrans",//rotateInUpLeft-hastrans
            animateOut: "rotateOutUpLeft-hastrans",
            showBoxShadow:true,
            buttons: [{name: '确定',className: 'defalut'}, {name: '取消', className: 'reverse'}]
        },function(ret) {
            if(ret.index===0){
                $.ajax({
                    type : "post",
                    url : $("#deletePermissionsUrl").val(),
                    dataType : "json",
                    data : {
                        "permissionIdStr" : permissionIdStr
                    },
                    success : function(result) {
                        if (result.code == 200) {
                            $.message("删除成功");
                            getGrid(false,false);//重新加载table数据
                        } else {
                            $.message({
                                message:result.msg,
                                type:'error'
                            });
                        }
                    }
                });
            }else{
                return false;
            }
        });
    }

    /**
     * 添加权限
     */
    function addPermission(){
        var loadUniqe,selectPermissionId;
        var selectRow = TreeGrid.table.getSelectedRow();
        if(selectRow && selectRow[0] != undefined){
            selectPermissionId = selectRow[0].id;
        }

        zeroModal.show({
            title: "新增权限",
            iframe: true,
            url: $("#toAddPermissionViewUrl").val() + "?selectPermissionId=" + selectPermissionId + "&appIdByListView=" + $("#appId").val(),
            width: '50%',
            top : "60px",
            height: ($(window).height() - 200) + "px",
            esc : true,
            buttons: [{
                className: 'zeromodal-btn zeromodal-btn-primary',
                name: '保存',
                fn: function(opt) {
                    loadUniqe = zeroModal.loading();//loading加载
                    $("#zeromodalIframe").contents().find("#btn_submit").click();//应用编辑页面中的form提交按钮
                    zeroModal.close(loadUniqe); // 关闭loading加载
                    return false;//需要加上return false，防止弹出框自动关闭，不然提交成功等提示信息将不会显示
                }
            },{
                className: 'zeromodal-btn zeromodal-btn-default',
                name: '取消',
                fn: function(opt) {
                    zeroModal.close(loadUniqe); // 关闭等待框
                }
            }]
        });
    }
</script>
