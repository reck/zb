﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>新增菜单</title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/validform/Validform.js" type="text/javascript"></script>
    <style>
        html{
            overflow: auto;
        }
    </style>
</head>
<body>
<div class="margin" id="page_style">
    <div class="add_style" style="width:600px; padding-right:10px; float:left;">
        <form action="${ctx}/menu/saveMenu" method="post" id="form-menu-add">
            <input type="hidden" des="存放选中的父菜单Id" value="" id="parentId" name="parentId"/>
            <ul>
                <li class="clearfix">
                    <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>所属应用：</label>
                    <div class="formControls col-xs-6">
                            <span class="select-box" style="width:150px;">
                                <select class="select col-xs-12" name="appId" size="1" style="height: 30px;line-height: 30px;">
                                    <c:forEach items="${apps}" var="app">
                                        <option value="${app.id}">${app.name}</option>
                                    </c:forEach>
                                </select>
                            </span>
                    </div>
                </li>
                <li class="clearfix">
                     <label class="label_name col-xs-2 col-lg-2"><i>*</i>类型：</label>
                    <div class="formControls col-xs-6">
                        <c:forEach items="${menuType}" var="type">
                            <label><input name="type" checked="checked" value="${type}" type="radio" class="ace"><span class="lbl">${type.description}</span></label>
                        </c:forEach>
                    </div>
                    <div class="col-4"> <span class="Validform_checktip Validform_wrong" id="menuTypeErrorTip"  style="display: none"></span></div>
                </li>
                <li class="clearfix">
                    <label class="label_name col-xs-2 col-lg-2"><i>*</i>名称：</label>
                    <div class="formControls col-xs-6">
                        <input type="text" class="input-text col-xs-12" value="" placeholder="请输入菜单名称" id="name" name="name" datatype="*" nullmsg="菜单名称不能为空"></div>
                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                </li>
                <li class="clearfix" id="parentMenuDiv" style="display: none;">
                    <label class="label_name col-xs-2 col-lg-2"><i>*</i>父级菜单：</label>
                    <div class="formControls col-xs-6">
                        <input readonly="readonly" style="cursor: pointer;" title="请选择父菜单" onclick="selectParentMenu()" type="text" class="input-text col-xs-12" value="" placeholder="请选择父级菜单" datatype="*" nullmsg="请选择父级菜单" id="parentIdInput" name="parentIdInput"></div>
                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                </li>
                <li class="clearfix" id="menuUrlDiv" style="display: none;">
                    <label class="label_name col-xs-2 col-lg-2"><i>*</i>URL：</label>
                    <div class="formControls col-xs-6">
                        <input type="text" class="input-text col-xs-12" value="" placeholder="请输入菜单URL" id="url" name="url" datatype="*" nullmsg="菜单URL不能为空"></div>
                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                </li>
                <li class="clearfix">
                    <label class="label_name col-xs-2 col-lg-2"><i>*</i>权限代码：</label>
                    <div class="formControls col-xs-6">
                        <input type="text" class="input-text col-xs-12" value="" placeholder="请输入菜单权限代码" id="permissionCode" name="permissionCode" datatype="*" nullmsg="菜单权限代码不能为空"></div>
                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                </li>
                <li class="clearfix">
                    <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>排序：</label>
                    <div class="formControls col-xs-6">
                        <input type="text" placeholder="请输入菜单排序" name="sort" id="sort" autocomplete="off" value="" class="input-text col-xs-12" datatype="n" nullmsg="菜单排序不能为空">
                    </div>
                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                </li>
                <li class="clearfix" id="iconDiv">
                    <label class="label_name col-xs-2 col-lg-2"><i>*</i>ICON图标：</label>
                    <div class="formControls col-xs-6">
                        <input type="text" class="input-text col-xs-12" value="" placeholder="请输入菜单ICON图标" id="icon" name="icon" datatype="*" nullmsg="icon图标不能为空">
                    </div>
                    <i id="iconPreview" style="margin-left: 5px;height: 30px;line-height: 30px;"></i>
                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                </li>

                <li class="clearfix" style="display: none;">
                    <div class="col-xs-2 col-lg-2">&nbsp;</div>
                    <div class="col-xs-9">
                        <button class="btn btn-primary" type="button" data-loading-text="提交中..." id="btn_submit"><i class="fa fa-save"></i>&nbsp;提交信息</button>
                    </div>
                </li>
            </ul>
        </form>
    </div>
</div>

<input type="hidden" des="进入选择父菜单的页面url" id="toSelecrParentMenuUrl" value="${ctx}/menu/toSelecrParentMenuView"/>

</body>
</html>


<script>
    $(function () {
        /*******滚动条*******/
        $("body").niceScroll({
            cursorcolor:"#888888",
            cursoropacitymax:1,
            touchbehavior:false,
            cursorwidth:"5px",
            cursorborder:"0",
            cursorborderradius:"5px"
        });

        /**
         * 菜单类型单选按钮点击事件
         */
        $("input[name='type']").click(function () {
            var val = $(this).val();
            if(val == 'MENU'){//选择的是菜单按钮
                $("#parentMenuDiv").css("display","block");
                $("#menuUrlDiv").css("display","block");
                $("#iconDiv").css("display","none");
                $("#icon").removeAttr("datatype").removeAttr("nullmsg");
                $("#parentIdInput").attr("datatype","*").attr("nullmsg","请选择父级菜单");
                $("#url").attr("datatype","*").attr("nullmsg","url不能为空");
            }else{
                $("#parentMenuDiv").css("display","none");
                $("#menuUrlDiv").css("display","none");
                $("#iconDiv").css("display","block");
                $("#icon").attr("datatype","*").attr("nullmsg","icon图标不能为空");
                $("#parentIdInput").removeAttr("datatype").removeAttr("nullmsg");
                $("#url").removeAttr("datatype").removeAttr("nullmsg");
            }
        });

        $("input[name='type']:checked").click();//默认显示类型为菜单的内容

        $("#icon").keyup(function () {
            $("#iconPreview").attr('class', '');
            $("#iconPreview").attr('class', $(this).val() + " fa-lg");
        });
    });


    //表单验证提交
    var btn_submit = "#btn_submit";//提交按钮id
    $("#form-menu-add").Validform({
        ajaxPost:true,
        tiptype:2,
        showAllError:true,
        postonce:false, //可选项 表单是否只能提交一次
        btnSubmit:btn_submit,
        beforeSubmit:function(curform){
            $(btn_submit).button("loading");//提交按钮的loading效果

            var type= $("input[name='type']").val();
            if(type == "" || type == undefined){
                $("#menuTypeErrorTip").css("display","block");
                $("#menuTypeErrorTip").html("请选择菜单类型");
                $(btn_submit).button('reset');//取消提交按钮的loading效果
                return false;
            }else{
                $("#menuTypeErrorTip").css("display","none");
                $("#menuTypeErrorTip").html("");
            }
            if($("input[name='type']:checked").val() == 'MENU' && $("#parentId").val() <= 0){//-1表示的是顶级菜单（与【系统管理】同级别），故如果是菜单类型的话，必须指定父菜单，除非菜单类型为目录
                $.message({
                    message:"请为当前菜单指定一个父菜单",
                    type:'warning'
                });
                $(btn_submit).button('reset');//取消提交按钮的loading效果
                return false;
            }
        },
        callback:function(data){
            $(btn_submit).button('reset');//取消提交按钮的loading效果
            if(data.code == 200){
                $.message("保存成功");
            }else{
                $.message({
                    message:data.msg,
                    type:'error'
                });
                return false;
            }
            window.parent.getGrid();//重新加载菜单数据表
        }
    });

    /**
     * 选择父菜单
     */
    function selectParentMenu() {
        var url = $("#toSelecrParentMenuUrl").val();
        var menuId = "";
        window.parent.selectParentMenu(url,menuId);
    }
</script>
