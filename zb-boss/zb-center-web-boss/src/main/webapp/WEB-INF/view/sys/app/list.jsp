﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title>应用列表</title>
	<style>
		/** 表格的操作按钮的内边距 */
		.btn{padding: 1px 8px!important;font-size: 12px!important;}
		/**顶部操作按钮的内边距*/
		.border .btn {
			display: inline-block;
			padding: 5px 12px!important;
			margin-bottom: 0;
			font-weight: 400;
			line-height:18px;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			cursor: pointer;
			user-select: none;
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
			margin-left: 10px;
		}
		.label {
			 display: inline!important;
			 padding: .2em .6em .3em!important;
			 font-size: 11px!important;
			 font-weight: 700!important;
			 line-height:20px!important;
			 height: 20px!important;
			 color: #fff!important;
			 text-align: center!important;
			 white-space: nowrap!important;
			 vertical-align: baseline!important;
			 border-radius: .25em!important;
		 }
		/**表格tr背景色*/
		.table thead tr{background-color: #fff!important;}
		/**表格单元格样式*/
		.table thead tr th{padding: 10px;}
		/**表格单元格border*/
		.table tbody tr td{border: 0px!important;border-bottom: 1px solid #e7eaec !important;border-left: 1px solid #e7eaec !important;}
		.table-bordered > thead > tr > th, .table-bordered > thead > tr > td{border-bottom-width: 1px!important;}
		.table-bordered{border: 0px!important;}
		/**表头的高度*/
		.table thead > tr > th,.table thead > tr > td{padding: 8px!important;}
	</style>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
	<script src="${_cdnStaticResourcesPath}/resources/static/js/jquery-fixed.js" type="text/javascript" ></script>
</head>

<body id="applist_frame_box">
<div class=" page-content clearfix">
	<div id="app_list_style">
		<!--条件查询-->
		<div class="search_style" style="background-color: #fff;">
			<div class="title_names">搜索查询</div>
			<ul class="search_content clearfix">
				<li><label class="l_f">应用名称</label><input id="name" name="name" type="text"  class="text_add ml-10" placeholder="应用名称（支持模糊查询）"  style="width:200px;height: 30px;line-height: 30px;"/></li>
				<li>
					<label class="l_f col-xs-4 col-lg-4">状态</label>
					<div class="col-xs-5">
						<span class="select-box">
							<select class="select ml-10" id="status" name="status" size="1"  style="height: 30px;line-height: 30px;">
								<option value="">全部</option>
								<c:forEach items="${status}" var="s">
									<option value="${s}">${s.description}</option>
								</c:forEach>
							</select>
						</span>
					</div>
				</li>
				<li style="width:90px;margin-left: 20px;"><button type="button" class="btn_search" style="height: 30px;line-height: 30px;"><i class="icon-search"></i>查询</button></li>
			</ul>
		</div>

		<!--功能按钮-->
		<div class="border clearfix">
			<span class="l_f">
				<a style="background-color: #fff!important;border-color: #ccc!important;color:#333!important;text-shadow: none!important;" href="javascript:void(0);" onfocus="this.blur();" title="刷新应用列表" onclick="reloadTable(true);" class="btn btn-default"><i class="icon-refresh"></i>刷新列表</a>
				<a href="${ctx}/app/toAddView" title="添加应用" class="btn btn-success"><i class="icon-plus"></i>添加应用</a>
				<a href="javascript:void(0);" class="btn btn-danger" onclick="batchDeleteApp();"><i class="icon-trash"></i>批量删除</a>
			</span>
		</div>

		<!--表格数据-->
		<div class="h_products_list clearfix" id="table_list">
			<div id="scrollsidebar" class="left_Treeview">
				<div class="show_btn" id="rightArrow"><span></span></div>
				<div class="widget-box side_content" >
					<div class="side_title"><a title="隐藏" class="close_btn"><span></span></a></div>
					<div class="side_list"><div class="widget-header header-color-green2"><h4 class="lighter smaller">(暂无内容填充)</h4></div>
						<div class="widget-body">
							<div class="widget-main padding-8"><div id="treeDemo" class="ztree"></div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="table_menu_list" id="appListIframe">
				<table class="table table-striped table-bordered table-hover" id="appListTable" style="width: 100%;">
					<thead>
					<tr>
						<th><label><input type="checkbox" class="ace" id="selectAll"/><span class="lbl"></span></label></th>
						<th>应用名称</th>
						<th>应用编码</th>
						<th>上线状态</th>
						<th>启用状态</th>
						<th>排序</th>
						<th>创建时间</th>
						<th>更新时间</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="appInfoDiv" style="display: none;">
	<div class="member_show">
		<div class="member_jbxx clearfix">
			<img class="img" src="${ctx}/resources/static/images/app-info.png">
			<dl class="right_xxln">
				<dt id="appDetail_title" style="font-size: 16px;"></dt>
				<dd class="" style="margin-left:0">应用指的是微服务架构下的子系统，拥有自己的角色与权限</dd>
			</dl>
		</div>
		<div class="member_content">
			<ul>
				<li><label class="label_name" style="width: 100px;">名称：</label><span class="name" id="appDetail_name" style="margin: 0 10px;"></span></li>
				<li><label class="label_name" style="width: 100px;">编码：</label><span class="name" id="appDetail_code" style="margin: 0 10px;"></span></li>
				<li><label class="label_name" style="width: 100px;">上线状态：</label><span class="name" id="appOnline_status" style="margin: 0 10px;"></span></li>
				<li><label class="label_name" style="width: 100px;">状态：</label><span class="name" id="appDetail_status" style="margin: 0 10px;"></span></li>
				<li><label class="label_name" style="width: 100px;">排序：</label><span class="name" id="appDetail_sort" style="margin: 0 10px;"></span></li>
				<li><label class="label_name" style="width: 100px;">创建时间：</label><span class="name" id="appDetail_createTime" style="margin: 0 10px;"></span></li>
				<li><label class="label_name" style="width: 100px;">更新时间：</label><span class="name" id="appDetail_updateTime" style="margin: 0 10px;"></span></li>
			</ul>
		</div>
	</div>
</div>

<input type="hidden" des="获取应用列表url" id="getAppListUrl" value="${ctx}/app/list"/>
<input type="hidden" des="获取应用详情信息url" id="getAppDetailUrl" value="${ctx}/app/detail/"/>
<input type="hidden" des="编辑应用url" name="toEditAppUrl" id="toEditAppUrl" value="<c:url value="/app/toEditView"/>" />
<input type="hidden" des="删除应用url" name="deleteAppUrl" id="deleteAppUrl" value="<c:url value="/app/deleteApp"/>" />
<input type="hidden" des="批量删除应用url" name="deleteAppsUrl" id="deleteAppsUrl" value="<c:url value="/app/deleteApps"/>" />
</body>
</html>
<script>
    var appListTable;//jquery table全局表格对象
    $(function() {
        appListTable = $('#appListTable').dataTable({
            order: [],  //取消默认排序查询,否则复选框一列会出现小箭头
            dom: '<"top">t<"float_left_l"l><"float_left_i"i><"float_left_r"r>p<"clear">',//设置表格的排版规则(提示语、分页、搜索、页数分组等的位置设置)
            language:lang,//自定义分页的相关信息，用于替换插件默认的配置。详细配置在common.jsp中
            aLengthMenu: [15,10,20,50,100],
            destroy : true,
            //bSort : true,
            autoWidth: false,  //自动调整列宽
            processing: false,  //取消插件默认的加载提示
            serverSide: true,  //服务器端分页
            bStateSave: false,//状态保存
            deferRender : true,//控制表格的延迟渲染，可以提高初始化的速度。
            ajax: function (data, callback, settings) {
                var param = {};
                //封装请求参数
                param.draw = data.draw;
                param.start = data.start;//开始的记录序号
                param.page = (data.start / data.length)+1;//当前页码
                param.limit = data.length;//页面显示记录条数，在页面显示每页显示多少项的时候
                param.name = $("#name").val();
                param.status = $("#status").val();
                //设置排序参数
                if(data.order.length > 0){
                    param.order = data.order[0].dir;//排序方式
                    param.sort = data.columns[data.order[0].column].name;//排序字段名
                }
                //ajax请求数据
                $.ajax({
                    type: "POST",
                    url: $("#getAppListUrl").val(),
                    cache: false,  //禁用缓存
                    data: param,  //传入组装的参数
                    dataType: "json",
                    timeout:10000,
                    beforeSend: function(){//请求之前显示loading效果
                        $(".dataTable tr:gt(0)").remove();
                        $(".dataTable thead").append("<tr style='line-height:38px;'><td style='text-align:center;background-color:#FFFFFF;' colspan='9'></td></tr>");
                        $(".dataTable tr:eq(1) td").html("<img src='" + $("#cdnStaticResourcesPath").val() + "/resources/static/images/loading.gif'/><span style='margin-left:5px;vertical-align:middle;'>" + lang.sProcessing + "</span>");
                    },
                    success: function (result) {
                        $(".dataTable tr:gt(0)").remove();//移除loading
                        //封装返回数据
                        var returnData = {};
                        returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
                        returnData.recordsTotal = result.recordsTotal;//返回数据全部记录
                        returnData.recordsFiltered = result.recordsFiltered;//后台不实现过滤功能，每次查询均视作全部结果
                        returnData.data = result.data;//返回的数据列表
                        callback(returnData);
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                        $(".dataTable tr:gt(0)").remove();//移除loading
                        $(".dataTable thead").append("<tr style='line-height:38px;'><td style='text-align:center;' colspan='9'></td></tr>");
                        if(textStatus=="timeout"){
                            $(".dataTable tr:eq(1) td").html("<span style='margin-left:5px;vertical-align:middle;color:red;'>数据请求超时，稍后请刷新重试</span>");
                        }else{
                            $(".dataTable tr:eq(1) td").html("<span style='margin-left:5px;vertical-align:middle;color:red;'>数据请求异常，稍后请刷新重试</span>");
                        }
                    }
                });
            },


            //列表表头字段 //visible:是否显示该列，bSortable：是否排序
            //这里的name属性有特别的作用，用于映射数据库对应的字段名。
            //      比如statusName为枚举的值，但数据库对应的字段为status，所以上面在设置请求排序参数的时候，应该获取的是name值，而不是data属性值。
            columns: [
                { "data": "id" , "bSortable": false ,width : "5%"},
                { "data": "name" , "bSortable": false, width : "15%"},
                { "data": "code" , "bSortable": false , width : "15%"},
                { "data": "onlineStatusName" , name : "online", width : "10%"},
                { "data": "statusName" , "bSortable": false ,name : "status", width : "6%"},
                { "data": "sort" , name : "sort", width : "5%"},
                { "data": "createTime" , name : "createTime", width : "15%"},
                { "data": "updateTime" , name : "updateTime", width : "15%"}
            ],

            //targets：表示具体需要操作的目标列，下标从0开始
            //data: 表示我们需要的某一列数据对应的属性名
            //render:返回需要显示的内容。在此我们可以修改列中样式，增加具体内容
            "columnDefs": [
                {
                    "targets": [0],
                    "data": "appId",
                    "render": function(data, type, full) {
                        var str = "";
                        if(full.online == "YES"){//已经上线的应用不能删除
                            str = "disabled='disabled'";
                        }
                        return '<label><input '+str+' type="checkbox" online="' +  full.online + '" class="ace" value="' + data + '" id="app_chbox_' + data + '"/><span class="lbl"></span></label>';
                    }
                },
                {
                    "targets": [1],
                    "data": "name",
                    "render": function(data, type, full) {
                        return "<a style='text-decoration: underline;color:#0A81E5;' onClick=\"showAppInfo('" + full.id + "')\" href=\"javascript:;\">" + data + "</a>";
                    }
                },
                {
                    "targets": [3],
                    "data": "online",
                    "render": function(data, type, full) {
                        if(full.online == "NO"){
                            return "<span class=\"label label-warning radius\">" + full.onlineStatusName + "</span>";
                        }else if(full.online == "YES"){
                            return "<span  style='background-color: #52AD52!important;' class=\"label label-success radius\">" + full.onlineStatusName + "</span>";
                        }
                        return data + "";
                    }
                },
                {
                    "targets": [4],
                    "data": "status",
                    "render": function(data, type, full) {
                        if(full.status == "ENABLE"){
                            return "<i style='font-size: 14pt;vertical-align: middle;margin-right: 3px;color:#449D44;' class='fa fa-toggle-on'></i>";
                        }else if(full.status == "DISABLE"){// + full.statusName
                            return "<i style='font-size: 14pt;vertical-align: middle;margin-right: 3px;color: #aaa;' class='fa fa-toggle-off'></i>";
                        }
                        return data + "";
                    }
                },
                {
                    "targets": [8],
                    "data": "id",
                    "render": function(data, type, full) {
                        var str = "";
                        if(full.online == "YES"){//已经上线的应用不能删除
                            str = "disabled='disabled'";
						}
                        return "<a title=\"应用监控\" onclick=\"appMonitoring('" + data + "')\" href=\"javascript:void(0);\" class=\"btn btn-xs btn-success\"><i class=\"icon-bar-chart\"></i></a>"
                            +  "<a " + str+ " style=\"background-color: #428BCA!important;border-color: #428BCA!important;\" title=\"编辑应用信息\" onclick=\"appEdit('编辑应用信息','" + $("#toEditAppUrl").val() + "?appId=" + data + "','','35%','65%')\" href=\"javascript:;\" class=\"btn btn-xs btn-info\"><i class=\"icon-edit bigger-120\"></i></a>"
							+  "<a " + str + " title=\"删除应用\" onclick=\"appDelete('" + data + "')\" href=\"javascript:;\" class=\"btn btn-xs btn-danger\"><i class=\"icon-trash bigger-120\"></i></a>";
                    }
                }
            ],

            //创建行的回调。行背景颜色处理
            createdRow : function(row, data, dataIndex){
                $(row).children('td').attr('style', 'text-align: center;');//设置表格列居中显示
                //$(row).unbind("mouseenter").unbind("mouseleave");
                if(data.status == "DISABLE"){
                    $(row).children('td').addClass("tr_disable_color");
                }else if(data.status == "LOCK"){
                    $(row).children('td').addClass("tr_lock_color");
                }
            },

            //表格初始化完毕的回调
            initComplete: function(oSettings, json) {}
        }).api();


		//条件查询
        $(".btn_search").click(function (){
            appListTable.draw();
        });

        //复选框选中事件处理
        $('table th input:checkbox').on('click' , function(){
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox').each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
        });
        //复选框选中事件处理。(全选复选框事件无须写代码，yui框架已经自动处理事件)
        $('#appListTable tbody').on('change', 'input[type="checkbox"]', function(){
            //如果取消选中，则设置全选为非选中状态
            if(!this.checked){
                var el = $('#selectAll').get(0);
                if(el && el.checked){
                    el.checked = false;
                }
            }else{//选中复选框，判断是否全部选择，设置全选复选框状态
                var checkAll = $("#appListTable tbody input[type='checkbox']");//获取当前页所有复选框
                var checkSel = $("#appListTable tbody input[type='checkbox']:checked");//获取当前页选中的复选框
                if(checkAll.length == checkSel.length){
                    $('#selectAll').prop("checked",true);
                }else{
                    $('#selectAll').prop("checked",false);
                }
            }
        });
    });

    /** ==========================================其他相关功能处理======================================= */
    $(function () {
        //滚动条美化
        $("#appListIframe").niceScroll({
            cursorcolor:"#888888",
            cursoropacitymax:1,
            touchbehavior:false,
            cursorwidth:"5px",
            cursorborder:"0",
            cursorborderradius:"5px"
        });

        //当前页面左侧菜单收缩功能
        $("#app_list_style").fix({
            float : 'left',
            //minStatue : true,
            skin : 'green',
            durationTime :false,
            spacingw:30,//设置隐藏时的距离
            spacingh:260,//设置显示时间距
        });

        $(".close_btn").click();
    });

    //获取应用详情信息
	function getAppDetail(appId){
	    var appDetail = "";
        $.ajax({
            type: "POST",
			async : false,//同步获取
            url: $("#getAppDetailUrl").val() + appId,
            cache: true,  //禁用缓存
            dataType: "json",
            timeout:5000,
            success: function (result) {
				console.log(result);
                appDetail = result;
            }
        });
        return appDetail;
	}
    //展示应用详情信息
    function showAppInfo(appId){
        var appInfo = getAppDetail(appId);
		if(appInfo == ""){
            $.message({
                message:"应用详情获取失败",
                type:'error'
            });
		}
		appInfo = eval(appInfo);
		$("#appDetail_title").html(appInfo.name);
        $("#appDetail_name").html(appInfo.name);
        $("#appDetail_code").html(appInfo.code);
        $("#appDetail_sort").html(appInfo.sort);
        $("#appOnline_status").html(appInfo.online == "NO" ? "未上线" : "已上线");
        if(appInfo.online == "NO"){
            $("#appOnline_status").css("color","#F89406").css("fontWeight","bold");
		}else if(appInfo.online == "YES"){
            $("#appOnline_status").css("color","#40AF40").css("fontWeight","bold");
		}
        $("#appDetail_status").html(appInfo.status == "ENABLE" ? "启用" : "禁用");
        $("#appDetail_createTime").html(appInfo.createTime);
        $("#appDetail_updateTime").html(appInfo.updateTime);

        var M = {}
		// 判断是否已存在，如果已存在则直接显示
        if(M.dialog6){
            return M.dialog6.show();
        }
        $("#appInfoDiv").css("display","block");//需要先显示div，让alert插件计算出宽高，否则只会显示”关闭叉叉“。至于为何要这么做，主要还是dialog插件提示框在关闭之后，会将div设置为block。
        M.dialog6 = jqueryAlert({
            'style'   : 'pc',
            'title'   : "详情信息",
            'content' :  $("#appInfoDiv"),
            'modal'   : true,
            'contentTextAlign' : 'left',
            'width'   : 'auto',
            'animateType' : 'linear',
            'buttons' :{
                '关闭' : function(){
                    M.dialog6.close();
                    $("#appInfoDiv").css("display","none");
                },
            }
        })
        $("#appInfoDiv").css("display","none");//立刻设置div为隐藏，不然页面的下方将会出现div的内容，占据一大片空间，很难看
	}
</script>

<script type="text/javascript">
    //初始化宽度、高度
    $(".widget-box").height(document.body.clientHeight-$(".widget-box").offset().top);
    $(".table_menu_list").height(document.body.clientHeight-$(".widget-box").offset().top);
    //当窗口发生改变是触发
    $(window).resize(function(){
        $(".widget-box").height(document.body.clientHeight-$(".widget-box").offset().top);
        $(".table_menu_list").height(document.body.clientHeight-$(".widget-box").offset().top).css("width","auto").css("position","absolute").css("right",0).css("left",0);
    });
    //变相的设置表格宽度自适应。这里延迟一些毫秒再设置比较有效，如果直接在onload等设置的话有问题，找不到问题在哪里。
    setTimeout(function () {
        $(".table_menu_list").height($(window).height()-$(".widget-box").offset().top).css("width","auto").css("position","absolute").css("right",0).css("left",0);
    },100);


    /*******树状图*******/
    var setting = {
        view: {
            dblClickExpand: false,
            showLine: false,
            selectedMulti: false
        },
        data: {
            simpleData: {
                enable:true,
                idKey: "id",
                pIdKey: "pId",
                rootPId: ""
            }
        },
        callback: {

        }
    };

    var zNodes =[
        { id:1, pId:0, name:"(暂无内容)", open:true}
    ];


    $(document).ready(function(){
        var t = $("#treeDemo");
        t = $.fn.zTree.init(t, setting, zNodes);
        //var zTree = $.fn.zTree.getZTreeObj("tree");
        //zTree.selectNode(zTree.getNodeByParam("id",'11'));
    });
	/*应用-编辑*/
    function appEdit(title,url,id,w,h){
        //layer_show(title,url,w,h);
        var loadUniqe;
        zeroModal.show({
            title: title,
            iframe: true,
            url: url,
            opacity: 0.8,
            width: w,
            height: h,
            top: '15%',
            buttons: [{
                className: 'zeromodal-btn zeromodal-btn-primary',
                name: '保存',
                fn: function(opt) {
                    loadUniqe = zeroModal.loading();//loading加载
                    $("#zeromodalIframe").contents().find("#btn_submit").click();//应用编辑页面中的form提交按钮
                    zeroModal.close(loadUniqe); // 关闭loading加载
					return false;//需要加上return false，防止弹出框自动关闭，不然提交成功等提示信息将不会显示
                }
            }, {
                className: 'zeromodal-btn zeromodal-btn-default',
                name: '取消',
                fn: function(opt) {
                    zeroModal.close(loadUniqe); // 关闭等待框
                }
            }]
        });
    }

	/*应用-删除*/
    function appDelete(appId){
        $("body").dialog({
            type: "danger",
            title: "系统提示",
            discription: '确定要删除该应用吗？',
            animateIn: "fadeInRight-hastrans",//rotateInUpLeft-hastrans
            animateOut: "rotateOutUpLeft-hastrans",
            showBoxShadow:true,
            buttons: [{name: '确定',className: 'defalut'}, {name: '取消', className: 'reverse'}]
        },function(ret) {
            if(ret.index===0){
				$.ajax({
					type : "post",
					url : $("#deleteAppUrl").val(),
					dataType : "json",
					data : {
						"appId" : appId
					},
					success : function(result) {
						if (result.code == 200) {
							$.message("删除成功");
							appListTable.draw();//重新加载table数据
						} else {
							$.message({
								message:result.msg,
								type:'error'
							});
						}
					}
				});
            }else{
                return false;
			}
        });
    }


    //批量删除应用
    function batchDeleteApp(){
        var checks = $("#appListTable tbody input[type='checkbox']:checked");//获取当前页所有复选框
        var appIdStr = "";
        var bool = false;
        for(var i = 0; i < checks.length; i++){
            if($(checks[i]).attr("online") == "YES"){
                bool = true;
                break;
			}
            var uid = $(checks[i]).val();
            appIdStr += uid;
            if(i < checks.length - 1){
                appIdStr += ",";
            }
        }
        if(bool){
            $.message({
                message:"已经上线的应用不能删除",
                type:'info'
            });
            return;
        }
        if(appIdStr == ""){
            $.message({
                message:"请先选择应用",
                type:'info'
            });
            return;
        }

        $("body").dialog({
            type: "danger",
            title: "系统提示",
            discription: "确定要删除选中的应用吗？",
            animateIn: "fadeInRight-hastrans",//rotateInUpLeft-hastrans
            animateOut: "rotateOutUpLeft-hastrans",
            showBoxShadow:true,
            buttons: [{name: '确定',className: 'defalut'}, {name: '取消', className: 'reverse'}]
        },function(ret) {
            if(ret.index===0){
				$.ajax({
					type : "post",
					url : $("#deleteAppsUrl").val(),
					dataType : "json",
					data : {
						"appIdArr" : appIdStr
					},
					success : function(result) {
						if (result.code == 200) {
							$.message("删除成功");
							appListTable.draw();//重新加载table数据
						} else {
							$.message({
								message:result.msg,
								type:'error'
							});
						}
					}
				});
            }else{
                return false;
            }
        });
    }

    /**
     * 重新加载表格数据
     * @param clearParmas 是否清空查询参数
     */
    function reloadTable(clearParmas) {
        if(clearParmas){
            $("#name").val("");
            $("#status").val("");
        }
        $(".btn_search").click();
    }


    /**
	 * 打开应用监控
     * @param id
     */
    function appMonitoring(id) {
        $('body').dialog({
            type: 'success',
            showBoxShadow: true,
            buttonsSameWidth:true,//按钮宽度100%
            title : '友情提示',
            buttons:[{name: '好哒！我会持续关注滴~',className: 'defalut'}],
            discription : '【应用监控】功能暂未开发完成哦<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;敬请期待哈^_^',
            animateIn:'fadeInRight-hastrans',
            animateOut:'fadeOutRight-hastrans'
        },function(ret) {
            if(ret.index===0){
            }else{
                return false;
            }
        });
    }
</script>