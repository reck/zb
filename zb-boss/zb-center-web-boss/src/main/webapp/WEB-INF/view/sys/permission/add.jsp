﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <title>新增权限</title>

    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/jquery-fixed.js" type="text/javascript" ></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/validform/Validform.js" type="text/javascript"></script>
</head>
<body>
<div class="clearfix">
    <div id="scrollsidebar" class="left_Treeview" style="margin-right: 10px;">
        <div class="show_btn" id="rightArrow"><span></span></div>
        <div class="widget-box side_content" style="margin-top: 0px;">
            <div class="side_title"><a title="隐藏" class="close_btn"><span></span></a></div>
            <div class="side_list">
                <div class="widget-header header-color-green2">
                    <span style="font-size: 13px;">权限所在层级(默认顶级节点)</span>
                </div>
                <div>
                    <div class="widget-main padding-8">
                        <div  id="permissionTree" class="ztree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="category" style="border: 1px solid #dddddd;height: 100%;">
        <div class="type_style" style="height: 100%;">
            <div class="type_title" style="font-size: 14px;">权限详细信息</div>
            <div class="type_content" style="margin-top: 0px;width: 100%;">
                <div class="margin" id="page_style">
                    <div class="add_style" style="float:left;width: 100%;">
                        <form action="${ctx}/permission/addPermission" method="post" id="form-permission-add">
                            <input type="hidden" des="存放选中的权限id" id="parentId" name="parentId" value=""/>
                            <ul>
                                <li class="clearfix" style="margin-top: 0px;">
                                    <div class="alert alert-info" style="text-align: left;margin-top: 0px;margin-bottom: 10px;">
                                        <p><i class="fa fa-hand-o-right"></i>&nbsp;左侧节点颜色含义说明：</p>
                                        <p style="color: #F752AD;">
                                            <i class="fa fa-stop" style="color: #94C76D;"></i>&nbsp;该颜色标注的节点，表示新增的权限属于此节点的子节点；若未选择，则默认顶级节点
                                        </p>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name" style="text-align: left;;width: 90px;"><i class="c-red">*</i>所属应用：</label>
                                    <div class="formControls col-xs-6">
                                        <span class="select-box" style="width:150px;">
                                            <select class="select col-xs-12" name="appId" id="appId" size="1" style="height: 30px;line-height: 30px;">
                                                <c:forEach items="${apps}" var="app">
                                                    <option value="${app.id}" <c:if test="${app.id == appId}">selected="selected"</c:if> >${app.name}</option>
                                                </c:forEach>
                                            </select>
                                        </span>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name" style="text-align: left;;width: 90px;"><i class="c-red">*</i>权限名称：</label>
                                    <div class="formControls col-xs-6">
                                        <input type="text" placeholder="请输入权限名称" name="name" id="name" autocomplete="off" value="" class="input-text col-xs-12" datatype="*" nullmsg="权限名称不能为空">
                                    </div>
                                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name" style="text-align: left;;width: 90px;"><i class="c-red">*</i>权限描述：</label>
                                    <div class="formControls col-xs-6">
                                        <input type="text" placeholder="请输入权限描述" name="description" id="description" autocomplete="off" value="" class="input-text col-xs-12" datatype="*" nullmsg="权限描述不能为空">
                                    </div>
                                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name" style="text-align: left;;width: 90px;"><i class="c-red">*</i>权限代码：</label>
                                    <div class="formControls col-xs-6">
                                        <input type="text" placeholder="请输入权限代码" name="code" id="code" autocomplete="off" value="" class="input-text col-xs-12" datatype="*" nullmsg="权限代码不能为空">
                                    </div>
                                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name" style="text-align: left;;width: 90px;"><i class="c-red">*</i>权限状态：</label>
                                    <div class="formControls col-xs-6">
                                        <span class="select-box" style="width:150px;">
                                            <select class="select col-xs-12" name="status" size="1" style="height: 30px;line-height: 30px;">
                                                <c:forEach items="${status}" var="s">
                                                    <option value="${s}">${s.description}</option>
                                                </c:forEach>
                                            </select>
                                        </span>
                                    </div>
                                </li>
                                <li class="clearfix" style="display: none;">
                                    <div class="col-xs-2 col-lg-2">&nbsp;</div>
                                    <div class="col-xs-9">
                                        <button class="btn btn-primary" type="button" data-loading-text="提交中..." id="btn_submit"><i class="fa fa-save"></i>&nbsp;提交信息</button>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" des="权限列表页面所选择的需要在哪一个权限下面添加权限" name="selectPermissionId"  id="selectPermissionId" value="${selectPermissionId}"/>
<input type="hidden" des="来自权限列表页面的appId查询参数" name="appIdByListView"  id="appIdByListView" value="${appIdByListView}"/>
<input type="hidden" des="添加权限信息URL" name="addPermissionUrl"  id="addPermissionUrl" value="<c:url value="/permission/addPermission"/>"/>
<input type="hidden" des="获取对应应用下的权限列表URL" name="getPermissionTreeUrl"  id="getPermissionTreeUrl" value="<c:url value="/permission/getPermissionTree"/>"/>
<input type="hidden" name="permissions" value='' des="权限列表json字符串，这里value必须是单引号，避免与permissions值中的双引号冲突，出现问题"/>
</body>
</html>


<script>

    $(document).ready(function() {
        //滚动条美化
        $(".type_style").niceScroll({
            cursorcolor:"#888888",
            cursoropacitymax:1,
            touchbehavior:false,
            cursorwidth:"5px",
            cursorborder:"0",
            cursorborderradius:"5px"
        });
        $("html").css("overflow","auto");
        //当前页面左侧菜单收缩功能
        $("#scrollsidebar").fix({
            float : 'left',
            //minStatue : true,
            skin : 'green',
            spacingw:30,//设置隐藏时的距离
            durationTime :false
        });

        $("#parentId").val(0);

        //如果在权限列表页面已经选择了某应用作为查询参数，则在当前添加页面动态指定所展示的是哪个应用的权限列表tree
        var appIdByListView = $("#appIdByListView").val();
        if(appIdByListView != undefined && appIdByListView != ""){
            $("#appId").val(appIdByListView);
        }

        getPermissionTreeData();//获取应用的权限tree数据

        //应用下拉框改变事件，获取对应的应用的权限数据
        $("#appId").change(function () {
            $("#parentId").val("");//清空之前选中的节点id
            getPermissionTreeData();
        });
    });

    /**
     * 获取应用的权限tree数据
     */
    function getPermissionTreeData(){
        $.ajax({
            url : $("#getPermissionTreeUrl").val(),
            async:true,
            data:{
                "appId" : $("#appId option:selected").val(),//默认获取所属应用下拉框的第一个应用权限数据
                "showTopParent" : true, //是否显示最顶级的节点【无上级权限】
                "open" : false
            },
            dataType:'json',
            success:function(data,textStatus,jqXHR){
                $("input[name='permissions']").val(data);
                loadTree();
            },
            error:function(xhr,textStatus){}
        });
    }



    function checkPermission() {
        var zTree = $.fn.zTree.getZTreeObj("permissionTree"), type = {"Y" : "ps","N" : "s"};
        zTree.setting.check.chkboxType = type;
    }
    /**
     * 加载权限tree
     */
    function loadTree(){
        var setting = {
            check : {
                enable : false
            },
            data : {
                simpleData : {
                    enable : true
                }
            },
            callback : {
                onClick : function(event, treeId, treeNode){//节点被点击后，设置选择的节点id
                    $("#parentId").val(treeNode.id);

                    var treeObj = $.fn.zTree.getZTreeObj("permissionTree");
                    var nodes = treeObj.getNodes();//获取顶级节点列表
                    if(nodes != null && nodes.length > 0){
                        for(var i = 0,len = nodes.length; i < len; i++){
                            $("#" + nodes[i].tId + "_a").css("backgroundColor","#fff").css("color","#333333");//将每个顶级节点的背景色设置为白色
                            settingNodeBackColor(nodes[i],treeNode);//递归方法，设置所有子节点背景色
                        }
                    }
                }
            }
        };
        var roles = $("input[name='permissions']").val();
        $.fn.zTree.init($("#permissionTree"), setting, eval("(" + roles + ")"));
        checkPermission();

        var treeObj = $.fn.zTree.getZTreeObj("permissionTree");
        var selectPermissionId = $("#selectPermissionId").val();//权限列表页面所选择的需要在哪一个权限下面添加权限
        if(selectPermissionId == undefined || selectPermissionId == "undefined" || selectPermissionId == ""){
            treeObj.expandAll(true);//先设置所有节点为展开状态
            //默认将”无上级权限“的节点设置背景色，突出显示当前新增的权限所在的父节点
            var node = treeObj.getNodeByParam("id", -1, null);
            if(node != null){
                $("#" + node.tId + "_a").css("backgroundColor","#94C76D").css("color","#333333");//设置选中的节点背景颜色
            }
        }else {
            //如果在权限列表页面选择了某一个权限后，再进入此添加页面的话，则当前添加的权限的默认上级节点为列表页面所选择的节点
            treeObj.expandAll(false);//先设置所有节点为折叠状态
            var selectNode = treeObj.getNodeByParam("id", selectPermissionId, null);//获取父节点
            $("#" + selectNode.tId + "_a").css("backgroundColor","#94C76D").css("color","#333333");//设置父节点的背景色，突出显示
            expandAllParentNode(treeObj,selectNode);//展开当前节点的所有父节点
        }
    }

    /**
     * 展开当前节点的所有父节点
     */
    function expandAllParentNode(treeObj,selectNode) {
        var parentNode = selectNode.getParentNode();
        if(parentNode != undefined && parentNode != null){
            treeObj.expandNode(parentNode,true, false);//指定选中ID节点展开
            expandAllParentNode(treeObj,parentNode);
        }
    }
    /**
     * 设置节点以及所有子节点的背景颜色
     * @param treeNode
     */
    function settingNodeBackColor(nodeObj,treeNode) {
        var nodes = nodeObj.children;//获取下级所有子节点
        if(nodes != null && nodes.length > 0){
            for(var i = 0,len = nodes.length; i < len; i++){
                if(nodes[i].tId != treeNode.tId){
                    $("#" + nodes[i].tId + "_a").css("backgroundColor","#fff").css("color","#333333");//设置节点的背景颜色
                }
                settingNodeBackColor(nodes[i],treeNode);
            }
        }else{
            $("#" + treeNode.tId + "_a").css("backgroundColor","#94C76D").css("color","#333333");//设置选中的节点背景颜色
        }
    }

    //表单验证提交
    var btn_submit = "#btn_submit";//提交按钮id
    $("#form-permission-add").Validform({
        ajaxPost:true,
        tiptype:2,
        showAllError:true,
        postonce:true, //可选项 表单是否只能提交一次，true开启，不填则默认关闭;
        btnSubmit:btn_submit,
        beforeSubmit:function(curform){
            var selelectParentId = $("#parentId").val();
            if(selelectParentId == -1){
                $("#parentId").val(0);//设置父节点为0，作为顶层菜单。提交到数据库后就是0，而不能是-1，以免造成tree的效果出现瑕疵
            }
        },
        callback:function(data){
            if(data.code == 200){
                $.message("保存成功");
                getPermissionTreeData();//重新加载tree
            }else{
                $.message({
                    message:data.msg,
                    type:'error'
                });
            }
            window.parent.getGrid(); //重新加载数据表
        }
    });
</script>
