﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta charset="utf-8">
    
    <title>分配应用</title>
    
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/js/sys/user/assign_app.js"></script>
</head>
<body>
    <article class="page-container">
        <form action="${ctx}/user/assignApp" method="post" class="form form-horizontal" id="form-user-assign_app">
            <!-- 存放用户id -->
            <input type="hidden" id="userId" name="userId" value="${userId}"/>
        
            <div class="row cl">
                <label class="form-label col-xs-8 col-sm-1">&nbsp;</label>
                <div class="formControls col-xs-8 col-sm-10">
                    <c:forEach items="${allApp}" var="app" varStatus="s">
                        <c:if test="${s.index%4 == 0 && s.index != 0}">
                            <br/><br/>
                        </c:if>
                        
                        <c:if test="${s.index == 0}">
                            <input type="checkbox" name="appId" value="${app.id}" <c:forEach items="${userAppList}" var="ua"><c:if test="${ua.appId == app.id}">checked="checked"</c:if></c:forEach> /> ${app.name}
                        </c:if>
                        <c:if test="${s.index != 0}">
                            <c:if test="${s.index%4 != 0}">
                                <input type="checkbox" style="margin-left: 20px;" name="appId" value="${app.id}" <c:forEach items="${userAppList}" var="ua"><c:if test="${ua.appId == app.id}">checked="checked"</c:if></c:forEach> /> ${app.name}
                            </c:if>
                            <c:if test="${s.index%4 == 0}">
                                <input type="checkbox" name="appId" value="${app.id}" <c:forEach items="${userAppList}" var="ua"><c:if test="${ua.appId == app.id}">checked="checked"</c:if></c:forEach> /> ${app.name}
                            </c:if>
                        </c:if>
                    </c:forEach>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row cl">
                <div class="col-xs-8 col-sm-5 col-xs-offset-4 col-sm-offset-5 mt-50">
                    <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
                </div>
            </div>
        </form>
    </article>
    
    <!--请在下方写此页面业务相关的脚本--> 
    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/lib/My97DatePicker/4.8/WdatePicker.js"></script>
    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/lib/jquery.validation/1.14.0/validate-methods.js"></script> 
    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/lib/jquery.validation/1.14.0/messages_zh.js"></script>
    <link rel="stylesheet" href="${_cdnStaticResourcesPath}/resources/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script> 
    
    <input type="hidden" name="allRoles" value='${allRoles}' des="角色列表json字符串，这里value必须是单引号，避免与allRoles值中的双引号冲突，出现问题"/>
</body>
</html>
