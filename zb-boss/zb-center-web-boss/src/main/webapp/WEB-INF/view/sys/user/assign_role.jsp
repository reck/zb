﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>为用户指定角色</title>
    <style>
        /**顶部操作按钮的内边距*/
        .button_brand .btn {
            display: inline-block;
            padding: 5px 12px!important;
            margin-bottom: 0;
            font-weight: 400;
            line-height:25px;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            user-select: none;
            background-image: none;
            border: 1px solid #FFF!important;
            border-radius: 4px;
            margin-left: 10px;
        }
    </style>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/validform/Validform.js" type="text/javascript"></script>
</head>
<body>
<div>
    <div id="page_style">
        <div class="add_style">
            <form action="${ctx}/user/assignRole" method="post" id="form-user-assignRole">
                <input type="hidden" name="userId" value="${user.id}"/>
                <input type="hidden" name="appId" value="${userApp.id}"/>
                <input type="hidden" name="roleIds" value=""/>
                <div class="alert alert-success" style="margin-top: 10px;margin-bottom: 5px;text-align: left;">
                    <p style="font-weight: bold;font-size: 16px;">为用户指定角色</p>
                </div>
                <div id="add_brand" style="border-top: 1px solid #dddddd;">
                    <div class="left_add">
                        <div class="title_name" style="padding-left:20px;">用户信息</div>
                        <ul class="add_conent" style="margin-left: 100px;">
                            <li style="margin: 0px;padding: 15px 0;" class=" clearfix"><label style="text-align:right;padding-right: 20px;" ><i></i>登录账户：</label> ${user.userName}</li>
                            <li style="margin: 0px;padding: 15px 0;" class=" clearfix"><label style="text-align:right;padding-right: 20px;" ><i></i>真实姓名：</label> ${user.realName}</li>
                            <li style="margin: 0px;padding: 15px 0;" class=" clearfix"><label style="text-align:right;padding-right: 20px;" ><i></i>所属应用：</label> ${userApp.name}</li>
                            <li style="margin: 0px;padding: 15px 0;" class=" clearfix"><label style="text-align:right;padding-right: 20px;" ><i></i>账户状态：</label> ${statusName}</li>
                            <li style="margin: 0px;padding: 15px 0;" class=" clearfix">
                                <label style="text-align:right;"><i></i>拥有角色：</label>
                                <div class="Add_content col-xs-9" style="line-height: 20px!important;float: right;">
                                    <c:forEach items="${haveRoles}" var="r">
                                        <span style="padding: 5px;line-height: 15px;margin: 5px 0 0 5px;" class="classification_name l_f"><label style="cursor: inherit;margin-right: 0px;"><span class="lbl">${r.name}</span></label></span>
                                    </c:forEach>
                                </div>
                            </li>
                            <li style="margin: 0px;padding: 15px 0;" class=" clearfix"><label style="text-align:right;padding-right: 20px;"><i></i>创建时间：</label> ${createTime}</li>
                            <li style="margin: 0px;padding: 15px 0;" class=" clearfix"><label style="text-align:right;padding-right: 20px;"><i></i>更新时间：</label> ${updateTime}</li>
                        </ul>
                    </div>
                    <div class="add_style right_add" style="margin-left: 500px;overflow: hidden;">
                            <div class="title_name">角色设置</div>
                            <div class="p_select_list">
                                <div class="alert alert-warning" style="margin:0 20px;text-align: left;">
                                    <p style="font-size: 14px;color: #B7832C;">请为当前用户&nbsp;<span style="font-weight: bold;color: #FA2C9B;">${user.userName}</span>&nbsp;指定：在&nbsp;<span style="font-weight: bold;color: deeppink;">《${userApp.name}》</span>&nbsp;中所拥有的角色.</p>
                                </div>
                                <div class="left_produt produt_select_style" style="margin: 20px 20px;">
                                    <div id="select_style">
                                        <span class="seach_style">
                                            <div class='title_name' style="padding-left: 10px;">未指定的角色</div>
                                            <select multiple='multiple' id="noRole" class="select" name = "noRole" style="border: 0px;">
                                                <c:forEach items="${allRoles}" var="role">
                                                    <c:if test="${role.id != 1}">
                                                        <option style="padding: 10px 10px;" value="${role.id}">${role.name}</option>
                                                    </c:if>
                                                </c:forEach>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="Switching_btn" style="margin-top: 150px;">
                                    <span id="add_all" class="Switching" style="margin: 20px 0;">
                                        <input type="button" class="btn btn-warning" value=">>"/>
                                    </span>
                                    <span id="add" class="Switching" style="margin: 20px 0;">
                                        <input type="button" class="btn btn-primary" value=">"/>
                                    </span>
                                    <span id="remove" class="Switching" style="margin: 20px 0;">
                                        <input type="button" class="btn btn-primary" value="<"/>
                                    </span>
                                    <span id="remove_all" class="Switching" style="margin: 20px 0;">
                                        <input type="button" class="btn btn-warning" value="<<"/>
                                    </span>
                                </div>
                                <div class="right_product produt_select_style" style="margin: 20px 20px;">
                                    <div class="title_name" style="padding-left: 10px;">已指定的角色</div>
                                    <select multiple="multiple" id="yesRole" class="select">
                                        <c:forEach items="${roleList}" var="role">
                                            <option style="padding: 10px 10px;" value="${role.id}">${role.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                    </div>
                </div>
                <div class="button_brand" style="margin-bottom: 50px;width: 100%;margin-left: -200px;">
                    <button style="background-color: #119999!important;border-color: #119999!important;" class="btn btn-success" type="button" data-loading-text="提交中..." id="btn_submit" onfocus="this.blur();"><i class="fa fa-save"></i>&nbsp;提交信息</button>
                    <a onclick="javascript:window.location.href='${ctx}/user/toUserListView'" style="background-color: #fff!important;border-color: #ccc!important;color:#333!important;text-shadow: none!important;" href="javascript:void(0);" onfocus="this.blur();" title="返回用户列表" class="btn btn-default"><i class="fa fa-reply"></i>&nbsp;返回上一步</a>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        $(".left_add").height($(document).height()-180);
        $(".right_add").height($(document).height()-180);

        $(".left_add").width(500);
        $(".left_add .title_name").width(480);
        $(".right_add").width($(document).width()-600);
        $(".select").height($(document).height()-350);
        $("#select_style").height($(document).height()-320);
        //当文档窗口发生改变时 触发
        $(window).resize(function(){
            $(".left_add").height($(document).height()-180);
            $(".right_add").height($(document).height()-180);
            $(".select").height($(document).height()-350);
            $("#select_style").height($(document).height()-320);
            $(".right_add").width($(document).width()-600);
        });

        getSelectRole();//初始化已经拥有的角色id列表给隐藏域
    });
    $(function(){
        //移到右边
        $('#add').click(function() {
            //获取选中的选项，删除并追加给对方
            $('#noRole option:selected').appendTo('#yesRole');
            getSelectRole();
        });
        //移到左边
        $('#remove').click(function() {
            $('#yesRole option:selected').appendTo('#noRole');
            getSelectRole();
        });
        //全部移到右边
        $('#add_all').click(function() {
            //获取全部的选项,删除并追加给对方
            $('#noRole option').appendTo('#yesRole');
            getSelectRole();
        });
        //全部移到左边
        $('#remove_all').click(function() {
            $('#yesRole option').appendTo('#noRole');
            getSelectRole();
        });
        //双击选项
        $('#noRole').dblclick(function(){ //绑定双击事件
            //获取全部的选项,删除并追加给对方
            $("option:selected",this).appendTo('#yesRole'); //追加给对方
            getSelectRole();
        });
        //双击选项
        $('#yesRole').dblclick(function(){
            $("option:selected",this).appendTo('#noRole');
            getSelectRole();
        });
    });


    //表单验证提交
    var btn_submit = "#btn_submit";//提交按钮id
    var userRoleForm = $("#form-user-assignRole").Validform({
        ajaxPost:true,
        tiptype:2,
        showAllError:true,
        postonce:false, //可选项 表单是否只能提交一次，true开启，不填则默认关闭;
        btnSubmit:btn_submit,
        beforeSubmit:function(curform){
            $(btn_submit).button("loading");//提交按钮的loading效果
            var noRoleSelect = $("#noRole option");
            var yesRoleSelect = $("#yesRole option");
            if($("#yesRole option").length <= 0){
                $.message({
                    message : "请指定角色",
                    type:'warning'
                });
                $(btn_submit).button('reset');//取消提交按钮的loading效果
                return false;
            }
        },
        callback:function(data){
            if(data.code == 200){
                $.message("数据提交成功");
                userRoleForm.resetForm();
            }else{
                $.message({
                    message:data.msg,
                    type:'error'
                });
            }
            $(btn_submit).button('reset');//取消提交按钮的loading效果
        }
    });

    /**
     * 获取选择的角色，赋予隐藏域
     */
    function getSelectRole(){
        var ids = "";
        var yesOptions = $("#yesRole option");
        if(yesOptions.length > 0){
            for(var i = 0,len = yesOptions.length; i < len; i++){
                var roleId = $(yesOptions[i]).val();
                ids += roleId;
                if(i < yesOptions.length - 1){
                    ids += ",";
                }
            }
        }
        $("input[name='roleIds']").val(ids);
    }
</script>