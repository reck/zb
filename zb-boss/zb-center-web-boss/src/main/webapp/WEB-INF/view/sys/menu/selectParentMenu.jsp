<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <title>选择父菜单</title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/validform/Validform.js" type="text/javascript"></script>
</head>
<body>
<div class="clearfix">
    <div id="scrollsidebar" style="margin-right: 10px;">
        <div class="side_content" style="margin-top: 0px;">
            <div class="side_list">
                <div>
                    <div class="widget-main padding-8">
                        <c:if test="${menuId != null && menuId != ''}">
                            <div class="alert alert-info" style="text-align: left;margin-top: 0px;margin-bottom: 10px;">
                                <p>当前编辑的菜单&nbsp;<span style="color: #D36A5A;font-weight: bold;">${menu.name}</span>&nbsp;&nbsp;已经通过&nbsp;<i class="fa fa-stop" style="color: #FFAE65;"></i>&nbsp;颜色块标注.</p>
                            </div>
                        </c:if>
                        <div  id="menuTree" class="ztree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--此form用于提交按钮的处理-->
    <div style="display: none;">
        <form action="" method="post" id="form-menu-selectParent">
            <ul>
                <li class="clearfix" style="display: none;">
                    <div class="col-xs-2 col-lg-2">&nbsp;</div>
                    <div class="col-xs-9">
                        <button class="btn btn-primary" type="button" data-loading-text="提交中..." id="selectParentMenu_btn_submit"><i class="fa fa-save"></i>&nbsp;提交信息</button>
                    </div>
                </li>
            </ul>
        </form>
    </div>
</div>
<input type="hidden" des="选中的父菜单id" name="selectParentMenuId"  id="selectParentMenuId" value=""/>
<input type="hidden" des="选中的父菜单name" name="selectParentMenuName"  id="selectParentMenuName" value=""/>
<input type="hidden" des="菜单id" name="menuId"  id="menuId" value="${menuId}"/>
<input type="hidden" des="获取菜单列表Tree" name="getMenuTreeUrl"  id="getMenuTreeUrl" value="${ctx}/menu/getMenuTree"/>
<input type="hidden" name="menus" value='' des="菜单列表tree的json字符串，这里value必须是单引号，避免与menus值中的双引号冲突，出现问题"/>
</body>
</html>


<script>

    $(document).ready(function() {
        //滚动条美化
        $("html").niceScroll({
            cursorcolor:"#888888",
            cursoropacitymax:1,
            touchbehavior:false,
            cursorwidth:"5px",
            cursorborder:"0",
            cursorborderradius:"5px"
        });
        $("html").css("overflow","auto");
        getMenuTreeData();//获取对应的应用的权限数据
    });

    /**
     * 获取菜单tree数据
     */
    function getMenuTreeData(){
        $.ajax({
            url : $("#getMenuTreeUrl").val(),
            async:true,
            data:{
                "menuId" : $("#menuId").val(),
                "open" : true,
                "showTopParent" : true //显示顶级菜单：无上级权限
            },
            dataType:'json',
            success:function(data,textStatus,jqXHR){
                $("input[name='menus']").val(data);
                loadTree();
            },
            error:function(xhr,textStatus){}
        });
    }

    /**
     * 加载菜单tree
     */
    function loadTree(){
        var setting = {
            check : {
                enable : false
            },
            data : {
                simpleData : {
                    enable : true
                }
            },
            callback : {
                onClick : function (event, treeId, treeNode) {
                    //设置选择的菜单节点背景色
                    $("#selectParentMenuId").val(treeNode.id);
                    $("#selectParentMenuName").val(treeNode.name);
                }
            }
        };
        var roles = $("input[name='menus']").val();
        $.fn.zTree.init($("#menuTree"), setting, eval("(" + roles + ")"));

        var zTree = $.fn.zTree.getZTreeObj("menuTree"), type = {"Y" : "ps","N" : "s"};
        zTree.setting.check.chkboxType = type;

        //tree加载显示完毕后，突出显示当前菜单节点的背景色
        var treeObj = $.fn.zTree.getZTreeObj("menuTree");
        var node = treeObj.getNodeByParam("id", $("#menuId").val(), null);
        if(node != null){
            $("#" + node.tId + "_a").css("backgroundColor","#FFAE65").css("color","#fff");//设置选中的节点的背景颜色
        }
    }

    //表单验证提交
    var btn_submit = "#selectParentMenu_btn_submit";//提交按钮id
    $("#form-menu-selectParent").Validform({
        postonce:true, //可选项 表单是否只能提交一次，true开启，不填则默认关闭;
        btnSubmit:btn_submit,
        beforeSubmit:function(curform){
            checkSelectParentMenu();
            return false;//form无需提交
        }
    });


    /**
     * 检查是否选择了父菜单
     */
    function checkSelectParentMenu() {
        var selectParentMenuId = $("#selectParentMenuId").val();
        if(selectParentMenuId == ""){
            $.message({
                message:"请先选择父菜单",
                type:'error'
            });
            return false;
        }
        if(selectParentMenuId == $("#menuId").val()){
            $.message({
                message:"不能选择自己为父菜单",
                type:'error'
            });
            return false;
        }
        selectParentMenuSuccess();
    }

    /**
     * 菜单选择成功后的回调处理
     */
    function selectParentMenuSuccess() {
        var selectParentMenuId = $("#selectParentMenuId").val();
        var selectParentMenuName = $("#selectParentMenuName").val();
        window.parent.selectParentMenuCallback(selectParentMenuId,selectParentMenuName);
    }
</script>
