<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <title>角色编辑</title>

    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/jquery-fixed.js" type="text/javascript" ></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/validform/Validform.js" type="text/javascript"></script>
</head>
<body>
<div class="clearfix">
    <div id="scrollsidebar" class="left_Treeview" style="margin-right: 10px;">
        <div class="show_btn" id="rightArrow"><span></span></div>
        <div class="widget-box side_content" style="margin-top: 0px;">
            <div class="side_title"><a title="隐藏" class="close_btn"><span></span></a></div>
            <div class="side_list">
                <div class="widget-header header-color-green2">
                    <span style="font-size: 13px;">角色拥有的权限</span>
                </div>
                <div>
                    <div class="widget-main padding-8">
                        <div  id="permissionTree" class="ztree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="category" style="border: 1px solid #dddddd;height: 100%;">
        <div class="type_style" style="height: 100%;">
            <div class="type_title" style="font-size: 14px;">角色详细信息</div>
            <div class="type_content" style="margin-top: 0px;width: 100%;">
                <div class="margin" id="page_style">
                    <div class="add_style" style="float:left;width: 100%;">
                        <form action="${ctx}/role/updateRole" method="post" id="form-role-update">
                            <input type="hidden" des="角色id" value="${role.id}" id="id" name="id"/>
                            <input type="hidden" des="应用ID" id="appId" name="appId" value=""/>
                            <input type="hidden" des="存放选中的权限id集合" id="permissionIds" name="permissionIds" value=""/>
                            <ul>
                                <li class="clearfix" style="margin-top: 0px;">
                                    <div class="alert alert-info" style="text-align: left;margin-top: 0px;margin-bottom: 10px;">
                                        <p><i class="fa fa-hand-o-right"></i>&nbsp;左侧节点颜色含义说明：</p>
                                        <p>
                                            <i class="fa fa-stop" style="color: #AAE5B7;"></i> 角色原本拥有的权限
                                            <i class="fa fa-stop" style="color: #FCC9C9;margin-left: 20px;"></i> 取消原来拥有的权限
                                            <i class="fa fa-stop" style="color: #F0F0A0;margin-left: 20px;"></i> 新增原本没有的权限
                                        </p>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>所属应用：</label>
                                    <div class="formControls col-xs-6">
                                        <span class="select-box" style="width:150px;">
                                            <select class="select col-xs-12" name="appIdSelect" size="1" style="height: 30px;line-height: 30px;" disabled="disabled">
                                                <c:forEach items="${apps}" var="app">
                                                    <option value="${app.id}" <c:if test="${app.id == role.appId}">selected="selected"</c:if> >${app.name}</option>
                                                </c:forEach>
                                            </select>
                                        </span>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>角色名称：</label>
                                    <div class="formControls col-xs-6">
                                        <input type="text" placeholder="请输入角色名称" name="name" id="name" autocomplete="off" value="${role.name}" class="input-text col-xs-12" datatype="*" nullmsg="角色名称不能为空">
                                    </div>
                                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>角色描述：</label>
                                    <div class="formControls col-xs-6">
                                        <input type="text" placeholder="请输入角色描述" name="description" id="description" autocomplete="off" value="${role.description}" class="input-text col-xs-12" datatype="*" nullmsg="角色描述不能为空">
                                    </div>
                                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                                </li>
                                <li class="clearfix">
                                    <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>状态：</label>
                                    <div class="formControls col-xs-6">
                                        <span class="select-box" style="width:150px;">
                                            <select class="select" name="status" size="1">
                                                <c:forEach items="${status}" var="s">
                                                    <option value="${s}" <c:if test="${role.status == s}">selected="selected"</c:if>>${s.description}</option>
                                                </c:forEach>
                                            </select>
                                        </span>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="alert alert-success" style="text-align: left;margin-top: 10px;">
                                        <p><i class="fa fa-exclamation-triangle"></i>&nbsp;说明：</p>
                                        <p>1、左侧菜单中绿色标识的部分表示当前角色拥有的权限，您也可以手动修改角色的权限.</p>
                                        <p>2、为了避免随意修改角色权限带来的功能操作问题，请考虑周全后再做权限修改.</p>
                                    </div>
                                </li>
                                <li class="clearfix" style="display: none;">
                                    <div class="col-xs-2 col-lg-2">&nbsp;</div>
                                    <div class="col-xs-9">
                                        <button class="btn btn-primary" type="button" data-loading-text="提交中..." id="btn_submit"><i class="fa fa-save"></i>&nbsp;提交信息</button>
                                        <button onclick="javascript:history.back(-1);" class="btn btn-warning" type="button"><i class="fa fa-reply"></i>&nbsp;返回上一步</button>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" des="获取对应应用下的权限列表URL" name="getPermissionTreeUrl"  id="getPermissionTreeUrl" value="<c:url value="/role/getPermissionTree"/>"/>
<input type="hidden" name="permissions" value='${permissions}' des="权限列表json字符串，这里value必须是单引号，避免与permissions值中的双引号冲突，出现问题"/>
</body>
</html>


<script>

    $(document).ready(function() {
        //滚动条美化
        $(".type_style").niceScroll({
            cursorcolor:"#888888",
            cursoropacitymax:1,
            touchbehavior:false,
            cursorwidth:"5px",
            cursorborder:"0",
            cursorborderradius:"5px"
        });
        $("html").css("overflow","auto");
        //当前页面左侧菜单收缩功能
        $("#scrollsidebar").fix({
            float : 'left',
            //minStatue : true,
            skin : 'green',
            spacingw:30,//设置隐藏时的距离
            durationTime :false
        });

        $("#appId").val($("select[name='appIdSelect'] option:selected").val());
        getPermissionTreeData();//加载显示权限tree
    });

    /**
     * 获取应用的权限tree数据
     */
    function getPermissionTreeData(){
        $.ajax({
            url : $("#getPermissionTreeUrl").val(),
            async:true,
            data:{
                "appId" : $("#appId").val(),//默认获取所属应用下拉框的第一个应用权限数据
                "roleId" : $("#id").val(),
                "showTopParent" : false, //是否显示最顶级的节点【无上级权限】
                "open" : false //节点是否展开
            },
            dataType:'json',
            success:function(data,textStatus,jqXHR){
                $("input[name='permissions']").val(data);
                loadTree();
            },
            error:function(xhr,textStatus){}
        });
    }

    //表单验证提交
    var btn_submit = "#btn_submit";//提交按钮id
    $("#form-role-update").Validform({
        ajaxPost:true,
        tiptype:2,
        showAllError:true,
        postonce:true, //可选项 表单是否只能提交一次，true开启，不填则默认关闭;
        btnSubmit:btn_submit,
        beforeSubmit:function(curform){
            permissionNodeSelect();//提交之前，赋予hidden最新的权限选中的id列表。用户若未修改任何权限的情况下直接提交，需要将之前权限一同提交。
        },
        callback:function(data){
            if(data.code == 200){
                $.message({
                    message:"保存成功",
                    time : 5000
                });
                //$.message("保存成功，权限已经实时生效了哦^_^");
                getPermissionTreeData();
            }else{
                $.message({
                    message:data.msg,
                    type:'error'
                });
                return false;
            }
            window.parent.searchData();//重新加载数据表
        }
    });


    /**
     * 加载权限tree
     */
    function loadTree(){
        var setting = {
            check : {
                enable : true
            },
            data : {
                simpleData : {
                    enable : true
                }
            },

            view: {
                fontCss: setFontCss
            },
            callback : {
                onCheck : function(event, treeId, treeNode){//节点被点击后，设置选择的节点id
                    permissionNodeSelect();//改变了节点的check状态，更新隐藏域的值
                    reloadNodeCheckChangeBackColor(treeNode);//设置check被改变的节点背景色
                }
            }
        };
        var roles = $("input[name='permissions']").val();
        $.fn.zTree.init($("#permissionTree"), setting, eval("(" + roles + ")"));

        var zTree = $.fn.zTree.getZTreeObj("permissionTree"), type = {"Y" : "ps","N" : "s"};
        zTree.setting.check.chkboxType = type;

        //tree加载显示完毕后，移除[无上级节点]这个节点
        /*var treeObj = $.fn.zTree.getZTreeObj("permissionTree");
        var node = treeObj.getNodeByParam("id", -1, null);
        if(node != null){
            treeObj.removeNode(node);
        }*/
    }

    /**
     * 递归方法。获取所有子节点，并还原子节点背景色为白色
     */
    function changeChildrenNodesBackColor(treeNode,bool){
        if (treeNode.isParent) {
            var childrenNodes = treeNode.children;
            if (childrenNodes) {
                for (var i = 0,len = childrenNodes.length; i < len; i++) {
                    var checkedOld = childrenNodes[i].checkedOld;//tree初始化的时候的最初状态
                    console.log(checkedOld + "\t" + bool);
                    if(bool){
                        if(checkedOld){
                            console.log("警告提示:" + childrenNodes[i].name);
                            $("#" + childrenNodes[i].tId + "_a").css("backgroundColor","#FCC9C9");//取消选中的话，如果是之前有的权限，则突出显示取消的权限节点
                        }else{
                            $("#" + childrenNodes[i].tId + "_a").css("backgroundColor","#fff");//取消选中的话，还原背景色为白色
                        }
                    }else {
                        if(checkedOld){
                            $("#" + childrenNodes[i].tId + "_a").css("backgroundColor","#AAE5B7");//角色之前已经有的权限
                        }else{
                            $("#" + childrenNodes[i].tId + "_a").css("backgroundColor","#F0F0A0");//角色之前没有的权限
                        }

                    }
                    if(childrenNodes[i].checked){
                        changeChildrenNodesBackColor(childrenNodes[i],false);
                    }else{
                        changeChildrenNodesBackColor(childrenNodes[i],true);
                    }
                }
            }
        }
    }

    /**
     * 递归方法。设置父节点的背景色
     */
    function changeParentNodesBackColor(treeNode){
        var parentNode = treeNode.getParentNode();
        if(parentNode == null){
            return;
        }
        if(treeNode.checked){
            if(parentNode.checkedOld){
                $("#" + parentNode.tId + "_a").css("backgroundColor","#AAE5B7");//设置选中的节点的背景颜色
            }else{
                $("#" + parentNode.tId + "_a").css("backgroundColor","#F0F0A0");//设置选中的节点的背景颜色
            }
            changeParentNodesBackColor(parentNode);
        }
    }
    /**
     * 设置check被改变的节点背景色
     */
    function reloadNodeCheckChangeBackColor(treeNode) {
        var checkedOld = treeNode.checkedOld;//tree初始化的时候的最初状态
        if(checkedOld == false){
            if(treeNode.checked){
                $("#" + treeNode.tId + "_a").css("backgroundColor","#F0F0A0");
                changeChildrenNodesBackColor(treeNode,false);//遍历子节点，子节点也要还原与之一样的背景色
            }else{
                $("#" + treeNode.tId + "_a").css("backgroundColor","#fff");
                changeChildrenNodesBackColor(treeNode,true);//遍历子节点，子节点也要还原与之一样的背景色
            }
            changeParentNodesBackColor(treeNode);//如果当前节点是飞父节点的话，则同时需要设置父节点的背景色
        }else{
            if(treeNode.checked){
                $("#" + treeNode.tId + "_a").css("backgroundColor","#AAE5B7");//设置选中的节点的背景颜色
                changeChildrenNodesBackColor(treeNode,false);//遍历子节点，子节点的背景色还原至最初背景色
                changeParentNodesBackColor(treeNode);//如果当前节点是飞父节点的话，则同时需要设置父节点的背景色
            }else{
                $("#" + treeNode.tId + "_a").css("backgroundColor","#FCC9C9");
                changeChildrenNodesBackColor(treeNode,true);//遍历子节点，子节点的背景色还原至最初背景色
            }
        }
    }
    /**
     * tree加载的时候，设置角色拥有的权限节点的背景色，突出显示
     * @param treeId tree对象的id
     * @param treeNode 每一个node节点对象
     * @returns {{background-color: string}}
     */
    function setFontCss(treeId, treeNode) {
        if(treeNode.checked){
            return {'background-color':'#AAE5B7'};//设置当前编辑的权限的节点背景色，突出显示
        }
    }

    /**
     * 选中权限tree节点的事件，赋予hidden的值为最新选中的权限id列表
     */
    function permissionNodeSelect() {
        var numberval = 0;
        var treeRole = $.fn.zTree.getZTreeObj("permissionTree"),
            roles = treeRole.getCheckedNodes(true),
            pmss = "";
        if (roles.length > 0) {
            for (var i = 0; i < roles.length; i++) {
                pmss += roles[i].id + ",";
            }
            if (pmss.length > 0) {
                pmss = pmss.substring(0, pmss.length - 1);
            }
            $("#permissionIds").val(pmss);
        } else {
            $("#permissionIds").val("");
            numberval++;
        }
        if (numberval > 0) {
            return false;
        }
        return true;
    }
</script>
