﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>菜单列表</title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <link href="http://v3.bootcss.com/dist/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="${_cdnStaticResourcesPath}/resources/static/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/jquery.treegrid.css">

    <style>
        .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td{
            color: #000000;
            padding: 5px;
        }
        /**表头的高度*/
        .table thead > tr > th,.table thead > tr > td{line-height: 1.2!important;}
        /** 表格的操作按钮的内边距 */
        .btn{
            padding: 3px 10px;
        }
        /**顶部操作按钮的内边距*/
        .border .btn{
            padding: 4px 10px;
            margin-left: 10px;
        }
        /** 避免其他css的全局样式对当前table的td的对齐方式产生影响 */
        .table.table-bordered tbody tr td:nth-child(3){/**菜单名称左对齐*/
            text-align: left!important;
            padding-left: 20px;
        }
        .table.table-bordered tbody tr td:nth-child(4){/**菜单url左对齐*/
            text-align: left!important;
            padding-left: 20px;
        }
        .table.table-bordered tbody tr td:nth-child(5){/**权限代码左对齐*/
            text-align: left!important;
            padding-left: 20px;
        }


        /**行选中的背景色*/
        .treegrid-selected{
            background-color: #C1E3C1!important;
        }
        .table-striped > tbody > tr:nth-child(2n+1) > td{
            background-color: inherit;
        }
    </style>

    <script src="${_cdnStaticResourcesPath}/resources/static/js/jquery-fixed.js" type="text/javascript" ></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/jquery.treegrid.min.js"></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/jquery.treegrid.bootstrap3.js"></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/jquery.treegrid.extension.js"></script>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/treegrid/tree.table.js"></script>

</head>

<body id="permissionlist_frame_box" style="height: 100%;">
<div class="page-content clearfix">
    <div id="menu_list_style">
        <div class="search_style" style="background-color: #fff;">
            <div class="title_names">搜索查询</div>
            <ul class="search_content clearfix" style="margin-bottom: 0px!important;">
                <li>
                    <label class="l_f" style="font-weight: inherit;margin-bottom: 0px!important;">所属应用</label>
                    <div class="col-xs-4">
						<span class="select-box">
							<select class="select" id="appId" name="appId" style="height: 30px;line-height: 30px;">
								<option value="">全部</option>
								<c:forEach items="${apps}" var="app">
                                    <option value="${app.id}">${app.name}</option>
                                </c:forEach>
							</select>
						</span>
                    </div>
                </li>
                <li style="width:90px;margin-left: 20px;"><button type="button" class="btn_search" style="height: 30px;line-height: 30px;"><i class="icon-search"></i>查询</button></li>
            </ul>
        </div>
        <!--功能按钮-->
        <div class="border clearfix" style="margin-top: 20px;">
			<span class="l_f">
				<a style="background-color: #fff!important;border-color: #ccc!important;color:#333!important;text-shadow: none!important;" href="javascript:void(0);" onfocus="this.blur();" title="刷新菜单" onclick="getGrid();" class="btn btn-default"><i class="icon-refresh"></i>刷新列表</a>
                <a href="javascript:void(0);" onfocus="this.blur();" title="新增菜单" onclick="menuAdd();" class="btn btn-success"><i class="icon-plus"></i>新增菜单</a>
                <a style="background-color: #428BCA!important;border-color: #428BCA!important;" href="javascript:void(0);" onfocus="this.blur();" title="编辑菜单" onclick="menuEdit();" class="btn btn-info"><i class="icon-edit"></i>编辑菜单</a>
                <a style="background-color: #199!important;border-color: #199!important;" href="javascript:foldingAll()" onfocus="this.blur();" title="折叠所有菜单" onclick="" class="btn btn-info"><i class="icon-minus-sign"></i>折叠所有菜单</a>
                <a style="background-color: #199!important;border-color: #199!important;" href="javascript:openAll()" onfocus="this.blur();" title="展开所有菜单" onclick="" class="btn btn-info"><i class="icon-plus-sign"></i>展开所有菜单</a>
			</span>
        </div>

        <!--表格数据-->
        <div class="table_menu_list" id="menuListIframe">
            <table id="menuDataGrid"></table>
        </div>
    </div>
</div>

<input type="hidden" des="获取菜单列表url" id="getMenuListUrl" value="${ctx}/menu/list"/>
<input type="hidden" des="编辑菜单信息url" id="toEditMenuUrl" value="${ctx}/menu/toEditView"/>
<input type="hidden" des="添加菜单url" id="toAddMenuUrl" value="${ctx}/menu/toAddView"/>
<input type="hidden" des="删除菜单url" id="deleteMenuUrl" value="${ctx}/menu/deleteMenu"/>
</body>
</html>

<script>
    $(function () {
        getGrid();

        /**
         * 浏览器出口缩放的时候，table表格自适应窗口。以免出现数据显示不全
         */
        $(window).resize(function () {
            $("#menuDataGrid").css("height",$(window).height() - 300);
            $("#menuDataGrid tbody").css("height",$(window).height() - 300);
        });

        //条件查询
        $(".btn_search").click(function (){
            getGrid(true);
        });
    });

    /**
     * 获取数据列表
     */
    function getGrid(folding) {
        //先清空之前已经初始化好的table，不然的话每次初始化的时候，表格的border会出现递增加1px
        $("#menuListIframe").html("<table id=\"menuDataGrid\"></table>");//加入表格容器
        var colunms = TreeGrid.initColumn();
        var table = new TreeTable(TreeGrid.id, $("#getMenuListUrl").val() + "?appId=" + $("#appId").val(), colunms);
        table.setExpandColumn(2);
        table.setIdField("id");
        table.setCodeField("id");
        table.setParentCodeField("parentId");
        table.setExpandAll(folding);
        table.setHeight($(document).height()-300);
        table.init();
        TreeGrid.table = table;
    }


    var TreeGrid = {
        id: "menuDataGrid",
        table: null,
        layerIndex: -1
    };

    /**
     * 初始化表格的列
     */
    TreeGrid.initColumn = function () {
        var columns = [
            {field: 'selectItem', radio: true},
            {title: '编号', field: 'id', visible: false, align: 'center', valign: 'middle', width: '5%'},
            {title: '名称', field: 'name', align: 'center', valign: 'middle', width: '15%'},
            {title: '菜单URL', field: 'url', align: 'center', valign: 'middle', width: '18%'},
            {title: '权限代码', field: 'permissionCode', align: 'center', valign: '10%'},
            {title: '类型', field: 'type', align: 'center', valign: 'middle', width: '5%', formatter: function(item, index){
                if(item.type == 'DIRECTORY'){
                    return '<span style="font-size: 90%;" class="label label-primary">目录</span>';
                }
                if(item.type == 'MENU'){
                    return '<span style="font-size: 90%;" class="label label-success">菜单</span>';
                }
            }},
            {title: '图标', field: 'icon', align: 'center', valign: 'middle', width: '5%', formatter: function(item, index){
                return item.icon == null ? '' : '<i class="'+item.icon+' fa-lg"></i>';
            }},
            {title: '排序', field: 'sort', align: 'center', valign: 'middle', width: '5%'},
            {title: '创建时间', field: 'createTime', align: 'center', valign: 'middle', width: '10%'},
            {title: '更新时间', field: 'updateTime', align: 'center', valign: 'middle', width: '10%'},
            {title: '操作', field: 'id', valign: 'middle', width: '10%', formatter: function(item, index){
                return "<a style=\"background-color: #428BCA!important;border-color: #428BCA!important;\" class=\"btn btn-xs btn-info\" onClick=\"menuEdit(" + item.id + ")\" href=\"javascript:;\" title=\"编辑菜单信息\"><i class=\"icon-edit bigger-120\"></i></a>"
                    + "<a class=\"btn btn-xs btn-danger\" onClick=\"menuDelete(" + item.id + ")\" href=\"javascript:;\" title=\"删除\"><i class=\"icon-trash bigger-120\"></i></a>";
            }
            }]
        return columns;
    };

    /**
     * 折叠所有菜单
     */
    function foldingAll(){
        getGrid(false);
    }

    /**
     * 展开所有权限
     */
    function openAll(){
        getGrid(true);
    }

    /**
     * 新增菜单
     */
    function menuAdd() {
        var loadUniqe;
        zeroModal.show({
            title: '新增菜单',
            iframe: true,
            url: $("#toAddMenuUrl").val(),
            opacity: 0.8,
            width: '35%',
            height: '60%',
            top: '15%',
            buttons: [{
                className: 'zeromodal-btn zeromodal-btn-primary',
                name: '保存',
                fn: function(opt) {
                    loadUniqe = zeroModal.loading();//loading加载
                    $("#zeromodalIframe").contents().find("#btn_submit").click();//菜单添加页面中的form提交按钮
                    zeroModal.close(loadUniqe); // 关闭loading加载
                    return false;//需要加上return false，防止弹出框自动关闭，不然提交成功等提示信息将不会显示
                }
            }, {
                className: 'zeromodal-btn zeromodal-btn-default',
                name: '取消',
                fn: function(opt) {
                    zeroModal.close(loadUniqe); // 关闭等待框
                }
            }]
        });
    }
    /*菜单-编辑*/
    function menuEdit(menuId){
        if(menuId == "" || menuId == undefined){
            var selectRow = TreeGrid.table.getSelectedRow();
            if(selectRow == undefined || selectRow[0] == undefined){
                $.message({
                    message:"请选择一个菜单",
                    type:'warning'
                });
                return false;
            }else{
                menuId = selectRow[0].id;
            }
        }

        var loadUniqe;
        zeroModal.show({
            title: '编辑菜单信息',
            iframe: true,
            url: $("#toEditMenuUrl").val() + "?menuId=" + menuId,
            opacity: 0.8,
            width: '35%',
            height: '60%',
            top: '15%',
            buttons: [{
                className: 'zeromodal-btn zeromodal-btn-primary',
                name: '保存',
                fn: function(opt) {
                    loadUniqe = zeroModal.loading();//loading加载
                    $("#zeromodalIframe").contents().find("#btn_submit").click();//菜单编辑页面中的form提交按钮
                    zeroModal.close(loadUniqe); // 关闭loading加载
                    return false;//需要加上return false，防止弹出框自动关闭，不然提交成功等提示信息将不会显示
                }
            }, {
                className: 'zeromodal-btn zeromodal-btn-default',
                name: '取消',
                fn: function(opt) {
                    zeroModal.close(loadUniqe); // 关闭等待框
                }
            }]
        });
    }


    /**
     * 选择父菜单弹出框。该方法在菜单编辑页面中调用。由于zeroModal插件不支持在父页面弹出窗口。故需要这么处理。
     */
    var selectParentMenuZeroModalUnique;
    function selectParentMenu(url,menuId) {
        if(url == ""){
            return;
        }
        var loadUniqe;
        selectParentMenuZeroModalUnique = zeroModal.show({
            title: '选择父菜单',
            iframe: true,
            url: url + "?menuId=" + menuId,
            opacity: 0.8,
            width: '300px',
            height: '70%',
            top: '10%',
            buttons: [{
                className: 'zeromodal-btn zeromodal-btn-primary',
                name: '确定',
                fn: function(opt) {
                    loadUniqe = zeroModal.loading();//loading加载
                    window.frames[1].document.getElementById("selectParentMenu_btn_submit").click();//【选择父菜单】页面中的form提交按钮
                    zeroModal.close(loadUniqe); // 关闭loading加载
                    return false;//需要加上return false，防止弹出框自动关闭，不然提交成功等提示信息将不会显示
                }
            }, {
                className: 'zeromodal-btn zeromodal-btn-default',
                name: '取消',
                fn: function(opt) {
                    zeroModal.close(loadUniqe); // 关闭等待框
                }
            }]
        });
    }
    /**
     * 该方法在选择父菜单的页面中调用（菜单编辑页面中的父菜单功能），由于处于不同的iframe，故特殊处理
     * @param selectParentMenuId 选择的父菜单的id
     * @param selectParentMenuName 选择的父菜单的name
     */
    function selectParentMenuCallback(selectParentMenuId,selectParentMenuName) {
        window.frames[0].document.getElementById("parentId").value = selectParentMenuId;
        window.frames[0].document.getElementById("parentIdInput").value = selectParentMenuName;
        zeroModal.close(selectParentMenuZeroModalUnique);//关闭选择父菜单的弹出框
    }


    /**
     * 菜单删除
     * @param menuId
     */
    function menuDelete(menuId) {
        $("body").dialog({
            type: "danger",
            title: "系统提示",
            discription: '确定要删除该菜单吗？',
            animateIn: "fadeInRight-hastrans",//rotateInUpLeft-hastrans
            animateOut: "rotateOutUpLeft-hastrans",
            showBoxShadow:true,
            buttons: [{name: '确定',className: 'defalut'}, {name: '取消', className: 'reverse'}]
        },function(ret) {
            if(ret.index===0){
                $.ajax({
                    type : "post",
                    url : $("#deleteMenuUrl").val(),
                    dataType : "json",
                    data : {
                        "menuId" : menuId
                    },
                    success : function(result) {
                        if (result.code == 200) {
                            $.message("删除成功");
                            getGrid();//重新加载table数据
                        } else {
                            $.message({
                                message:result.msg,
                                type:'error'
                            });
                        }
                    }
                });

            }else{
                return false;
            }
        });
    }
</script>