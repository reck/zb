﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>编辑用户信息</title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/validform/Validform.js" type="text/javascript"></script>

    <style>
        .add_style li{
            margin-top: 25px;
        }
        html{
            overflow: auto;
        }
    </style>
</head>
<body>
<div class="margin" id="page_style">
    <div class="add_style" style="width:600px; padding-right:10px; float:left;">
        <form action="${ctx}/user/updateUser" method="post" id="form-user-update">
            <input type="hidden" des="存放用户userId" value='${user.id}' id="userId" name="userId"/>
            <ul>
                <li class="clearfix">
                    <label class="label_name col-xs-2 col-lg-2"><i>*</i>登录账户：</label>
                    <div class="formControls col-xs-6">
                        <input <c:if test="${user.userName == 'admin'}">disabled="disabled"</c:if> type="text" class="input-text col-xs-12" value="${user.userName}" placeholder="请输入登录账户" id="userName" name="userName" datatype="*2-16" nullmsg="登录账户不能为空"></div>
                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                </li>
                <li class="clearfix">
                    <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>用户姓名：</label>
                    <div class="formControls col-xs-6">
                        <input <c:if test="${user.userName == 'admin'}">disabled="disabled"</c:if>  type="text" placeholder="请输入用户姓名" name="realName" id="realName" autocomplete="off" value="${user.realName}" class="input-text col-xs-12" datatype="*" nullmsg="用户姓名不能为空">
                    </div>
                    <div class="col-4"> <span class="Validform_checktip"></span></div>
                </li>
                <li class="clearfix">
                    <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>账户状态：</label>
                    <div class="formControls col-xs-6">
                            <span class="select-box" style="width:150px;">
                                <select <c:if test="${user.userName == 'admin'}">disabled="disabled"</c:if>  class="select" name="status" size="1" <c:if test="${userHaveAdminRole == true}">disabled="disabled"</c:if>>
                                    <c:forEach items="${userStatus}" var="s">
                                        <option value="${s}" <c:if test="${user.status == s}">selected="selected"</c:if> >${s.description}</option>
                                    </c:forEach>
                                </select>
                            </span>
                    </div>
                </li>
                <li class="clearfix" style="display: none;">
                    <div class="col-xs-2 col-lg-2">&nbsp;</div>
                    <div class="col-xs-9">
                        <button class="btn btn-primary" type="button" data-loading-text="提交中..." id="btn_submit"><i class="fa fa-save"></i>&nbsp;提交信息</button>
                        <button onclick="javascript:history.back(-1);" class="btn btn-warning" type="button"><i class="fa fa-reply"></i>&nbsp;返回上一步</button>
                    </div>
                </li>
            </ul>
        </form>
    </div>
</div>
</body>
</html>


<script>
    /*******滚动条*******/
    $("body").niceScroll({
        cursorcolor:"#888888",
        cursoropacitymax:1,
        touchbehavior:false,
        cursorwidth:"5px",
        cursorborder:"0",
        cursorborderradius:"5px"
    });

    //表单验证提交
    var btn_submit = "#btn_submit";//提交按钮id
    $("#form-user-update").Validform({
        ajaxPost:true,
        tiptype:2,
        showAllError:true,
        postonce:true, //可选项 表单是否只能提交一次，true开启，不填则默认关闭;
        btnSubmit:btn_submit,
        beforeSubmit:function(curform){
            $(btn_submit).button("loading");//提交按钮的loading效果
        },
        callback:function(data){
            $(btn_submit).button('reset');//取消提交按钮的loading效果
            if(data.code == 200){
                $.message("保存成功");
            }else{
                $.message({
                    message:data.msg,
                    type:'error'
                });
                return false;
            }
            window.parent.userListTable.draw();//重新加载数据表
        }
    });
</script>
