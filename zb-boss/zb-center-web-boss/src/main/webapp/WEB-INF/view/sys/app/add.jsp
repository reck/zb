﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>添加应用</title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="${_cdnStaticResourcesPath}/resources/static/js/plugins/validform/Validform.js" type="text/javascript"></script>
</head>
<body>
    <div class="margin" id="page_style">
        <div class="add_style" style="width:600px; padding-right:10px; float:left;">
            <div class="title_name" style="border-bottom: 1px solid #dddddd;font-size: 18px;">添加应用</div>
            <form action="${ctx}/app/addApp" method="post" id="form-app-add">
                <ul>
                    <li class="clearfix" style="margin-top: 10px;">
                        <div class="alert alert-success" style="text-align: left;margin-top: 0px;margin-bottom: 10px;">
                            <p><span style="color: #F752AD;font-weight: bold;">【&nbsp;应用编码&nbsp;】</span>说明：</p>
                            <p>1、该属性表示的是每个应用系统的唯一标识，具有一定的功能需要，若没有特别需求请勿擅自修改，以免影响业务功能.</p>
                            <p>2、在当前系统中的应用是：角色权限编辑过程中，结合MQ实现在不同客户端下对应角色的登录账户的权限实时更新.</p>
                        </div>
                    </li>
                    <li class="clearfix">
                        <label class="label_name col-xs-2 col-lg-2"><i>*</i>应用名称：</label>
                        <div class="formControls col-xs-6">
                            <input type="text" class="input-text col-xs-12" value="" placeholder="请输入应用名称" id="name" name="name" datatype="*2-16" nullmsg="应用名称不能为空"></div>
                        <div class="col-4"> <span class="Validform_checktip"></span></div>
                    </li>
                    <li class="clearfix">
                        <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>应用编码：</label>
                        <div class="formControls col-xs-6">
                            <input type="text" placeholder="请输入应用编码" name="code" id="code" autocomplete="off" value="" class="input-text col-xs-12" datatype="*" nullmsg="应用编码不能为空">
                        </div>
                        <div class="col-4"> <span class="Validform_checktip"></span></div>
                    </li>
                    <li class="clearfix" style="display: none;">
                        <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>应用ICON图标：</label>
                        <div class="formControls col-xs-6">
                            <input type="text" placeholder="请输入应用ICON图标" name="icon" id="icon" autocomplete="off" value="" class="input-text col-xs-12" >
                        </div>
                        <div class="col-4"> <span class="Validform_checktip"></span></div>
                    </li>
                    <li class="clearfix">
                        <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>应用排序：</label>
                        <div class="formControls col-xs-6">
                            <input type="text" class="input-text col-xs-12" value="" placeholder="请输入应用排序" id="sortNum" name="sortNum" datatype="n" nullmsg="应用排序不能为空">
                        </div>
                        <div class="col-4"> <span class="Validform_checktip"></span></div>
                    </li>
                    <li class="clearfix">
                        <label class="label_name col-xs-2 col-lg-2"><i class="c-red">*</i>是否可用：</label>
                        <div class="formControls col-xs-6">
                            <span class="select-box" style="width:150px;">
                                <select class="select" name="status" size="1">
                                    <c:forEach items="${status}" var="s">
                                        <option value="${s}">${s.description}</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                    </li>
                    <li class="clearfix">
                        <div class="col-xs-2 col-lg-2">&nbsp;</div>
                        <div class="col-xs-9">
                            <button class="btn btn-primary" type="button" data-loading-text="提交中..." id="btn_submit"><i class="fa fa-save"></i>&nbsp;提交信息</button>
                            <button onclick="javascript:window.location.href='${ctx}/app/toAppListView';" class="btn btn-warning" type="button"><i class="fa fa-reply"></i>&nbsp;返回上一步</button>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</body>
</html>


<script>
    /*******滚动条*******/
    $("body").niceScroll({
        cursorcolor:"#888888",
        cursoropacitymax:1,
        touchbehavior:false,
        cursorwidth:"5px",
        cursorborder:"0",
        cursorborderradius:"5px"
    });

    //表单验证提交
    var btn_submit = "#btn_submit";//提交按钮id
    var appAddForm = $("#form-app-add").Validform({
        ajaxPost:true,
        tiptype:2,
        showAllError:true,
        postonce:false, //可选项 表单是否只能提交一次，true开启，不填则默认关闭;
        btnSubmit:btn_submit,
        beforeSubmit:function(curform){
            $(btn_submit).button("loading");//提交按钮的loading效果
        },
        callback:function(data){
            if(data.code == 200){
                $.message("数据提交成功");
                appAddForm.resetForm();
            }else{
                $.message({
                    message:data.msg,
                    type:'error'
                });
            }
            $(btn_submit).button('reset');//取消提交按钮的loading效果
        }
    });
</script>
