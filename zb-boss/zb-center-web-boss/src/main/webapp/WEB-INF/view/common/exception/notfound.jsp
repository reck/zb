<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<head>
    <title>404 - 页面找不到</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        div.notice_page{
            position: absolute;
            top:30%;
            left:50%;
            width:100%;
            transform:translate(-50%,-50%);
        }
        div.notice_page .notice_404_p{height:450px;background:url('${_cdnStaticResourcesPath}/resources/static/images/404_02.png') center 70px no-repeat}
        div.notice_page .home_page{margin:0 auto;height:20px;width:300px;font-size: 14px;text-align: center;}
    </style>
</head>
<body>
<div class="notice_page">
    <div class="notice_404_p"></div>
    <%--<div class="home_page"><h3>404 - <span style="font-size: 14px;font-weight: normal;">友情提示：您访问的内容不存在</span></h3></div>--%>
</div>
</body>