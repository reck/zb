<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<head>
    <title>${errorMsg}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        .cont{margin:0 auto;width:500px;line-height:20px;}
        .c1{text-align:center}
        .cont p{text-align:center;color:#555;font-size:15px;font-weight:normal;height:20px}
        .c3{text-align:center;color:#898989;font-size:12px}
        .bg{
            position: absolute;
            top:45%;
            left:50%;
            width:100%;
            transform:translate(-50%,-50%);
        }
    </style>
</head>
<body>
<div class="bg">
    <div class="cont">
        <div class="c1"><img src="${_cdnStaticResourcesPath}/resources/static/images/error_exception.png" class="img1"></div>
        <p>哎呀…亲…系统突然出现了错误</p>
        <div class="c3">友情提醒您 - 管理员已经收到了系统发送的异常通知哦</div>
    </div>
</div>
</body>