<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<head>
    <title>${errorMsg}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        body, button, input, select, textarea {
            font:12px/1.5 tahoma, arial, "微软雅黑";
            color:#666
        }
        .center {
            text-align:center
        }
        .clear:after, .clearfix:after {
            content:".";
            display:block;
            clear:both;
            visibility:hidden;
            font-size:0;
            height:0;
            line-height:0
        }
        .clear, .clearfix {
            zoom:1
        }
        .b-panel {
            position:absolute
        }
        .b-fr {
            float:right
        }
        .b-fl {
            float:left
        }
        .error {
            background-color:#fff
        }
        .module-error {
            /*margin-top:200px*/
        }
        .module-error .error-main {
            margin:0 auto;
            width:450px
        }
        .module-error .label {
            float:left;
            width:160px;
            height:151px;
            background:url('${_cdnStaticResourcesPath}/resources/static/images/error_unauthorized.png') 0 0 no-repeat
        }
        .module-error .info {
            margin-left:182px;
            line-height:35px;
        }
        .module-error .title {
            color:#666;
            font-size:14px
        }
        .module-error .reason {
            margin:8px 0 18px 0;
            color:#666
        }
        .module-error .reason p{
            margin:8px 0 18px 0;
            color:#666;
            line-height: 15px;
        }
        #doc_main{
            position: absolute;
            top:45%;
            left:50%;
            width:100%;
            transform:translate(-50%,-50%);
        }
    </style>
</head>
<body class="error">
<div id="doc_main">
    <section class="bd clearfix">
        <div class="module-error">
            <div class="error-main clearfix">
                <div class="label"></div>
                <div class="info">
                    <h3 class="title">${errorMsg}</h3>
                    <div class="reason">
                        <p >可能的原因：</p>
                        <p>1、暂未拥有此模块功能的访问权限</p>
                        <p>2、管理员禁止他人进行操作访问</p>
                    </div>
                    <%--<div class="oper">
                        <p><a href="###">返回上一级页面&gt;</a></p>
                        <p><a href="###">回到网站首页&gt;</a></p>
                    </div>--%>
                </div>
            </div>
        </div>
    </section>
</div>
</body>