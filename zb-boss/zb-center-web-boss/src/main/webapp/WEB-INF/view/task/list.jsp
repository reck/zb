﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title>定时任务列表</title>
	<style>
		/** 表格的操作按钮的内边距 */
		.btn{padding: 1px 8px!important;font-size: 12px!important;}
		/**顶部操作按钮的内边距*/
		.border .btn {
			display: inline-block;
			padding: 5px 12px!important;
			margin-bottom: 0;
			font-weight: 400;
			line-height:18px;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			cursor: pointer;
			user-select: none;
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
			margin-left: 10px;
		}
		.label {
			 display: inline!important;
			 padding: .2em .6em .3em!important;
			 font-size: 11px!important;
			 font-weight: 700!important;
			 line-height:20px!important;
			 height: 20px!important;
			 color: #fff!important;
			 text-align: center!important;
			 white-space: nowrap!important;
			 vertical-align: baseline!important;
			 border-radius: .25em!important;
		 }
		/**表格tr背景色*/
		.table thead tr{background-color: #fff!important;}
		/**表格单元格样式*/
		.table thead tr th{padding: 10px;}
		/**表格单元格border*/
		.table tbody tr td{border: 0px!important;border-bottom: 1px solid #e7eaec !important;border-left: 1px solid #e7eaec !important;}
		.table-bordered > thead > tr > th, .table-bordered > thead > tr > td{border-bottom-width: 1px!important;}
		.table-bordered{border: 0px!important;}
		/**表头的高度*/
		.table thead > tr > th,.table thead > tr > td{padding: 8px!important;}

		td.details-control {
			background: url('http://datatables.club/example/resources/details_open.png') no-repeat center center;
			cursor: pointer;
		}
		tr.shown td.details-control {
			background: url('http://datatables.club/example/resources/details_close.png') no-repeat center center;
		}
	</style>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
	<script src="${_cdnStaticResourcesPath}/resources/static/js/jquery-fixed.js" type="text/javascript" ></script>
</head>

<body id="tasklist_frame_box">
<div class=" page-content clearfix">
	<div id="task_list_style">
		<div class="search_style" style="background-color: #fff;margin-bottom: 0px;">
			<div class="title_names">说明</div>
			<%--<div class="alert alert-block alert-warning" style="text-align: left;margin-top: 5px;margin-bottom: 0px;padding: 5px!important;">
				<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
				<i class="fa fa-hand-o-right"></i>
				&nbsp;定时任务列表数据是通过反射机制获取的。当前系统不做任务的任何执行操作，具体的任务调度等功能，都统一在【分布式任务调度平台】系统中进行操作管理；
			</div>--%>
			<%--<div class="alert alert-block alert-success" style="text-align: left;margin-top: 5px;margin-bottom: 0px;padding: 5px!important;">
				<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
				<i class="fa fa-hand-o-right"></i>
				&nbsp;《分布式任务调度平台》优点：统一管理各系统的Task任务、调度、日志、执行器等功能；与具体项目业务进行解耦；
			</div>--%>
			<div class="alert alert-block alert-info" style="text-align: left;margin-top: 5px;margin-bottom: 0px;padding: 5px!important;">
				<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
				<i class="fa fa-hand-o-right"></i>
				&nbsp;定时任务列表数据是通过反射机制获取的。当前系统不做任何的任务执行操作，具体的任务调度等功能，都统一在【分布式任务调度平台】系统中进行操作管理；
				<br/>
				<span style="color: #FF1493;">
					<i class="fa fa-hand-o-right"></i>
					&nbsp;业务系统只需关心task的具体业务逻辑代码即可，无需关心如何去调度执行；
				</span>
				<br/>
				<span>
					<i class="fa fa-hand-o-right"></i>
					&nbsp;业务系统只需简单配置，《任务调度平台》的管理员即可在平台上手动录入执行器信息，平台会自动检测并注册具体的执行器；实现分布式服务下的任务统一管理;
				</span>
			</div>
			<%--<div class="alert alert-block alert-danger" style="text-align: left;margin-top: 5px;margin-bottom: 0px;padding: 5px!important;">
				<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
				<i class="fa fa-hand-o-right"></i>
				&nbsp;线上环境下的类似于数据库备份的Task，为了系统的稳定性，将直接跳过不执行；（任务执行的开关是在配置文件中指定）
			</div>--%>
		</div>
		<hr style="border-style: dashed;border-width: 1px;margin-bottom:20px;margin-top: 20px;"/>

		<!--表格数据-->
		<div class="h_products_list clearfix" id="table_list" style="overflow: visible!important;">
			<%--<div id="scrollsidebar" class="left_Treeview">
				<div class="show_btn" id="rightArrow"><span></span></div>
				<div class="widget-box" >

				</div>
			</div>--%>
			<div class="table_task_list" id="taskListIframe" style="margin-left: 5px!important;width: 100%;">
				<table class="table table-striped table-bordered table-hover" id="taskListTable" style="width: 100%;">
					<thead>
					<tr>
						<th></th>
						<th>任务名称</th>
						<th>任务描述</th>
						<th>执行器注册方式</th>
						<th>执行器机器</th>
						<th>机器运行状态</th>
						<th>任务调度平台地址</th>
					</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<input type="hidden" des="获取任务列表url" id="getTaskListUrl" value="${ctx}/task/list"/>
</body>
</html>
<script>
    var taskListTable;//jquery table全局表格对象
    $(function() {
        taskListTable = $('#taskListTable').dataTable({
            order: [],  //取消默认排序查询,否则复选框一列会出现小箭头
            dom: '<"top">t<"float_left_l"l><"float_left_i"i><"float_left_r"r>p<"clear">',//设置表格的排版规则(提示语、分页、搜索、页数分组等的位置设置)
            language:lang,//自定义分页的相关信息，用于替换插件默认的配置。详细配置在common.jsp中
            aLengthMenu: [15,10,20,50,100],
            destroy : true,
            //bSort : true,
            autoWidth: false,  //自动调整列宽
            processing: false,  //取消插件默认的加载提示
            serverSide: true,  //服务器端分页
            bStateSave: false,//状态保存
            deferRender : true,//控制表格的延迟渲染，可以提高初始化的速度。
            ajax: function (data, callback, settings) {
                var param = {};
                //封装请求参数
                param.draw = data.draw;
                param.start = data.start;//开始的记录序号
                param.page = (data.start / data.length)+1;//当前页码
                param.limit = data.length;//页面显示记录条数，在页面显示每页显示多少项的时候
                param.name = $("#name").val();
                param.status = $("#status").val();
                //设置排序参数
                if(data.order.length > 0){
                    param.order = data.order[0].dir;//排序方式
                    param.sort = data.columns[data.order[0].column].name;//排序字段名
                }
                //ajax请求数据
                $.ajax({
                    type: "POST",
                    url: $("#getTaskListUrl").val(),
                    cache: false,  //禁用缓存
                    data: param,  //传入组装的参数
                    dataType: "json",
                    timeout:10000,
                    beforeSend: function(){//请求之前显示loading效果
                        $(".dataTable tr:gt(0)").remove();
                        $(".dataTable thead").append("<tr style='line-height:38px;'><td style='text-align:center;background-color:#FFFFFF;' colspan='9'></td></tr>");
                        $(".dataTable tr:eq(1) td").html("<img src='" + $("#cdnStaticResourcesPath").val() + "/resources/static/images/loading.gif'/><span style='margin-left:5px;vertical-align:middle;'>" + lang.sProcessing + "</span>");
                    },
                    success: function (result) {
                        $(".dataTable tr:gt(0)").remove();//移除loading
                        //封装返回数据
                        var returnData = {};
                        returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
                        returnData.recordsTotal = result.recordsTotal;//返回数据全部记录
                        returnData.recordsFiltered = result.recordsFiltered;//后台不实现过滤功能，每次查询均视作全部结果
                        returnData.data = result.data;//返回的数据列表
                        callback(returnData);
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                        $(".dataTable tr:gt(0)").remove();//移除loading
                        $(".dataTable thead").append("<tr style='line-height:38px;'><td style='text-align:center;' colspan='9'></td></tr>");
                        if(textStatus=="timeout"){
                            $(".dataTable tr:eq(1) td").html("<span style='margin-left:5px;vertical-align:middle;color:red;'>数据请求超时，稍后请刷新重试</span>");
                        }else{
                            $(".dataTable tr:eq(1) td").html("<span style='margin-left:5px;vertical-align:middle;color:red;'>数据请求异常，稍后请刷新重试</span>");
                        }
                    }
                });
            },


            //列表表头字段 //visible:是否显示该列，bSortable：是否排序
            //这里的name属性有特别的作用，用于映射数据库对应的字段名。
            //      比如statusName为枚举的值，但数据库对应的字段为status，所以上面在设置请求排序参数的时候，应该获取的是name值，而不是data属性值。
            columns: [
                {
                    "class":'details-control',//第一列td的样式，css代码请至页面的style样式设置区域查看
                    "bSortable": false,
                    "width" : "5%",
                    "defaultContent": '' //td中不显示任何内容
                },
                { "data": "taskName" , "bSortable": false,  width : "12%"},
                { "data": "taskDesc" , "bSortable": false , width : "15%"},
                { "data": "addressType" , "bSortable": false , width : "8%"},
                { "data": "jobExecutorIp" , "bSortable": false , width : "10%"},
                { "data": "jobExecutorStopStatus" , "bSortable": false , width : "8%"},
                { "data": "taskDispatchPlatformUrl" , "bSortable": false , width : "20%"}
            ],

            //targets：表示具体需要操作的目标列，下标从0开始
            //data: 表示我们需要的某一列数据对应的属性名
            //render:返回需要显示的内容。在此我们可以修改列中样式，增加具体内容
            "columnDefs": [
                {
                    "targets": [3],
                    "data": "addressType",
                    "render": function(data, type, full) {
                        if(full.addressType == 0){
                        	return "<span  style='background-color: #03B7E3!important;font-weight: bold;padding-top: 3px;' class=\"badge bg-green\">自动注册</span>";
						}else if(full.addressType == 1){
                            return "<span  style='background-color: #00BA9C!important;font-weight: bold;padding-top: 3px;' class=\"badge bg-green\">手动录入</span>";
						}else{
                            return "<span  style='background-color: #DD4B39!important;font-weight: bold;padding-top: 3px;' class=\"badge bg-green\">（未知）</span>";
						}
                    }
                },
                {
                    "targets": [4],
                    "data": "jobExecutorIp",
                    "render": function(data, type, full) {
                        return "<span  style='background-color: #C798EA!important;font-weight: bold;padding-top: 3px;' class=\"badge bg-green\">" + full.jobExecutorIp + "</span>";
                    }
                },
                {
                    "targets": [5],
                    "data": "jobExecutorOnlineStatus",
                    "render": function(data, type, full) {
                        if(full.jobExecutorStopStatus == false){
                            return "<span  style='background-color: #00A65A!important;font-weight: bold;padding-top: 3px;' class=\"badge bg-green\">运行中</span>";
                        }else if(full.jobExecutorStopStatus == true){
                            return "<span  style='background-color: #D2D6DE!important;font-weight: bold;padding-top: 3px;' class=\"badge bg-red\">已停止</span>";
						}else{
                            return "<span  style='background-color: #DD4B39!important;font-weight: bold;padding-top: 3px;' class=\"badge bg-red\">（未知）</span>";
						}
                    }
                },
                {
                    "targets": [6],
                    "data": "taskDispatchPlatformUrl",
                    "render": function(data, type, full) {
                        return "<a style='font-weight: bold;color: #F39C12;' target='_blank' href='" + full.taskDispatchPlatformUrl + "'>" + full.taskDispatchPlatformUrl + "</a>";
                    }
                }
			],
            //表格初始化完毕的回调
            initComplete: function(oSettings, json) {
                function format (row) {
                    var runStr = "";
                    if(row.onlineRun == false){
                        runStr = "不执行";
					}else{
                        runStr = "执行";
					}
                    return '' +
						'<table cellspacing="0" border="0">'+
							'<tr>'+
								'<td style="color: #2C98E0;text-align: right;padding-right: 20px;">JobHandler名称</td>'+
								'<td style="text-align: left;color: #2C98E0;padding-left: 20px;">' + row.beanName + '</td>'+
								'<td style="text-align: left;color: #2C98E0;padding-left: 20px;">【说明】：对应的是： 任务调度平台 > 任务管理 > 新增任务 > JobHandler</td>'+
							'</tr>'+
							'<tr>'+
								'<td style="color: #119999;text-align: right;padding-right: 20px;">执行器应用名称</td>'+
								'<td style="text-align: left;color: #119999;padding-left: 20px;">' + row.jobExecutorAppName + '</td>'+
								'<td style="text-align: left;color: #119999;padding-left: 20px;">【说明】：对应的是： 任务调度平台 > 执行器管理 > 新增执行器 > AppName</td>'+
							'</tr>'+
							'<tr>'+
								'<td style="color: #9D76EA;text-align: right;padding-right: 20px;">任务日志路径</td>'+
								'<td style="text-align: left;color: #9D76EA;padding-left: 20px;">' + row.jobExecutorLogpath + '</td>'+
								'<td style="text-align: left;color: #9D76EA;padding-left: 20px;">【说明】：配置文件中指定具体路径, 记录每一个Task任务的执行日志信息, 方便后期查看与追踪</td>'+
							'</tr>'+
							'<tr>'+
								'<td style="color: #F79046;text-align: right;padding-right: 20px;">任务逻辑代码是否执行</td>'+
								'<td style="text-align: left;color: #F79046;padding-left: 20px;">' + runStr + '</td>'+
								'<td style="text-align: left;color: #F79046;padding-left: 20px;">【说明】：表示在线上环境下, 该Task的具体逻辑代码是否会执行; 线上应用提供功能预览的情况下, 对于一些重要操作, 考虑到系统稳定性等因素, 建议设置成不执行;</td>'+
							'</tr>'+
                        '</table>';
                }

                $('#taskListTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = taskListTable.row( tr );
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                });
                //启用bootstrap的tootip提示功能
                //$('#taskListTable tbody td.details-control').click();//默认展开详情内容
			}
        }).api();
    });


    /** ==========================================其他相关功能处理======================================= */
    $(function () {
        //滚动条美化
        $("#taskListIframe").niceScroll({
            cursorcolor:"#888888",
            cursoropacitymax:1,
            touchbehavior:false,
            cursorwidth:"5px",
            cursorborder:"0",
            cursorborderradius:"5px"
        });
    });
</script>
