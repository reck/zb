﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
		<title>分布式任务调度平台——xxl-job</title>
	<%@ include file="/WEB-INF/view/common/common.jsp"%>
</head>

<body id="applist_frame_box">
<div class=" page-content clearfix">
	<div id="app_list_style">
		<c:if test="${empty platformUrl}">
			<div class="alert alert-block alert-danger" style="text-align: left;margin-top: 30px;">
				&nbsp;检测到项目中未指定分布式任务调度平台的访问地址！请前往 &nbsp; zb-center-web-boss/resources/(dev|pro)/properties/config.properties &nbsp; 中检查！
			</div>
		</c:if>
		<c:if test="${not empty platformUrl}">
			<div class="search_style" style="background-color: #fff;margin-bottom: 0px;">
				<div class="title_names">简介</div>
				<div class="alert alert-block alert-info" style="text-align: left;margin-top: 5px;margin-bottom: 0px;padding: 5px!important;">
					<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
					<i class="fa fa-hand-o-right"></i>
					&nbsp;友情提示：当前分布式任务调度平台采用的是开源中国上的项目【xxl-job】；尊重开源，尊重版权；项目源码地址：<a target="_blank" href="https://gitee.com/xuxueli0323/xxl-job">https://gitee.com/xuxueli0323/xxl-job</a>
				</div>
			</div>
			<hr style="border-style: dashed;border-width: 1px;margin-bottom: 5px;margin-top: 5px;"/>







			<!--分布式任务调度平台的功能引用另外一个子系统zb-task-admin。--->
			<!--分布式任务调度平台 网上有很多开源的案例，这里我采用的是开源中国上的项目【XXL-JOB】，项目源码地址：https://gitee.com/xuxueli0323/xxl-job -->
			<iframe id="platformIframe" src="${platformUrl}" style="width: 100%;height:auto;border: 0px;"></iframe>






			<!--让ifram出现loading加载的动画，比较人性化一些-->
			<div id="loadingDiv" align="center">
				<img  style='position: absolute;width:320px;height:240px;left:50%;top:50%;margin-left:-160px;' src="${_cdnStaticResourcesPath}/resources/static/images/code_generator_iframe_loading.gif"/>
			</div>
			<script type="text/javascript">
                var platformIframe = document.getElementById("platformIframe");//ifram对象
                var loadingDiv = document.getElementById("loadingDiv");//loading加载层

                platformIframe.style.display = "none"; //页面默认隐藏
                loadingDiv.style.display = "block"; //默认显示loading

                $(platformIframe).load(function(){//iframe加载完毕的事件
					loadingDiv.style.display = "none";//隐藏loading
                    platformIframe.style.display = "block";//显示页面内容
                });

                /** ===================================================下方用于检测处理ifram加载超时的问题，对用户友好提示============================================== */
                var kill = setTimeout("showErr()",10000);//设置iframe默认10秒超时时间
                platformIframe.onload = function(){//ifram加载成功后的处理
					clearTimeout(kill);//ifram加载成功后,停止计时器
				};
                function showErr() {//ifram加载超时处理
                    loadingDiv.innerHTML = "" +
						"<p style='position: absolute;width:800px;height:200px;left:50%;top:45%;margin-left:-400px;color: #F29B03;font-weight: bold;font-size: 15px;'>" +
							"哎呀！ &lt;分布式任务调度平台&gt; 访问超时了！" +
						"</p>" +
						"<span style='position: absolute;width:800px;height:200px;left:50%;top:45%;margin-left:-400px;text-align: left; '>" +
							"<br/><br/>您需要检查一下：" +
							"<br/><br/>1、请到 &nbsp; zb-center-web-boss/resources/(dev|pro)/properties/config.properties &nbsp; 中检查分布式任务调度平台访问地址是否配置错误；" +
							"<br/>2、检查zb-task-admin系统模块是否成功启动；" +
						"</span>";
                }
			</script>

			<!--浏览器大小缩放的时候，代码生成页面的高度要自适应浏览器，避免数据显示不全-->
			<script>
                window.onload = function (ev) {
                    var h = $("#platformIframe").offset().top;
                    $("#platformIframe").css("height",($(document).height() - h) + "px");
                }
                window.onresize = function (ev) {
                    var h = $("#platformIframe").offset().top;
                    //console.log(document.body.clientHeight);
                    //console.log(h);
                    //console.log(document.body.clientHeight - h);
                    $("#platformIframe").css("height",(document.body.clientHeight - h) + "px");
                }
			</script>
		</c:if>
	</div>
</div>
</body>
</html>
