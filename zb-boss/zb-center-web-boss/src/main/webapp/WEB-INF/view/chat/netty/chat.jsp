﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta charset="utf-8">

    <title>netty在线聊天服务</title>

    <!-- 静态css、js资源 -->
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <link href="${_cdnStaticResourcesPath}/resources/static/chat/images/dandan.css" rel="stylesheet" media="screen" type="text/css" />

    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/static/chat/jquery.snowfall.js"></script>
    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/static/chat/chat.js"></script>
    <script type="text/javascript" src="${_cdnStaticResourcesPath}/resources/static/chat/view.js"></script>
    <script type="text/javascript">
        var homeTab = "聊天大厅";
        window.onload = function(){
            CHAT.init($admin_name);//聊天初始化
            title_newuser("title_user00",homeTab,"");
        }
    </script>
</head>
<body>
<div id="mid_top">
    <!--  <div class="list">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="td_user td_user_click">老猪</td>
            <td class="td_hide td_hide_click">X</td>
          </tr>
        </table>
      </div>-->
</div>
<div id="top" style="text-align: center;"></div>
<div id="body" style="height: 100%!important;">
    <div id="left">
        <div class="box">
            <h3 class="h3 h3_1">最近聊天(<span class="n_geshu_1"></span>)</h3>
            <ul class="ul ul_1">
                <li></li>
            </ul>
            <h3 class="h3 h3_2">在线用户(<span class="n_geshu_2"></span>)</h3>
            <ul class="ul ul_2">
                <li></li>
            </ul>
        </div>
        <div class="box_f"></div>
    </div>
    <div id="right">
        <div class="right_box">
            <div id="right_top">
                <!--<p><img src="images/head.jpg" alt="头象" /></p>
                老猪--></div>
            <div id="right_mid"></div>
            <%--<div id="right_foot"></div>--%>
            <div class="blank"></div>
        </div>
        <div class="right_box_foot"></div>
    </div>
    <div id="mid">
        <div id="mid_con">
            <div class="my_show">
                <div class="con_box" style="height: auto;">
                    <%--<p style="text-align: center;padding-top:20px;">
                        <span style="color: #A7A7A7;width: auto;padding: 2px 6px;" class="time-label">欢迎来到在线聊天室，您当前所在位置 - 聊天大厅</span>
                    </p>
                    <p style="text-align: center;padding-top:20px;">
                        <span style="color: #A7A7A7;width: auto;padding: 2px 6px;" class="time-label">给在线的朋友发送一条问候信息吧^_^</span>
                    </p>--%>
                </div>
            </div>
        </div>
        <div id="face-box" style="display: none;position: absolute;bottom: 40px;height: 100px;overflow: auto;background: #FFF;margin-bottom: 170px;padding:5px;"></div>
        <div id="mid_mid">
            <span style="background: url('${_cdnStaticResourcesPath}/resources/static/chat/images/face.png') no-repeat;height: 30px;width: 30px;margin: 2px 0px;display: inline-block;cursor: pointer" onclick="CHAT.openFace()" title="选择表情"></span>
            <span style="background: url('${_cdnStaticResourcesPath}/resources/static/chat/images/flower.png') no-repeat;height: 30px;width: 30px;margin: 2px 0px;display: inline-block;cursor: pointer" onclick="CHAT.sendFlower()" title="送鲜花"></span>
        </div>
        <div id="mid_foot">
            <div id="mid_say">
                <div contenteditable="true" placeholder="这里输入你要说的内容" name=""  id="texterea" style="padding-left: 5px!important;padding-right: 5px!important;height: 138px;"></div>
            </div>
            <div id="mid_sand">发送</div>
            <div class="blank"></div>
        </div>
        <div class="mid_box"></div>
    </div>
</div>

<input type="hidden" des="静态资源服务器地址" name="cdnStaticResourcesPath" id="cdnStaticResourcesPath" value="${_cdnStaticResourcesPath}" />
</body>
</html>
