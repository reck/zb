﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>

<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>首页</title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <style>
        /**表头的高度*/
       .table > tbody > tr > td{padding: 8px!important;}
        .Single_page .Single_page_list li{width: 300px!important;height: 160px!important;}

        /**动画参考 http://www.htmleaf.com/css3/css3donghua/201505221885.html*/
        .bb,.bb2,.bb3,.bb4,.bb5, .bb::before, .bb::after,.bb2::before, .bb2::after,.bb3::before, .bb3::after,.bb4::before, .bb4::after,.bb5::before, .bb5::after {position: absolute;top: 0;bottom: 0;left: 0;right: 0;}
        .bb {width: 300px;height: 160px;color: #56ABE3;}
        .bb2 {width: 300px;height: 160px;color: #F56764;}
        .bb3 {width: 300px;height: 160px;color: #F79850;}
        .bb4 {width: 300px;height: 160px;color: #00BA9C;}
        .bb5 {width: 300px;height: 160px;color: #B6A2DE;}
        .bb::before,.bb2::before,.bb3::before,.bb4::before,.bb5::before, .bb::after,.bb2::after,.bb3::after,.bb4::after,.bb5::after {content: '';z-index: -1;margin: -2%;box-shadow: inset 0 0 0 2px;animation: clipMe 8s linear infinite;}
        .bb::before,.bb2::before,.bb3::before,.bb4::before,.bb5::before {animation-delay: -4s;}
        @keyframes clipMe {
            0%, 100% {
                clip: rect(0px, 310px, 2px, 0px);
            }
            25% {
                clip: rect(0px, 2px, 170px, 0px);
            }
            50% {
                clip: rect(168px, 310px, 170px, 0px);
            }
            75% {
                clip: rect(0px, 310px, 170px,308px);
            }
        }
    </style>
</head>



<body  id="iframe_box">
<div id="index_style" style="height:100%" class="clearfix Single_page">
    <div class="alert alert-block alert-success" style="text-align: left;margin-left: 10px;font-weight: bold;margin-top: 0px;margin-bottom: 10px;">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="fa fa-hand-o-right"></i>&nbsp;最新发表文章教程：<span style="color:#F56764">《zookeeper集群环境搭建详细图文教程》</span> 访问地址：<a href="http://www.cnblogs.com/zhoubang521/p/8178997.html" target="_blank" style="color:#468847;text-decoration:underline;">http://www.cnblogs.com/zhoubang521/p/8178997.html</a>
    </div>
    <%--<div class="alert alert-block alert-warning" style="text-align: left;margin-left: 10px;font-weight: bold;margin-top: 0px;margin-bottom: 10px;">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="fa fa-hand-o-right"></i>&nbsp;友情提示：当前系统只开放【系统管理】、【文件打印】、【定时任务】、【代码生成器】、【在线资源管理器】、【在线聊天】模块，其他模块正在努力整理中，陆续加入新功能。
    </div>--%>
    <div class="spacing_style" ></div>
    <div class="Single_page_list">
        <ul>
            <li class="page_list_content bb">
                <%--<a target="_blank" href="https://gitee.com/zhoubang85/zb" class="link_name"></a>--%>
                <div class="title_name" style="background-color: #56ABE3;color: white;">当前zb项目源码</div>
                <div class="page_info">
                    分布式服务架构 - Java后台管理系统。主要技术框架：Dubbo、Zookeeper、SpringMVC、Mybatis、Shiro
                </div>
                <div class="time"><a style="position: relative;color:#56ABE3;" target="_blank" href="https://gitee.com/zhoubang85/zb" class="link_name">https://gitee.com/zhoubang85/zb</a></div>
            </li>
            <li class="page_list_content bb3">
                <%--<a target="_blank" href="https://gitee.com/zhoubang85/zb" class="link_name"></a>--%>
                <div class="title_name" style="background-color: #F79850;color: white;">当前zb项目springboot版本</div>
                <div class="page_info">
                    基于zb项目（https://gitee.com/zhoubang85/zb ）进行了springboot版本的开发重构，方便不同需求的朋友都可以找到自己的学习资源；
                </div>
                <div class="time"><a style="position: relative;color: #F37B1D;" target="_blank" href="https://gitee.com/zhoubang85/sea_springboot" class="link_name">https://gitee.com/zhoubang85/sea_springboot</a></div>
            </li>
            <li class="page_list_content bb2">
                <%--<a target="_blank" href="https://gitee.com/zhoubang85/zb" class="link_name"></a>--%>
                <div class="title_name" style="background-color: #F56764;color: white;">分布式事务项目</div>
                <div class="page_info">
                    以【电商购物支付】作为当前分布式项目的业务功能，通过该项目完整实现并解决分布式服务下的【分布式事务】问题
                </div>
                <div class="time"><a style="position: relative;color: #F56764;" target="_blank" href="https://gitee.com/zhoubang85/zb-pay-dubbo" class="link_name">https://gitee.com/zhoubang85/zb-pay-dubbo</a></div>
            </li>
            <li class="page_list_content bb4">
                <%--<a target="_blank" href="https://gitee.com/zhoubang85/zb" class="link_name"></a>--%>
                <div class="title_name" style="background-color: #00BA9C;color: white;">springcloud全家桶基础项目案例</div>
                <div class="page_info">Eureka注册中心（支持集群部署）、Config注册中心（支持集群部署）、Zuul 服务网关（支持集群部署）、Hystrix 断路器、Ribbon 负载均衡、Feign 负载均衡、Turbine 聚合监控、Bus 消息总线
                </div>
                <div class="time"><a style="position: relative;color:#00BA9C;" target="_blank" href="https://gitee.com/zhoubang85/sea-springcloud" class="link_name">https://gitee.com/zhoubang85/sea-springcloud</a></div>
            </li>
            <li class="page_list_content bb5">
                <%--<a target="_blank" href="https://gitee.com/zhoubang85/zb" class="link_name"></a>--%>
                <div class="title_name" style="background-color: #B6A2DE;color: white;">Java技术论坛</div>
                <div class="page_info">
                    个人搭建的技术论坛，分享技术资源、项目源码等学习案例。将所学习、掌握的技能分享给需要的朋友
                </div>
                <div class="time"><a style="position: relative;color: #A581E8;" target="_blank" href="http://www.2b2b92b.cn" class="link_name">http://www.2b2b92b.cn</a></div>
            </li>
        </ul>
    </div>
    <div class="center clearfix margin-bottom">
        <!--店铺信息-->
        <div class="col-xs-9" style="margin-top:30px;">
            <div class="Shops_info clearfix frame">
                <div class="left_shop">
                    <div class="dd_echarts">
                        <div id="main" style="width:100%; height:270px"></div>
                    </div>
                </div>
                <div class="operating_style Quick_operation" style="height: 30px;line-height: 30px;">
                    <p style="margin-left: 10px;text-align: center;">今日登录次数：${todayLoginNum} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本次登录IP：${curLoginIp} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上次登录IP：${lastLoginIp} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上次登录时间：${lastLoginTime}</p>
                </div>
            </div>
        </div>
        <div class="col-xs-3" style="margin-top: 30px;">
            <div class="admin_info frame clearfix">
                <div class="title_name"><i></i>登录动态</div>
                <table class="record_list table table_list">
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <!---->
    <%--<div class="Order_form ">
        <div class="col-xs-6 col-lg-7">
            <div class="frame margin-right clearfix">
                <div class="title_name"><i></i>店铺/订单状态提示</div>
                <div class="clearfix">
                    <div class="col-xs-3 col-lg-6 ">
                        <div class="prompt_name"><i class="icon_Order"></i>订单& 商品</div>
                        <ul class="padding list_info">
                            <li>代发货订单 &nbsp;<a href="">(02)</a></li>
                            <li>已完成订单 &nbsp;<a href="">(32)</a></li>
                            <li>未完成订单 &nbsp;<a href="">(02)</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-3 col-lg-6">
                        <div class="prompt_name"><i class="icon_Promotions"></i>促销</div>
                        <ul class="padding list_info">
                            <li>待审核的促销 &nbsp;<a href="">(02)</a></li>
                            <li>发布的促销 &nbsp;<a href="">(32)</a></li>
                            <li>即将结束的促销 &nbsp;<a href="">(02)</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-3 col-lg-6">
                        <div class="prompt_name"><i class="icon_Aftermarket"></i>售后</div>
                        <ul class="padding list_info">
                            <li>待处理订单 &nbsp;<a href="">(02)</a></li>
                            <li>待退款订单 &nbsp;<a href="">(32)</a></li>
                            <li>待处理售后单 &nbsp;<a href="">(02)</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-3 col-lg-6 ">
                        <div class="prompt_name"><i class="icon_Billing"></i>结算</div>
                        <ul class="padding list_info">
                            <li>待支付 &nbsp;<a href="">(02)</a></li>
                            <li>待结算确认 &nbsp;<a href="">(32)</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 ranking_style col-lg-5" >
            <div class="frame clearfix">
                <div class="title_name"><i></i>商品销售排行</div>
                <table  class="table table_list ranking_list">
                    <thead>
                    <th width="50">排名</th>
                    <th>商品编号</th>
                    <th>商品名称</th>
                    <th width="80">销售数量</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td ><em>1</em></td>
                        <td>2345776</td>
                        <td><a href="#">联想（ThinkPad ）轻薄系列</a></td>
                        <td>23</td>
                    </tr>
                    <tr>
                        <td ><em>2</em></td>
                        <td>2345776</td>
                        <td><a href="#">施巴（sebamed）婴儿泡泡沐浴露200ml家庭装</a></td>
                        <td>23</td>
                    </tr>
                    <tr>
                        <td ><em>3</em></td>
                        <td>2345776</td>
                        <td><a href="#">七匹狼纯棉时尚休闲条纹翻领POLO衫T恤</a></td>
                        <td>23</td>
                    </tr>
                    <tr>
                        <td ><em>4</em></td>
                        <td>2345776</td>
                        <td><a href="#">桂格即食燕麦片超值装1478g</a></td>
                        <td>23</td>
                    </tr>
                    <tr>
                        <td ><em>5</em></td>
                        <td>2345776</td>
                        <td><a href="#">韩国爱敬挚爱香氛花香洗护套装（洗发水600ml+护发素</a></td>
                        <td>23</td>
                    </tr>
                    <tr>
                        <td ><em>6</em></td>
                        <td>2345776</td>
                        <td><a href="#">韩国爱敬挚爱香氛花香洗护套装（洗发水600ml+护发素</a></td>
                        <td>23</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>--%>
</div>

<input type="hidden" id="statisticsRequestUrl" value="${cxt}/access/record/accessStatistics" desc="访问记录统计，请求地址"/>
<input type="hidden" id="getAccessRecordListUrl" value="${cxt}/access/record/list" desc="获取访问记录数据"/>

</body>
</html>

<script src="${_cdnStaticResourcesPath}/resources/static/js/dist/echarts.js" type="text/javascript"></script>
<script>
    //设置框架
    $(function() {
        $("#index_style").frame({
            float : 'left',
            menu_nav:'.Quick_operation',
            color_btn:'.skin_select',
            Sellerber_menu:'.list_content',
            Sellerber_header:'.Sellerber_header',
        });
    });
    $("#iframe_box").niceScroll({
        cursorcolor:"#888888",
        cursoropacitymax:1,
        touchbehavior:false,
        cursorwidth:"5px",
        cursorborder:"0",
        cursorborderradius:"5px"
    });
    /** =================================================================================================================== */
    var myChart;
    require.config({
        paths: {
            echarts: '/resources/static/js/dist'
        }
    });
    require(
        [
            'echarts',
            'echarts/theme/macarons',
            'echarts/chart/line',   // 按需加载所需图表，如需动态类型切换功能，别忘了同时加载相应图表
            'echarts/chart/bar'
        ],
        function (ec,theme) {
            myChart = ec.init(document.getElementById('main'),theme);
            myChart.showLoading({
                text : "访问统计数据加载中...",
                effect : "whirling",
                textStyle : {
                    fontSize : 14
                },
                noDataLoadingOption : {
                    text : "暂无统计数据",
                    effect : "bubble"
                }
            });
            var option = {
                timeline:{
                    data:[],
                    label : {
                        formatter : function(s) {
                            return s.slice(0, 10);
                        }
                    },
                    autoPlay : true,
                    playInterval : 1000
                },
                options:[{
                    title : {
                        subtext: '统计数值',
                        textStyle : {
                            fontSize : 14,
                            color : "#03A58B"
                        }
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['总登录次数','客户端数量（IP统计）']
                    },
                    grid: {x:25, y: 60, y2:75, x2:30},
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: false},
                            dataView : {show: false, readOnly: false},
                            magicType : {show: true, type: [ 'line','bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            axisLabel:{'interval':0},
                            data : ['1点','2点','3点','4点','5点','6点','7点','8点','9点','10点','11点','12点',
                                '13点','14点','15点','16点','17点','18点','19点','20点','21点','22点','23点','24点']
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value',
                            axisLabel : {
                                formatter: '{value}'
                            }
                        }
                    ],
                    series : [
                        {
                            name:'总登录次数',
                            type:'bar',
                            itemStyle : { normal: {label : {show: true, position: 'top'}}},
                            markPoint : {
                                data : [
                                    {type : 'max', name: '最大值'},
                                    {type : 'min', name: '最小值'}
                                ]
                            },
                            markLine : {
                                data : [
                                    {type : 'average', name: '平均值'}
                                ]
                            },
                            barCategoryGap:'35%'
                        },
                        {
                            name:'客户端数量（IP统计）',
                            type:'bar',
                            itemStyle : { normal: {label : {show: true, position: 'top'}}},
                            markPoint : {
                                data : [
                                    {type : 'max', name: '最大值'},
                                    {type : 'min', name: '最小值'}
                                ]
                            },
                            markLine : {
                                data : [
                                    {type : 'average', name : '平均值'}
                                ]
                            }
                        }
                    ]
                }]
            };
            initData(option);//加载统计图
        }
    );

    /**
     * 初始化数据
     * @param option
     */
    function initData(option){
        //获取访问统计数据
        var curDate = getNowDate();
        getStatisticalData(option,curDate,10);
    }

    /**
     * 获取访问统计数据
     * @param dateStr 结束的日期
     * @param dayNum 往前推算统计多少天的数据，后台默认7天
     */
    function getStatisticalData(option,dateStr,dayNum){
       $.ajax({
            type: "post",
            url: $("#statisticsRequestUrl").val() + "/" + dateStr + "/" + dayNum,
            cache: false,
            async : true,
            dataType: "json",
            success: function (data){
                if(data.code == 200){
                    assembly(option,dateStr,dayNum,data.result);
                }else{
                    myChart.showLoading({
                        text : "访问统计 - 数据加载异常",
                        effect : "whirling",
                        textStyle : {
                            fontSize : 14,
                            color : "#F37B1D"
                        }
                    });
                }
            },
            error:function (XMLHttpRequest, textStatus, errorThrown) {
                myChart.showLoading().text = "访问统计 - 数据加载异常";
            }
        });
    }

    /**
     * 接收处理后台返回的统计数据，展示统计图表
     * @param result
     */
    function assembly(option,dateStr,dayNum,result){
        var dataObj = eval("(" + result + ")");

        var dataMap = {};//统计图所需数据集
        dataMap.dataCount = {};//客户端总访问次数
        dataMap.dataClientCount = {};//客户端数量（IP统计）

        var reqNum = [];//存放每个日期的24个小时的请求数
        var clientNum = [];//存放每个日期的客户端数量（IP分组求和）
        var firstDate = "";//X轴第一个显示的日期
        var num = 0;//这里的num只作为一个逻辑判断的作用。
        for(var i in dataObj){//用javascript的for/in循环遍历对象的属性。i指的是日期值，比如2017-11-20
            if(firstDate == ""){
                firstDate = i;
                num = 1;
            }
            //获取24个小时的每个小时的访问统计数据
            for(var j = 0; j < 24; j++){
                reqNum.push(dataObj[i][j].reqNum);
                clientNum.push(dataObj[i][j].clientNum);
            }

            option.timeline.data.push(i);//X轴的日期

            /**
             * 说明已经执行过一次循环了，因为第一次循环的时候值为1。
             * 为何要加上判断？因为firstDate作为X轴第一个日期，已经在下面代码初始化了，故需要从第二天日期开始处理。详情代码请在本页面上搜索【初始化统计图的第一个日期的统计数据】
             */
            if(num == 0){
                var dynamicDate = {
                    title : {
                        text : ""
                    },
                    series : []
                }
                var dataTemp = {
                    data : ""
                }
                dynamicDate.title.text = i + "访问统计";
                dataTemp.data = reqNum;
                dynamicDate.series.push(dataTemp);//填充请求数量

                //这里声明一个一模一样的dataTemp对象，与上面的dataTemp对象区分开，如果不声明的话，下面的push操作将会覆盖上面的push结果。避免对象被覆盖造成数据错误
                var dataTemp = {
                    data : ""
                }
                dataTemp.data = clientNum;
                dynamicDate.series.push(dataTemp);//填充客户端数量

                //每个日期的统计数据，每隔一秒动态展示
                option.options.push(dynamicDate);
            }

            dataMap.dataCount[i] = reqNum;//这里是指，对应日期下的所有请求统计数据
            dataMap.dataClientCount[i] = clientNum;//这里是指，对应日期下的所有客户端的数量

            //清空对象数组，以免出现重复数据
            reqNum = [];
            clientNum = [];
            num = 0;//第一次循环的时候，num的值为1，这里进行清零，作用就是方便判断已经执行了一次循环，方便日期的获取处理
        }
        //初始化统计图的第一个日期的统计数据
        option.options[0].title.text = firstDate + "访问统计";
        option.options[0].series[0].data = dataMap.dataCount[firstDate];
        option.options[0].series[1].data = dataMap.dataCount[firstDate];

        //填充统计图，实现效果
        myChart.hideLoading();//取消loading加载效果
        myChart.setOption(option);
        window.onresize = myChart.resize;//窗口发生变化，图标自适应宽高
    }

    //获取当前日期 yyyy-MM-dd
    function getNowDate() {
        var date = new Date();
        var seperator = "-";
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = date.getFullYear() + seperator + month + seperator + strDate;
        return currentdate;
    }
</script>
<script>
    getAccessRecordList(7);
    /**
     * 获取访问记录
     * @param num
     */
    function getAccessRecordList(num){
        $.ajax({
            type: "post",
            url: $("#getAccessRecordListUrl").val(),
            data : {"limit" : num},
            cache: false,
            async : true,
            dataType: "json",
            success: function (result){
                result = result.data;
                $(".record_list tbody").append("<tr style='font-weight: bold;'><td>登录用户名</td><td>登录时间</td><td>客户端IP</td></tr>");
                for(var i = 0; i < result.length; i++){
                    var str = "<tr><td>" + result[i].loginUserName + "</td><td>" + result[i].createTime + "</td><td>" + result[i].cliIp + "</td></tr>";
                    $(".record_list tbody").append(str);
                }
            },
            error:function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("获取访问记录失败,请稍后重试");
            }
        });
    }
</script>
<script>
    //设置框架
    $(function() {
        $("#page_style").frame({
            hover_btn:'.Single_page_list li',
            menu_nav:'.operation',
            minStatue : false,
        });
    });
    /*******滚动条*******/
    $("body").niceScroll({
        cursorcolor:"#888888",
        cursoropacitymax:1,
        touchbehavior:false,
        cursorwidth:"5px",
        cursorborder:"0",
        cursorborderradius:"5px"
    });
</script>