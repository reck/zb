<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>shiro权限认证-[BOSS系统]</title>
<meta charset="UTF-8" />

<style type="text/css">
body {
	/*background: #ebebeb;*/
	background: white;
	font-family: "Helvetica Neue", "Hiragino Sans GB", "Microsoft YaHei", "\9ED1\4F53", Arial, sans-serif;
	color: #222;
	font-size: 12px;
}
* {
	padding: 0px;
	margin: 0px;
}
a {
	text-decoration: none;
}

/* input框自动填充内容背景颜色为黄色解决方法，参考：http://www.w3cfuns.com/notes/17783/c7e9fe25bf61e3adfc2a37d3f8e39dc9.html */
input:-webkit-autofill { box-shadow: 0 0 0px 1000px white inset !important;}

.top_div {
	/*background: #008ead;*/
	background: white;
	width: 100%;
	height: 400px;
}
.ipt {
	border: 1px solid #d3d3d3;
	padding: 10px 10px;
	width: 290px;
	border-radius: 4px;
	padding-left: 35px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	-webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow
		ease-in-out .15s;
	-o-transition: border-color ease-in-out .15s, box-shadow ease-in-out
		.15s;
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s
}
.ipt:focus {
	border-color: #66afe9;
	outline: 0;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px
		rgba(102, 175, 233, .6);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px
		rgba(102, 175, 233, .6)
}
</style>


<script src="<c:url value="/resources/static/js/jquery-1.9.1.min.js"/>" type="text/javascript"></script>
<script type="text/javascript">
	//是否开启验证码登录
	var openVerifyCode = false;

	//是否开启ajax登录
	var openAjaxLogin = true;

	$(document).keyup(function(event) {
        if (event.keyCode == 13) {
            $("#loginLink").trigger("click");
        }
    });
	
	function reset(){
		$("#userName").val("");
		$("#password").val("");
		$("#userName").focus();
	}
	
	//客户端校验
    function check() {
        if ($("#userName").val() == "") {
            alert("请输入登录账户名");
            $("#userName").focus();
            return false;
        } else {
            $("#userName").val(jQuery.trim($('#userName').val()));
        }

        if ($("#password").val() == "") {
            alert("请输入登录密码");
            $("#password").focus();
            return false;
        }
        return true;
    }
	
    //服务器校验
    function severCheck(){
        if(check()){
            //使用ajax登录
            if(openAjaxLogin){
                var userName = $("#userName").val();
                var password = $("#password").val();
                
                $.ajax({
                    type: "post",
                    url: "<c:url value='/login/login'/>",
                    data: {"userName" : userName, "password" : password , "openAjaxLogin" : openAjaxLogin},
                    dataType:'json',
                    cache: false,
                    success: function(data){
                        if(200 == data.code){
                            window.location.href="<c:url value='/'/>";
                        }else if("usererror" == data.msg){
                            alert("用户名或密码有误");
                            $("#userName").focus();
                        }else{
                            alert(data.msg);
                            $("#userName").focus();
                        }
                    }
                });
            }
        }
    }
</script>
</head>
<body style="height:100%;">
	<div class="top_div" style="line-height: 550px;text-align: center;color:rgb(0, 142, 173);font-weight: bold;font-size: 25px;font-family: 宋体;">登录 - [BOSS系统]</div>
	<div id="loginbox" style="background: rgb(255, 255, 255); margin: -100px auto auto; border: 1px solid rgb(0,174,255); border-image: none; width: 400px; height: 200px; text-align: center;">
		<p style="padding: 30px 0px 10px; position: relative;">
			<span class="u_logo"></span> <input class="ipt" type="text" placeholder="请输入用户名" name="userName" id="userName" value="admin">
		</p>

		<p style="position: relative;">
		      <span class="p_logo"></span> 
		      <input class="ipt" id="password" name="password" type="password" onfocus="this.type='password'" placeholder="请输入密码" value="123456">
		</p>
		<div style="height: 50px; line-height: 50px; margin-top: 30px; border-top-color: rgb(218,218,218); border-top-width: 1px; border-top-style: solid;">
			<p style="margin: 0px 35px 20px 45px;">
				<span style="float: left;">
				    <!-- <a style="color: rgb(204, 204, 204);" href="#">忘记密码?</a> -->
				</span>
                <span style="float: right;">
				    <!-- <a style="color: rgb(204, 204, 204); margin-right: 10px;" href="#">注册</a> -->
				    <a  onclick="reset();" style="background: rgb(243,159,31); padding: 7px 10px; border-radius: 4px; border: 1px solid rgb(243,159,31); border-image: none; color: rgb(255, 255, 255); font-weight: bold;" href="javascript:void(0);">重置</a>
					<a id="loginLink" onclick="severCheck();" style="background: #199258; padding: 7px 10px; border-radius: 4px; border: 1px solid rgb(26, 117, 152); border-image: none; color: rgb(255, 255, 255); font-weight: bold;margin-left: 10px;" href="javascript:void(0);">登录</a>
                </span>
			</p>
		</div>
	</div>
	<div style="text-align: center;">
		<!-- <p style="color: #36AC55;font-size: 12px;margin-top: 20px;display: inline-flex;display: -webkit-inline-box;"><a href="#" class="login_qq">QQ登录</a><a href="#" class="login_wx">微信登录</a></p> -->
		<p style="color: red;font-size: 12px;margin-top: 20px;font-weight: bold;">系统登录账户：admin 密码：123456</p>
	</div>
</body>
</body>
</html>